let parameter=require("../specs/parameters.js");
var diagnostics = function(){
  
    this.enterFilterTextInDiagnostics = function(value) {
        browser.setValue('input[data-test-id="diagnosis-filter"]',value,100);
        browser.pause(1000);
    }
    this.dataUnavailable=function(){
        var diagGrid=browser.elements('app-data-grid[name="diagnosis-grid"]');
        var noData=diagGrid.elements('[data-test-id="Sorry, data unavailable"]');
        return noData.value.length
    }
    this.getColumnWidthVal=function(){
        var diagnosticsCol=browser.elements("//app-data-grid[@name='diagnosis-grid']//datatable-header-cell[@title='Protocol']");
        browser.pause(100);
        return diagnosticsCol.getAttribute("style");
    }
    
};
module.exports=new diagnostics(); 