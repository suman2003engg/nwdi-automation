let parameter=require("../specs/parameters.js");
var sequence_diagram = function(){

    //calculateLatency method is in testbase.js//

    this.getToolTipContent=function(){       
        var sequenceToolTip=browser.element("//div[@class='tooltip-container']").getText();
        return sequenceToolTip;
    };

    this.clickOnSelectedRow=function(){  
        var sequenceRowSelected=browser.element("//div[contains(@class,'list-line-center selected')]");
        sequenceRowSelected.click();
    }

    this.clickOnCalculateBtn = function () {
        var calculateBtn = browser.element("[data-test-id='Calculate-btn']");
        calculateBtn.click();
    };

    this.clickOnCalculateLatency = function () {
        var latencyBtn = browser.element("[data-test-id='Calculate-latency-btn']");
        latencyBtn.click();
    };

    this.clickFullScreenIcon=function(){
        browser.element('div[class="widget-max flex-centered"]').click();
    }

    this.clickOnCalculateLatencyCancelBtn = function () {
        var cancelBtn = browser.element("[data-test-id='Cancel-btn']");
        cancelBtn.click();
    }

    this.getCalculatedLatency = function () {
        var calculatedLatency = browser.element("[data-test-id='Calculated-latency']").getText();
        return calculatedLatency;        
    };

    //if needed this method may change//
    this.selectDataToCalculateLatency = function () {
        var dataTab = browser.elements("[data-test-id='list-line-center']");
        var objectCount = dataTab.value;
        var value = 1;
        objectCount.forEach(element => {
            if (value == 1 || value == 2) {
                element.click();
            }
            value++;
        });
    };
    this.selectDataToCalculateLatency2 = function () {
        var dataTab = browser.elements("[data-test-id='list-line-center']");
        var objectCount = dataTab.value;
        var value = 1;
        objectCount.forEach(element => {
            if (value == 3 || value == 6) {
                element.click();
            }
            value++;
        });
    };
    this.clickOnShowTimestampsBtn = function() {
        var timestampBtn = browser.element("[data-test-id='Show-timestamp-btn']");
        timestampBtn.click();
    }

    this.clickOnPlotedEvent = function() {
        var eventElement=browser.element("//div[@data-test-id='list-line-center']/div/img[@class='image']");
        eventElement.click();
    }

    this.isDevicePresent = function(value) {
        var sequenceDeviceName = browser.elements("label[data-test-id='"+value+"']");
        return sequenceDeviceName.value.length;
    }
    
    this.isTabPresent = function(value) {
        var tabDeviceName = browser.elements("//div[@class='widget-header']//span[@title='"+value+"']");
        return tabDeviceName.value.length;
    }
    this.getSequenceTabCount = function(value) {
        var tabDeviceCount = browser.elements("//div[@class='widget-header']//span[contains(@title,'"+value+"')]");
        return tabDeviceCount.value.length;
    }
    this.clickOKNoDataMsgBtn = function() {
        if(browser.element(parameter.okBtnNoDataMsg).isVisible()){
            browser.element(parameter.okBtnNoDataMsg).click();
        }
    }
    this.clickColumnBtn = function() {
        if(browser.element(parameter.columnsBtn).isVisible()){
            browser.element(parameter.columnsBtn).click();
        }
    }
};
module.exports=new sequence_diagram(); 