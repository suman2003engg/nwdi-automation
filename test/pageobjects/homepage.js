var homepage = function(){
    this.enterFieldValue=function(value){
        browser.setValue('input[name="username"]',value,100);
    };
    this.pageElement=function(){
        element(by.name('username'));
    }
    this.getTitleText=function(){
        return browser.element('span[class="app-logo-title"]').getText();
    };
    this.clickContinue=function(){
        //element(by.buttonText('Continue')).click();
        browser.element("//button[@type='submit']").click();
    };
    this.enterpasswordValue=function(value){
        browser.element('input[name="password"]',value,100);
    }
};
module.exports=new homepage();