let genericObject = require("../pageobjects/genericobjects");
let parameter = require('../specs/parameters.js');
let baseMethods = require('../specs/testbase');
var messageObject = require('../pageobjects/messagePage');
let config = require('../config');
var expect = require('chai').expect;
var assert = require('assert');
var templatesPage = function(){
    this.compareScopeDevices=function(value){
        return browser.element("[data-test-id='" + value +"']").isVisible();
    };
    this.repeatforScopeTest=function(val_1, val_2){
        genericObject.clickOnDataMenu();
        baseMethods.clickOnSearchViewIcon(val_1);
        var modalBox = browser.element(parameter.modalBox).isVisible();
        if (modalBox == true) {
            browser.element(val_2).click();
            browser.pause(200);
            if (browser.element(parameter.dataScopeReplaceOKBtn).isVisible()){
                genericObject.clickOnReplaceOkWithoutSaving();
                baseMethods.verifySpinnerVisibility();
                browser.pause(5000);
            }
        }
        //verify messages box filetered value retained
        var serchVal = browser.element(parameter.FilterMessages).getValue();
        assert.equal(serchVal, config.findMessageFilterValueInvalid);
        //verify No data Available msg retained
        var newnoDatamsg = browser.element(parameter.noData).isVisible();
        assert.equal(newnoDatamsg, true);
        //clear the filter and verify messages grid
        messageObject.clearFilterContent();
        var rowCount = browser.elements(parameter.MessagesRowContent).value.length;
        if (rowCount > 1){
            assert.ok(rowCount);
        }
        else{
            assert.fail();
        }        
    };
};

module.exports=new templatesPage();