let baseMethods = require('../specs/testbase.js');
let genericObject = require('../specs/generic_objects.js');
let parameter = require('../specs/parameters.js');
var searchdataObject = require('../pageobjects/searchDataPage.js');
var config = require('../config.js');
var assert = require('assert');
var expect = require('chai').expect;
var workspacespage = function(){

    this.deleteWorkspace=function(value){
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        var spinner = browser.element("[data-test-id='spinner']").isVisible();
        if(spinner == true) {
        baseMethods.verifySpinnerVisibility();
        }
        browser.element("[data-test-id='"+value+'Delete'+"']").click();
        browser.element(parameter.workspaceDeleteBtn).click();
        browser.pause(1000);
        var workspacedeleted = browser.element("[data-test-id='"+value+"']").isVisible();
        return workspacedeleted;
    };

    this.saveWorkspace=function(value){
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element(parameter.workspaceSaveAsBtn).click();
        searchdataObject.enterSaveWorkspaceNameValue(value);
        browser.element(parameter.workspaceSaveBtn).click();
        browser.pause(1500);
    };

    this.openSavedWorkspace=function(value){
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element("[data-test-id='"+value+"']").click();
        browser.element(parameter.workspaceOpenBtn).click();
        browser.pause(1000);
        var spinner = browser.element("[data-test-id='spinner']").isVisible();
        if(spinner == true) {
        baseMethods.verifySpinnerVisibility();
        }
        browser.pause(3000);
    };


    this.validateDeleteWindowWarningMsgs=function(value){
        var  workspaceCancelBtn = browser.element("[data-test-id="+value+"delete-msg]").getText();
        assert.equal(config.wspaceDelWarningText+value, workspaceCancelBtn);
        var workspaceDelWarningMsg = browser.element("[data-test-id='ws-delete-msg']").getText();
        assert.equal(config.wspaceDelWarningMsg, workspaceDelWarningMsg);
    };

    this.validateSpecialCharacters=function(value){
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element(parameter.workspaceSaveAsBtn).click();
        searchdataObject.enterSaveWorkspaceNameValue(value);
        browser.pause(500);
        var verifyMsg= browser.element(parameter.workspaceINvalidMsg).getText();
        assert.equal(config.workspaceInvalidMsg, verifyMsg);
        browser.pause(1000);        
        browser.element(parameter.workspaceCancelBtn).click();
        browser.element(parameter.incorrectQueryClose).click();
    };

};

module.exports=new workspacespage();