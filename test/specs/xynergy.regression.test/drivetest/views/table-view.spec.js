let baseMethods = require('../../../testbase.js');
let genericObject= require("../../../generic_objects.js");
let parameter = require("../../../parameters.js");
let xynergyconstants = require("../../../test_xynergy.constants.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');

describe('views', function () {
    beforeEach(function () {
        browser.url('/account/login');
        browser.setValue('input[name="username"]', 'demotest', 100);
        browser.setValue('input[name="password"]', 'Xceed123', 100);
        var submitButton = browser.element("//button[@type='submit']");
        submitButton.click();
        //baseMethods.verifySpinnerVisibility();
        browser.pause(2000);
        browser.windowHandleMaximize();
        browser.waitForExist("[data-test-id='Analytics View']");
    });

    describe('table-view as selected widget', function () {

        it('should verify table-view of event plot', function () {
            console.log("should verify table-view of event plot-started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.TableDraggable,parameter.WidgetRightBox);
            baseMethods.plotEvent(xynergyconstants.VoiceStartEvent,parameter.VoiceStartEvent,parameter.TableDroppable);
            //baseMethods.verifySpinnerVisibility();
            var value = baseMethods.verifyData();
            assert.equal(value, true);
            console.log("should verify table-view of event plot -done");
        });

        it('should verify table-view of metric plot', function () {
            console.log("should verify metric info data table-started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.TableDraggable,parameter.WidgetRightBox);
            baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.TableDroppable);
            //baseMethods.verifySpinnerVisibility();
            var value = baseMethods.verifyData();
            assert.equal(value, true);
            console.log("should verify table-view of metric plot -done");
        });

        it('should change table-view menu to metric menu on metric plot', function () {
            console.log("should change table-view menu to metric menu on metric plot");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.TableDraggable,parameter.WidgetRightBox);
            baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.TableDroppable);
            //baseMethods.verifySpinnerVisibility();
            var value = baseMethods.verifyRightMenuHeader("Metric View");
            assert.equal(value, true);
            console.log("should change table-view menu to metric menu on metric plot -done");
        });

        it('should change table-view menu to event menu on event plot', function () {
            console.log("should change table-view menu to event menu on event plot-started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.TableDraggable,parameter.WidgetRightBox);
            baseMethods.plotEvent(xynergyconstants.VoiceStartEvent,parameter.VoiceStartEvent,parameter.TableDroppable);
            //baseMethods.verifySpinnerVisibility();
            var value = baseMethods.verifyRightMenuHeader("Event View");
            assert.equal(value, true);
            console.log("should change table-view menu to event menu on event plot -done");
        });
    });
});