let baseMethods = require('../../../testbase.js');
let xynergy_Constants = require("../../../test_xynergy.constants.js");
let genericObject = require("../../../generic_objects.js");
let parameter = require("../../../parameters.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');

describe('views menu', function () {
    beforeEach(function () {
        browser.url('/account/login');
        browser.setValue('input[name="username"]', 'demotest', 100);
        browser.setValue('input[name="password"]', 'Xceed123', 100);
        var submitButton = browser.element("//button[@type='submit']");
        submitButton.click();
        //baseMethods.verifySpinnerVisibility();
        browser.pause(2000);
        browser.windowHandleMaximize();
        browser.waitForExist("[data-test-id='Analytics View']");
    });

    it('should have drive test header under layer component', function () {
        console.log("should have drive test header under layer component -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable, parameter.WidgetRoot);
        browser.pause(1000);
        baseMethods.clickOnLayerMenuBtn();
        var value = baseMethods.verifyLayerData(xynergy_Constants.Drive_Test);
        assert.equal(value, true);
        console.log("should have drive test header under layer component -done");
    });

    it('should have map menu widget', function () {
        console.log("should have map menu widget -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        var mapTab = browser.isVisible("[data-test-id='WidgetUniqueIdXynergyMapComponent']");
        assert.equal(mapTab, true);
        console.log("should have map menu widget -done");
    });

    it('should have layer3 messages menu widget', function () {
        console.log("should have layer3 messages menu widget -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        var messagesTab = browser.isVisible("[data-test-id='WidgetUniqueIdL3PaneComponent']");
        assert.equal(messagesTab, true);
        console.log("should have layer3 messages menu widget -done");
    });

    it('should have time series menu widget', function () {
        console.log("should have time series menu widget -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        var timeseriesTab = browser.isVisible("[data-test-id='WidgetUniqueIdTimeSeriesComponent']");
        assert.equal(timeseriesTab, true);
        console.log("should have time series menu widget -done");
    });

    it('should verify the buttons of close-all-views option', function () {
        console.log("should verify the buttons of close-all-views option -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable, parameter.WidgetRoot);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MessagesDraggable, parameter.WidgetRightBox);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.clickOnViewsCloseAllBtn();
        var saveAsBtn = browser.isVisible("[data-test-id='save-as-btn']");
        var closeAllViewsBtn = browser.isVisible("[data-test-id='close-all-views']");
        var cancelBtn = browser.isVisible("[data-test-id='cancel-btn']");
        assert.equal(saveAsBtn && closeAllViewsBtn && cancelBtn, true);
        console.log("should verify the buttons of close-all-views option -done");
    });

    // it('should verify the save-as button of close-all-views option', function () {
    //     console.log("should verify the save-as button of close-all-views option -started");
    //     this.skip();
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     baseMethods.clickOnViewsCloseAllBtn();
    //     baseMethods.clickOnViewsSaveAsBtn();
    //     baseMethods.uniqueWorkspaceName();
    //     var inputTab = browser.element("[data-test-id='ws-name-input']");
    //     inputTab.click();
    //     baseMethods.clickOnWorkspaceSaveOKBtn();
    //     //baseMethods.verifySpinnerVisibility();
    //     browser.pause(1000);
    //     var workspace = browser.element("[data-test-id='Workspaces']");
    //     workspace.click();
    //     var verifyWorkspace = browser.element("[data-test-id='Test']").isExisting();
    //     assert.equal(verifyWorkspace, true);
    //     console.log("should verify the save-as button of close-all-views option -done");
    // });

    it('should verify the cancel button of close-all-views option', function () {
        console.log("should verify the cancel button of close-all-views option -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable, parameter.WidgetRoot);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MessagesDraggable, parameter.WidgetRightBox);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.clickOnViewsCloseAllBtn();
        var cancelBtn = browser.element("[data-test-id='cancel-btn']");
        cancelBtn.click();
        var verifyWorkspace = browser.element("[data-test-id='Close All Views-Title-Bar']").isExisting();
        assert.equal(verifyWorkspace, false);
        console.log("should verify the cancel button of close-all-views option -done");
    });
});
