let baseMethods = require('../../../testbase.js');
let genericObject = require('../../../generic_objects.js');
let parameter=require('../../../parameters.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');

describe('views', function () {
    beforeEach(function () {
        browser.url('/account/login');
        browser.setValue('input[name="username"]', 'demotest', 100);
        browser.setValue('input[name="password"]', 'Xceed123', 100);
        var submitButton = browser.element("//button[@type='submit']");
        submitButton.click();
        //baseMethods.verifySpinnerVisibility();
        browser.windowHandleMaximize();
        browser.waitForExist("[data-test-id='Analytics View']");
    });

    describe('map as selected widget', function () {

        it('should have drive route on map', function () {
            console.log("should have drive route on map -started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
            baseMethods.clickOnLayerMenuBtn();
            var value = baseMethods.verifyLayerData("Drive Route");
            assert.equal(value, true);
            console.log("should have drive route on map -done");
        });

        it('should display map menu', function () {
            console.log("should display map menu -started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
            var value = baseMethods.verifyRightMenuHeader("Map");
            assert.equal(value, true);
            console.log("should display map menu -done");
        });

        it('should have map layers menu under layer component', function () {
            console.log("should have map layers menu under layer component -started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
            browser.waitForVisible("[data-test-id='IconUniqueDataIdLegend']", 10000);
            var mapMenu = browser.element("[data-test-id='IconUniqueDataIdLegend']");
            mapMenu.click();
            var value = baseMethods.verifyDTMenuHeader("Map Layers");
            assert.equal(value, true);
            console.log("should have map layers menu under layer component -done");
        });

        // it('should open info menu on click of bin-tootip viewmore button', function () {
        //     console.log("should open info menu on click of bin-tootip viewmore button -started");
        //     baseMethods.searchTRDMDataSet();
        //     baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
        //      baseMethods.plotMetric(xynergyconstants.RSRPMetric,parameter.RSRPMetric,parameter.WidgetRoot);
        //     browser.pause(100);
        //     var count = baseMethods.verifyBinTooltipData();
        //     assert.equal(count, 3);
        //     console.log("should open info menu on click of bin-tootip viewmore button -started");

        
        // it('should open info menu on click of tootip viewmore button', function () {
        //     baseMethods.clickOnAdvancedSearchViewIcon();
        //     baseMethods.clickOnOpenInCanvasBtn();
        //     baseMethods.waitTilDataLoads();
        //     baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
        //    //baseMethods.clickOnMapBin();
        //    //browser.timeoutsAsyncScript(9000);
        //    browser.timeoutsAsyncScript(100000);
        //     var mapComponent = browser.executeAsync(function(done) {
        //             console.log("entered");
        //             var elem;
        //             function handleMapResponse(event) {
        //                 console.log('handle map response called');
        //                 elem.removeEventListener('testHookCallback', handleMapResponse);
        //                 done(event);
        //             }
        //             elem = document.querySelector("[data-automation-id='mapTestHook']");
        //             elem.addEventListener('testHookCallback', handleMapResponse);
        //             elem.click();
        //    });
        //     mapComponent = mapComponent.value;
        //     var mapResponsePromise = new Promise((resolve, reject) => {
        //         console.log('promise called');
        //         mapComponent.addEventListener('testHookCallback', handleMapResponse);
        //     });
        //     baseMethods.clickOnBinViewmoreBtn();
        //     var value = baseMethods.verifyDTMenuHeader("Info: Event");
        //     assert.equal(value, true);
        // });

    });

});


