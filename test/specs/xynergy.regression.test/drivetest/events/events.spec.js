let baseMethods = require('../../../testbase.js');
let genericObject= require("../../../generic_objects.js");
let parameter=require("../../../parameters.js");
let xynergyconstants = require("../../../test_xynergy.constants.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');

describe('drive-test events menu', function () {
    beforeEach(function () {
        browser.url('/account/login');
        browser.setValue('input[name="username"]', 'demotest', 100);
        browser.setValue('input[name="password"]', 'Xceed123', 100);
        var submitButton = browser.element("//button[@type='submit']");
        submitButton.click();
        //baseMethods.verifySpinnerVisibility();
        browser.windowHandleMaximize();
        browser.waitForExist("[data-test-id='Analytics View']");
    });  

    it('should have drag and drop message', function () {
        console.log("should have drag and drop message -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.EventsMenu);
        var notificationPanel = browser.element("[data-test-id='Events-notification-panel']");
        var notificationPanelText = notificationPanel.getText();
        assert.equal("Drag and drop items from below to views.", notificationPanelText);
        console.log("should have drag and drop message -done");
    });
    
    it('should render Voice Call Start event', function () {
        console.log("should render Voice Call Start event' -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
        baseMethods.plotEvent(xynergyconstants.VoiceStartEvent,parameter.VoiceStartEvent,parameter.PlotAcrossWidgets);
        // browser.pause(100);
        // var okBtn=browser.element("[data-test-id='ok-btn']");
        // okBtn.click();
        // browser.pause(1500);
        baseMethods.clickOnLayerMenuBtn();
        var layerValue = baseMethods.verifyEventsLayerData(xynergyconstants.VoiceStartEvent);
        assert.equal(layerValue, true);
        console.log("should rende Voice Call Start event' -done");
    });

    it('should render multiple events', function () {
        console.log("should render multiple events' -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
        baseMethods.plotEvent(xynergyconstants.VoiceStartEvent,parameter.VoiceStartEvent,parameter.PlotAcrossWidgets);
        // browser.pause(100);
        // var okBtn=browser.element("[data-test-id='ok-btn']");
        // okBtn.click();
        // browser.pause(1500);
        baseMethods.plotEvent(xynergyconstants.VoiceSetupEvent,parameter.VoiceSetupEvent,parameter.PlotAcrossWidgets);
        // browser.pause(100);
        // var okBtn=browser.element("[data-test-id='ok-btn']");
        // okBtn.click();
        // browser.pause(1500);
        baseMethods.clickOnLayerMenuBtn();
        var layerValue = baseMethods.verifyEventsLayerData(xynergyconstants.VoiceStartEvent);
        var layerTabValue = baseMethods.verifyEventsLayerData(xynergyconstants.VoiceSetupEvent);
        assert.equal(layerValue && layerTabValue, true);
        console.log("should render multiple events' -done");
    });
      
    it('render Voice Call Start event and verify event tab in layer component', function () {
        console.log("render Voice Call Start event and verify event tab in layer component -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
        baseMethods.plotEvent(xynergyconstants.VoiceStartEvent,parameter.VoiceStartEvent,parameter.PlotAcrossWidgets);
        // browser.pause(100);
        // var okBtn=browser.element("[data-test-id='ok-btn']");
        // okBtn.click();
        // browser.pause(1500);
        baseMethods.clickOnLayerMenuBtn();
        var value = baseMethods.verifyDTMenuHeader("Events");
        assert.equal(value, true);
        console.log("render Voice Call Start event and verify event tab in layer component -done");
    });
    
    it('should have search bar', function () {
        console.log("should have search bar -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.EventsMenu);
        var searchtabValue = false;
        if (browser.isVisible("[data-test-id='Events-search-box']")) {
            searchtabValue = true;
        }
        assert.equal(searchtabValue, true);
        console.log("should have search bar -done");
    });

});