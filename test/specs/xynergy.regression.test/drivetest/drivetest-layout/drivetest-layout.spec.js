let baseMethods = require('../../../testbase.js');
let genericObject= require("../../../generic_objects.js");
let parameter = require("../../../parameters.js");
let xynergy_Constants= require("../../../test_xynergy.constants.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');

describe('drive-test page', function () {

    beforeEach(function () {
        browser.url('/account/login');
        browser.setValue('input[name="username"]', 'demotest', 100);
        browser.setValue('input[name="password"]', 'Xceed123', 100);
        var submitButton = browser.element("//button[@type='submit']");
        submitButton.click();
        //baseMethods.verifySpinnerVisibility();
        browser.windowHandleMaximize();
        browser.waitForExist("[data-test-id='Analytics View']");
        baseMethods.checkForWorkspaceWidget();
    });

    it('should have metrics menu', function () {
        console.log("should have metrics menu -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        var assertionvalue = baseMethods.verifyDTMenuHeader("Metrics");
        assert.equal(assertionvalue, true);
        console.log("should have metrics menu -done");
    });

    it('should have events menu', function () {
        console.log("should have events menu' -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.EventsMenu);
        var value = baseMethods.verifyDTMenuHeader("Events");
        assert.equal(value, true);
        console.log("should have events menu' -done");
    });

    it('should have views menu', function () {
        console.log("should have views menu -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        var value = baseMethods.verifyDTMenuHeader("Views");
        assert.equal(value, true);
        console.log("should have views menu -done");
    });

    it('should have scope menu', function () {
        console.log("should have data menu -started");
        baseMethods.clickOnAnalyticsView();
        var value = baseMethods.verifyDTMenuHeader("Scope");
        assert.equal(value, true);
        console.log("should have data menu -done");
    });

    it('should have search bar under data pane', function () {
        console.log("should have search bar under data pane -started");
        baseMethods.clickOnAnalyticsView();
        var searchtabValue = false;
        if (browser.isVisible("[data-test-id='Data-search-box']")) {
            searchtabValue = true;
        }
        assert.equal(searchtabValue, true);
        console.log("should have search bar under data pane -done");
    });

    it('should have drag and drop message on map canvas', function () {
        console.log("should have drag and drop message on map canvas -started");
        baseMethods.clickOnAnalyticsView();
        baseMethods.clickOnDataCloseBtn();
        genericObject.clickOnDTTab(parameter.EventsMenu);
        var notificationPanel = browser.element("[data-test-id='Events-notification-panel']");
        var notificationPanelText = notificationPanel.getText();
        assert.equal("Drag and drop items from below to views.", notificationPanelText);
        console.log("should have drag and drop message on map canvas -done");
    });

    it('should render layer3 meassages tab as new tab on drive test page', function () {
        console.log("should render layer3 meassages tab as new tab on drive test page -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
        browser.pause(2000);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MessagesDraggable,parameter.WidgetTopBox);
        browser.waitForVisible("[data-test-id='right-tab-menu-header']", 20000);
        var mapValue = baseMethods.verifyRightMenuHeader("Map");
        var messageValue = baseMethods.verifyRightMenuHeader("Messages");
        assert.equal(mapValue && messageValue, true);
        console.log("should render layer3 meassages tab as new tab on drive test page -done");
    });

    it('should replace map with layer3 messages tab', function () {
        console.log("verify replacing map with layer3 messages tab -started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
        browser.pause(2000);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MessagesDraggable,parameter.WidgetCenterBottomBox);
        browser.waitForVisible("[data-test-id='right-tab-menu-header']", 20000);
        var mapValue = baseMethods.verifyRightMenuHeader("Map");
        var messageValue = baseMethods.verifyRightMenuHeader("Messages");
        assert.equal(messageValue, true);
        assert.equal(mapValue, false);
        console.log("verify replacing map with layer3 messages tab -done");
    });

    it('should display Info:Bin tab on click of info button', function () {
        console.log("should display Info:Bin tab on click of info button -started");
        baseMethods.clickOnAnalyticsView();
        baseMethods.clickOnDataCloseBtn();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
        baseMethods.clickOnInfoTab();
        var value = baseMethods.verifyDTMenuHeader("Info: Bin");
        assert.equal(value, true);
        console.log("should display Info:Bin tab on click of info button -done");
    });

    it('should display Info:Event tab on click of info button', function () {
        console.log("verify Info:Event tab on click of info button -started");
        baseMethods.clickOnAnalyticsView();
        baseMethods.clickOnDataCloseBtn();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
        baseMethods.clickOnInfoTab();
        var value = baseMethods.verifyDTMenuHeader("Info: Event");
        assert.equal(value, true);
        console.log("verify Info:Event tab on click of info buttont -done");
    });

});