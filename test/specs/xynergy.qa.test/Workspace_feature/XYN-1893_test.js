let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var browseDataObject = require('../../../pageobjects/browseDataPage.js');
var workspacesObject = require('../../../pageobjects/workspacesPage.js');
var messageObject = require('../../../pageobjects/messagePage.js');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;require('../../commons/global.js')();

describe('Workspace page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });


    it('To Verify Restoring Diagnostics View replaced Message view from workspace: XYN-1893' ,function(){
        console.log("Test Started - Restoring Diagnostics View replaced Message view from workspace: XYN-1893");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchALFDataset);
        searchdataObject.selectdatasetresult();
        browser.element(parameter.searchDataOpenBtn).click();
        baseMethods.openInCanvasValidation();
        //browser.pause(500);
        //baseMethods.verifySpinnerVisibility();
        //console.log("1");        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.DiagnostaticsDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TableDraggable, parameter.WidgetRightBox);
        browser.execute(() => {
            let obj = document.querySelector("[data-test-id='center-box-bottom']");
            obj.outerHTML = ''
        });
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MessagesDraggable, parameter.WidgetCenterBottomBox);
        baseMethods.verifySpinnerVisibility();
        browser.pause(1000);
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        workspacesObject.saveWorkspace(workspacename);
        //baseMethods.saveWorkspace(workspacename);
        var workspacesaved = browser.element("[data-test-id='"+workspacename+"']").isVisible();
        assert(workspacesaved,true);
        browser.element(parameter.incorrectQueryClose).click();
        baseMethods.closeAllViews();
        workspacesObject.openSavedWorkspace(workspacename);
        var diagnosticsview = browser.element(parameter.viewDiagnostics).isVisible();
        assert.equal(diagnosticsview,true);
        var messageview = browser.element(parameter.viewMessages).isVisible();
        assert.equal(messageview,true);
        baseMethods.closeAllViews();
        var wspacedel = workspacesObject.deleteWorkspace(workspacename);
        assert.equal(wspacedel,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        searchdataObject.newSearchPage();
        console.log("Test Completed - Verified Restoring Diagnostics View replaced Message view from workspace: XYN-1893");

    });

});