let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var browseDataObject = require('../../../pageobjects/browseDataPage.js');
var workspacesObject = require('../../../pageobjects/workspacesPage.js');
var messageObject = require('../../../pageobjects/messagePage.js');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;require('../../commons/global.js')();

describe('Workspace page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });

    it('To verify Timeseries workspace with two metrics: XYN-1720' ,function(){
    console.log("Test Started - To verify Timeseries workspace with two metrics: XYN-1720");              
    baseMethods.SelectTabs("BROWSEDATA");
    browseDataObject.LastFilters(config.LastFilterDaysValue);
    browseDataObject.LastdropdownDays();
    browseDataObject.clickApplyBtn();
    browser.pause(200);
    baseMethods.selectdatasetinBrowseDatasets(config.searchDatasetForRegularTest);
    var open = browser.element(parameter.datasetsOpenBtn);
    open.click();
    baseMethods.openInCanvasValidation();
    baseMethods.verifySpinnerVisibility();
    genericObject.clickOnDTTab(parameter.ViewsMenu);        
    baseMethods.dragAndDrop(parameter.TimeseriesDraggable, parameter.FreshViewsDroppable);
    baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
    baseMethods.verifySpinnerVisibility();
    browser.element(parameter.TimeseriesLayerButton).click();        
    var metricPlot = baseMethods.verifyTimeSeriesMetricLayerData(config.metricName);
    assert.equal(metricPlot, true);        
    browser.element(parameter.incorrectQueryClose).click();
    baseMethods.plotMetric(config.metricNameRSRQ, parameter.RSRQMetric, parameter.PlotAcrossWidgets);                
    baseMethods.verifySpinnerVisibility();
    browser.element(parameter.TimeseriesLayerButton).click();
    var metricPlot2 = baseMethods.verifyTimeSeriesMetricLayerData(config.metricNameRSRQ);
    assert.equal(metricPlot2, true);
    browser.element(parameter.incorrectQueryClose).click();
    genericObject.clickOnDTTab(parameter.workspacesMenu);
    browser.element(parameter.workspaceSaveAsBtn).click();
    var workspacename = baseMethods.saveFileName("a");
    console.log(workspacename);
    searchdataObject.enterSaveWorkspaceNameValue(workspacename);
    browser.element(parameter.workspaceSaveBtn).click();
    browser.pause(1000);
    browser.element(parameter.incorrectQueryClose).click();        
    baseMethods.closeAllViews();        
    browser.pause(500);        
    workspacesObject.openSavedWorkspace(workspacename);
    //There is a bug in application workflow, so commiting out below and will enable once it fix.
    //browser.element(parameter.TimeseriesLayerButton).click();      
    var metricPlot3 = baseMethods.verifyTimeSeriesMetricLayerData(config.metricName);
    assert.equal(metricPlot3, true);
    console.log("Metric Plot Verified");
    browser.pause(1000);
    var metricplot4 = baseMethods.verifyTimeSeriesMetricLayerData(config.metricNameRSRQ);
    assert.equal(metricplot4, true);
    console.log("Event Plot Verified");
    baseMethods.closeAllViews();        
    browser.pause(500);
    var wspacedel = workspacesObject.deleteWorkspace(workspacename);
    assert.equal(wspacedel,false);
    console.log('Workspace ' + workspacename + ' is deleted successfully');
    browser.element(parameter.incorrectQueryClose).click();
    browser.element(parameter.DataMenu).click();
    var checkBox = browser.element(parameter.openBtn);
    if(!(checkBox.getAttribute("class")).includes('button-disabled')){
        browser.element(parameter.selectCheckBox).click();
        browser.element(parameter.datasetsFilterClear).click();
    }
    else{
        browser.element(parameter.datasetsFilterClear).click();
    }                 
    
    console.log("Test Completed - Verified Timeseries workspace with two metrics: XYN-1720");
    });
});