let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var browseDataObject = require('../../../pageobjects/browseDataPage.js');
var workspacesObject = require('../../../pageobjects/workspacesPage.js');
var messageObject = require('../../../pageobjects/messagePage.js');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;require('../../commons/global.js')();

describe('Workspace page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });

    it('To verify if user able to open the saved workspace: XYN-1724' ,function(){
        console.log("Test Started - To verify if user able to open the saved workspace: XYN-1724");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchDatasetForRegularTest);
        searchdataObject.selectdatasetresult();
        browser.element(parameter.searchDataOpenBtn).click();
        baseMethods.openInCanvasValidation();
        //browser.pause(500);
        baseMethods.verifySpinnerVisibility();
        //console.log("1");        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        browser.element(parameter.mapLayersTab).click();
        browser.element(parameter.dockpinPinned).click();
        browser.pause(500);
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.pause(500);
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        workspacesObject.saveWorkspace(workspacename);
        var workspacesaved = browser.element("[data-test-id='"+workspacename+"']").isVisible();
        assert(workspacesaved,true);
        browser.element(parameter.incorrectQueryClose).click();
        baseMethods.closeAllViews();
        workspacesObject.openSavedWorkspace(workspacename);
        var checktab = browser.elements("[title='Map']").getText();
        console.log(checktab);
        expect(checktab).to.include("Map");
        //browser.element(parameter.dataLayers).click();
        //browser.element(parameter.mapLayersTab).click();
        browser.pause(1500);
        var layerValue2 = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue2,true);
        console.log("verifiedRSRP");
        browser.pause(500);
        baseMethods.closeAllViews();
        var workspacedeleted = workspacesObject.deleteWorkspace(workspacename);
        assert.equal(workspacedeleted,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        var checkBox = browser.element(parameter.openBtn);
        searchdataObject.newSearchPage();
        console.log("Test Completed - Verified if user able to open the saved workspace: XYN-1724");
    });

});
