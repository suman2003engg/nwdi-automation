let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var browseDataObject = require('../../../pageobjects/browseDataPage.js');
var workspacesObject = require('../../../pageobjects/workspacesPage.js');
var scopeDataObject = require('../../../pageobjects/scopeWidgetPage.js');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;require('../../commons/global.js')();

describe('Workspace',function(){
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
    });
    it('Workspace - To Verify Workspace with Parameter comparison - split views: XYN-2878' ,function(){
    console.log("Test Started - Workspace - To Verify Workspace with Parameter comparison - split views: XYN-2878");                  
    /* Drop Parameter comparison view on AV*/
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
    baseMethods.verifySpinnerVisibility(); 
    browser.waitForExist(parameter.mBoxTitleId);                       
    var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
    //if modelbox opened
    var cellName = [];
    var file = [];
    if (mboxOpened){
        //click 4 parameters to compare
        for (var i=1; i<5; i++){
            parameterComparisonObject.clickCheckBox(i);
        }            
        parameterComparisonObject.clickOKBtn();
        baseMethods.verifySpinnerVisibility();
    }    
    //drop MessagesView
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.WidgetRightBox);
    baseMethods.verifySpinnerVisibility();
    /* Save the view state as workspace and close all views */
    genericObject.clickOnDTTab(parameter.workspacesMenu);
    browser.element(parameter.workspaceSaveAsBtn).click();
    var workspacename = baseMethods.saveFileName("a");
    console.log(workspacename);
    searchdataObject.enterSaveWorkspaceNameValue(workspacename);
    browser.element(parameter.workspaceSaveBtn).click();
    browser.waitForExist("[data-test-id='"+workspacename+"']");
    browser.element(parameter.incorrectQueryClose).click();        
    baseMethods.closeAllViews();        
    browser.pause(500);        
    /* Open saved workspace and verify if it saved with latest changes made on the view*/
    workspacesObject.openSavedWorkspace(workspacename);
    baseMethods.verifySpinnerVisibility();
    browser.pause(500);
    browser.waitForExist(parameter.viewMessages);
    browser.waitForExist(parameter.viewParameterComparison);
    /* Close all views and delete the workspace */
    baseMethods.closeAllViews();        
    browser.pause(500);
    var wspacedel = workspacesObject.deleteWorkspace(workspacename);
    assert.equal(wspacedel,false);
    console.log('Workspace ' + workspacename + ' is deleted successfully');
    console.log("Test Completed - Workspace - To Verify Workspace with Parameter comparison - split views: XYN-2878");
    });
});