let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var browseDataObject = require('../../../pageobjects/browseDataPage.js');
var workspacesObject = require('../../../pageobjects/workspacesPage.js');
var messageObject = require('../../../pageobjects/messagePage.js');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;require('../../commons/global.js')();

describe('Workspace page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });

    it('To verify workspace with Diagnostic View - info pane closed: XYN-2883' ,function(){
        console.log("Test Started - To verify workspace with Diagnostic View - info pane closed: XYN-2883");
            /* Import Diagnostics dataset to AV */
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchALFDataset);
        searchdataObject.selectdatasetresult();
        browser.element(parameter.searchDataOpenBtn).click();
        baseMethods.openInCanvasValidation();
        //browser.pause(500);
        baseMethods.verifySpinnerVisibility();
        //console.log("1");        
            /* Drop diagnostics view on AV and verify if extended pane is open by default and hide it */
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.DiagnostaticsDraggable, parameter.FreshViewsDroppable);
        var tabstatus = browser.element(parameter.diagnosisExtendedPanel);
        var enabled = false;
        if((tabstatus.getAttribute("class")).includes('active')) {
            enabled = true;
        }
        assert.equal(enabled,true);
        tabstatus.click();
        var enabled1 = false;
        if((tabstatus.getAttribute("class")).includes('active')) {
            enabled1 = true;
        }
        assert.notEqual(enabled1,true);
            /* Save the view as workspace and close all views */
        var workspacename = baseMethods.saveFileName("a");
        console.log(workspacename);
        workspacesObject.saveWorkspace(workspacename);
        var workspacesaved = browser.element("[data-test-id='"+workspacename+"']").isVisible();
        assert(workspacesaved,true);
        browser.element(parameter.incorrectQueryClose).click();
        baseMethods.closeAllViews();
            /* Open saved workspace and verify if extended pane hidden state is remembered when workspace restores */
        workspacesObject.openSavedWorkspace(workspacename);
        var tabstatus1 = browser.element(parameter.diagnosisExtendedPanel);
        var enabled2 = false;
        if((tabstatus1.getAttribute("class")).includes('active')) {
            enabled2 = true;
        }
        assert.notEqual(enabled2,true);
            /* Close all views and delete the workspace */
        baseMethods.closeAllViews();
        var wspacedel = workspacesObject.deleteWorkspace(workspacename);
        assert.equal(wspacedel,false);
        console.log('Workspace ' + workspacename + ' is deleted successfully');
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        searchdataObject.newSearchPage();
        
        console.log("Test Completed - Verified workspace with Diagnostic View - info pane closed: XYN-2883");

    });

});