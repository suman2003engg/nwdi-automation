let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter=require('../../parameters.js');
let xynergyconstants=require('../../test_xynergy.constants.js');
let timeseriesObject=require("../../../pageobjects/timeseries.js");
var assert = require('assert');
var expect = require('chai').expect;
let config=require('../../../config.js');
require('../../commons/global.js')();

describe('Time Series',function(){
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
    });
    it('Verify if events hide/unhide button and close button is available in plotted data layers in Time series - XYN-3031', function(){
        console.log("Test started - Verify if events hide/unhide button and close button is available in plotted data layers in Time series - XYN-3031");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TimeseriesDraggable,parameter.FreshViewsDroppable);
        baseMethods.plotEvent(config.eventName,parameter.VoiceStartEvent,parameter.TimeseriesDroppable);
        timeseriesObject.clickOnTimeSeriesLayerBtn();       
        browser.pause(200);
        var tabName=browser.elements("//tab[@name='Plotted Data Layers']");
        var closeButton=tabName.element("//tab[@name='Events']//*[@data-test-id='close-btn']");
        var hideButton=tabName.element("[data-test-id='hide-btn']");
        console.log(closeButton);
        assert.equal(closeButton.isExisting(),true);
        assert.equal(hideButton.isExisting(),true);
        console.log("Test competed - Verify if events hide/unhide button and close button is available in plotted data layers in Time series- XYN-3031");
    });    
});