let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
var tableViewObject=require('../../../pageobjects/tableViewPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
let eventsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let xynergyconstants=require('../../test_xynergy.constants.js');
let genericObject=require('../../generic_objects.js');
let config=require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var conf = require('../../../config');
var expect = require('chai').expect;require('../../commons/global')();

describe('Table View',function(){
    beforeEach(function () {
        if (!isElectron()) {
            console.log("Logging into the application");
            baseMethods.loginToApplication();
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
    });

    it('To verify whether filter is working accordingly - XYN-1666', function () {
        console.log("Test started to verify filter functionality according to Device name");        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        var selectRowElement = browser.element(parameter.SelectColumn);
        selectRowElement.click();
        browser.pause(300);
        var selectTime = browser.element(parameter.DeviceName);
        selectTime.click();
        tableViewObject.enterTableFilterValue(conf.findTableViewFilterValue1);
        browser.pause(300);
        var j = 1;
        rowCount = browser.elements(parameter.gridRows).value.length;
        console.log(rowCount);
        while (rowCount > 2) {
            var row = browser.elements("//datatable-row-wrapper[" + j +"]");
            var rowContent=row.element("//datatable-body-cell[3]");
            console.log(rowContent.getText(),j);
            expect(rowContent.getText()).to.include(conf.findTableViewFilterValue1);
            j++;
            rowCount--;   
        }
    });

    it('To verify whether filter is working accordingly - XYN-1666', function () {
        console.log("Test started to verify filter functionality according to Plotted metric");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        var selectRowElement = browser.element(parameter.SelectColumn);
        selectRowElement.click();
        browser.pause(1000);
        var selectTime = browser.element(parameter.PlottedMetric);
        selectTime.click();
        tableViewObject.enterTableFilterValue(conf.findTableViewFilterValue2);
        browser.pause(300);
        var j = 1;
        rowCount = browser.elements(parameter.gridRows).value.length;
        console.log(rowCount);
        while (rowCount > 2) {
            var row = browser.elements("//datatable-row-wrapper[" + j +"]");
            var rowContent=row.element("//datatable-body-cell[4]");
            console.log(rowContent.getText(),j);
            expect(rowContent.getText()).to.include(conf.findTableViewFilterValue2);
            j++;
            rowCount--;   
        }
    });

});