let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
var tableViewObject=require('../../../pageobjects/tableViewPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
let eventsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let xynergyconstants=require('../../test_xynergy.constants.js');
let genericObject=require('../../generic_objects.js');
let config=require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var conf = require('../../../config');
var expect = require('chai').expect;require('../../commons/global')();

describe('Table View',function(){
    beforeEach(function () {
        if (!isElectron()) {
            console.log("Logging into the application");
            baseMethods.loginToApplication();
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
    });

    it('Show Metric Units in brackets - XYN-3434', function(){
        console.log("Test started - Show Metric Units in brackets - XYN-3434");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        var divElements = browser.elements(parameter.widgetContainer_1);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TimeseriesDraggable, parameter.WidgetRightBox);
        var divElements1 = browser.elements(parameter.widgetContainer_2);
        baseMethods.plotMetric(config.metricNameRSRQ, parameter.RSRQMetric, parameter.PlotAcrossWidgets);        
        baseMethods.verifySpinnerVisibility();
        browser.pause(300);
        //verify for Table view
        var headerRow = browser.element(parameter.MetricNameInHeader).getText();        
        expect(headerRow).to.include(config.BestCellRSRQUnits);
        //Verify for TimeSeries
        timeseries.clickOnTimeSeriesLayerBtn();
        var plottedLayers = browser.element(parameter.TimeseriesLayerData).isVisible();
        if (plottedLayers){
            var plotMetric = browser.elements(parameter.TimeseriesLayerData).getText();
            expect(plotMetric[1]).to.include(config.BestCellRSRQUnits);
        }
        baseMethods.closeTimeSeriesDataLayer();               
        console.log("Test completed - Show Metric Units in brackets - XYN-3434");
    });
});