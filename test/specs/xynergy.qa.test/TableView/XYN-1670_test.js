let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
var tableViewObject=require('../../../pageobjects/tableViewPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
let eventsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let xynergyconstants=require('../../test_xynergy.constants.js');
let genericObject=require('../../generic_objects.js');
let config=require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var conf = require('../../../config');
var expect = require('chai').expect;require('../../commons/global')();

describe('Table View',function(){
    beforeEach(function () {
        if (!isElectron()) {
            console.log("Logging into the application");
            baseMethods.loginToApplication();
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
    });

    it('Verify Full screen feature for Table view - XYN-1670', function(){
        console.log("Verify Full screen feature for Table view - XYN-1670");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        var divElements = browser.elements(parameter.widgetContainer_1);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MapDraggable, parameter.WidgetRightBox);
        var divElements1 = browser.elements(parameter.widgetContainer_2);
        baseMethods.plotMetric(config.metricNameRSRQ, parameter.RSRQMetric, parameter.PlotAcrossWidgets);        
        baseMethods.verifySpinnerVisibility();
        browser.pause(300);
        var styleType = browser.elements(parameter.analyticsview_2).getAttribute(config.style);
        expect(styleType).to.include(config.block);        
        tableViewObject.legendOpenButton();
        var MapLayers = browser.elements(parameter.dataLayers).getText();
        expect(MapLayers[2]).to.include(config.metricNameRSRQ);
        tableViewObject.closeButton();
        tableViewObject.clickFullScreenIcon();
        var newStyleType = browser.elements(parameter.analyticsview_2).getAttribute(config.style);
        expect(newStyleType).to.include(config.none);
        console.log("Verify Full screen feature for Table view - XYN-1670");
    });

});