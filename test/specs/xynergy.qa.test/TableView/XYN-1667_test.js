let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
var tableViewObject=require('../../../pageobjects/tableViewPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
let eventsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let xynergyconstants=require('../../test_xynergy.constants.js');
let genericObject=require('../../generic_objects.js');
let config=require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var conf = require('../../../config');
var expect = require('chai').expect;require('../../commons/global')();

describe('Table View',function(){
    beforeEach(function () {
        if (!isElectron()) {
            console.log("Logging into the application");
            baseMethods.loginToApplication();
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
    });

    it.skip('Verify dropdown in table view - XYN-1667', function(){
        console.log("Test started - XYN-1667 - verification of dropdown in table view");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        browser.pause(3000);
        var selectSearchColumn = browser.element("//select[@data-test-id='grid-filter-select']");
        selectSearchColumn.click();
        console.log("click on select column");
        console.log(selectSearchColumn.value.length);
        var selectAColumn=selectSearchColumn.element("//option[@data-test-id='Time']");
        selectAColumn.click();
        var i=1,contentRows=[];
        console.log("no issue");
        var colSelector=browser.elements("//div[@class='datatable-header-cell col-selector']");
        var checkBoxCount=colSelector.elements("//i[@class='icon-menu-1 icon-small-size pointer-cursor']").value.length;
        var j=1,checkBoxSelect,notChecked=0,n=checkBoxCount;
        while(checkBoxCount>1){
            checkBoxSelect=colSelector.element("//i[@class='icon-menu-1 icon-small-size pointer-cursor']["+j+"]");
            console.log("no issue");
            while(n>0){
                headerColumnName[i]= headerRow.element("//datatable-header-cell["+i+"]");
                console.log(headerColumnName[i].getText());
                var checkboxText=checkBoxSelect.getText();
                if(checkboxText==headerColumnName[i].getText()){
                    notChecked=1;
                    itemToCheck=headerColumnName[i];
                    break;
                }
                n--;
                i++;
            }
            n=checkBoxCount;
            j++;
            if(notChecked==0){
                var checkItem=checkBoxSelect.element("//input[@type='checkbox']");
                checkItem.click();
            }
            checkBoxCount--;
        }
        console.log("Test completed - XYN-1667 - verification of dropdown in table view");
    });

it('Verify Filtering option on table view - XYN-1667', function(){
    console.log("Test started - To Verify Filtering option on table view - XYN-1667");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        var selectRowElement = browser.element(parameter.SelectColumn);
        selectRowElement.click();
        browser.pause(1000);
        var selectTime = browser.element(parameter.Time);
        selectTime.click();
        tableViewObject.enterTableFilterValue(conf.findTableViewFilterValue);
        browser.pause(300);
        var j = 1;
        rowCount = browser.elements(parameter.gridRows).value.length;
        console.log(rowCount);
        while (rowCount > 2) {
            var row = browser.elements("//datatable-row-wrapper[" + j +"]");
            var rowContent=row.element("//datatable-body-cell[2]");
            console.log(rowContent.getText(),j);
            expect(rowContent.getText()).to.include(conf.findTableViewFilterValue);
            j++;
            rowCount--;   
        }
        console.log("Test completed - To Verify Filtering option on table view - XYN-1667");

    });
});