let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
var tableViewObject=require('../../../pageobjects/tableViewPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
let eventsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let xynergyconstants=require('../../test_xynergy.constants.js');
let genericObject=require('../../generic_objects.js');
let config=require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var conf = require('../../../config');
var expect = require('chai').expect;require('../../commons/global')();

describe('Table View',function(){
    beforeEach(function () {
        if (!isElectron()) {
            console.log("Logging into the application");
            baseMethods.loginToApplication();
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
    });

    it('XYN-1664_To check table views widget', function () {
        console.log("Test started- XYN-1664_To check table views widget");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        var sActaulText=browser.element(parameter.TableHeadText).getText();
        assert.equal(sActaulText,config.tableViewHeadData,sActaulText+" actual text is not same as expected text"+config.tableViewHeadData);
        browser.pause(300);
        var sActaulText1=browser.element(parameter.TableSmallText).getText();
        assert.equal(sActaulText1,config.tableViewData,sActaulText1+" actual text is not same as expected text"+config.tableViewData);
        
        console.log("Test completed - XYN-1664_To check table views widget");
    });

});