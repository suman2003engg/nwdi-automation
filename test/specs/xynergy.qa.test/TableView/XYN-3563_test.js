let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
var tableViewObject=require('../../../pageobjects/tableViewPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
let eventsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let xynergyconstants=require('../../test_xynergy.constants.js');
let genericObject=require('../../generic_objects.js');
let config=require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var conf = require('../../../config');
var expect = require('chai').expect;require('../../commons/global')();

describe('Table View',function(){
    beforeEach(function () {
        if (!isElectron()) {
            console.log("Logging into the application");
            baseMethods.loginToApplication();
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
    });

    it('Verify that plotted event name should be displayed under event name column - XYN-3563', function(){
        console.log("Test started - Verify that plotted event name should be displayed under event name column - XYN-3563");        
        //Table Event
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);        
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        browser.pause(300);
        //verify Event name column
        var headerRow = browser.elements(parameter.eventViewHeader);
        var TimeColumn = headerRow.elements(parameter.eventHeader).getText();         
        expect(TimeColumn).to.include(parameter.eventHeaderTime);
        var fileElement = tableViewObject.getfileElement(config.eventName);    
        assert.equal(fileElement,config.eventName);
        var totRows=browser.elements(parameter.totRows).value.length;
        console.log(totRows);
        var i=1;
        while (i<=totRows){
            var getContent = tableViewObject.getEventElement(i);
            assert.equal(getContent.toUpperCase(), config.eventName.toUpperCase())
            i++;
        }
        console.log("Test completed - Verify that plotted event name should be displayed under event name column - XYN-3563");
    });

});