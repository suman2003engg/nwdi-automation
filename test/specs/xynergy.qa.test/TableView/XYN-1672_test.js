let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
var tableViewObject=require('../../../pageobjects/tableViewPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
let eventsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let xynergyconstants=require('../../test_xynergy.constants.js');
let genericObject=require('../../generic_objects.js');
let config=require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var conf = require('../../../config');
var expect = require('chai').expect;require('../../commons/global')();

describe('Table View',function(){
    beforeEach(function () {
        if (!isElectron()) {
            console.log("Logging into the application");
            baseMethods.loginToApplication();
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
    });

    it('Verify single event tab created for multiple events on table views - XYN-1672', function(){
        console.log("Started - Verify single event tab created for multiple events on table views - XYN-1672");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        browser.pause(300)
        EventViewTabCnt =  browser.elements(parameter.eventViewTab).value.length;
        assert.equal(EventViewTabCnt, 1);
        var headerRow = browser.elements(parameter.eventViewHeader);
        var TimeColumn = headerRow.elements(parameter.eventHeader).getText();         
        expect(TimeColumn).to.include(parameter.eventHeaderTime);        
        expect(TimeColumn).to.include(parameter.eventHeaderEventName);
        expect(TimeColumn).to.include(parameter.eventHeaderTechnology);
        expect(TimeColumn).to.include(parameter.eventHeaderDeviceName);        
        baseMethods.plotEvent(config.eventName2, parameter.VoiceSetupEvent, parameter.PlotAcrossWidgets);
        browser.pause(300);
        var fileElement = tableViewObject.getfileElement(config.eventName);    
        assert.equal(fileElement,config.eventName);
        fileElement = tableViewObject.getfileElement(config.eventName2);
        assert.equal(fileElement,config.eventName2);
        console.log("Completed - Verify single event tab created for multiple events on table views - XYN-1672");
    });

});