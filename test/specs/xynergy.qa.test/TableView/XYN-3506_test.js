let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
var tableViewObject=require('../../../pageobjects/tableViewPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
let eventsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let xynergyconstants=require('../../test_xynergy.constants.js');
let genericObject=require('../../generic_objects.js');
let config=require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var conf = require('../../../config');
var expect = require('chai').expect;require('../../commons/global')();

describe('Table View',function(){
    beforeEach(function () {
        if (!isElectron()) {
            console.log("Logging into the application");
            baseMethods.loginToApplication();
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
    });

    it('Verify that multiple tabs are not getting created when events are plotted - XYN-3506', function(){
        console.log("Test started - Verify that multiple tabs are not getting created when events are plotted - XYN-3506");
        //Table-1 Metric
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        var headerRow = browser.elements(parameter.tableHeaderRow);
        var headerCol=headerRow.elements(parameter.headerColumn);
        console.log(headerCol);
        expect(headerCol.getText()).to.be.includes(config.metricName);
        //Table-2 Event
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.WidgetLeftBox);
        browser.pause(300);        
        baseMethods.plotEvent(config.eventName2, parameter.VoiceSetupEvent, parameter.TableDroppable);
        browser.pause(300);
        //click on any row in metric table and verify
        browser.element(parameter.metricRow).click();
        browser.pause(1000); //wait time must needed.
        var getCneterTexts = browser.element(parameter.unAvailableMsg).getText();
        console.log(getCneterTexts);
        expect(getCneterTexts).to.include(config.unAvailable2SecMsg);                          
        console.log("Test completed - Verify that multiple tabs are not getting created when events are plotted - XYN-3506");
    });

});