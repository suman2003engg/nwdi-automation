let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
var tableViewObject=require('../../../pageobjects/tableViewPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
let eventsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let xynergyconstants=require('../../test_xynergy.constants.js');
let genericObject=require('../../generic_objects.js');
let config=require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var conf = require('../../../config');
var expect = require('chai').expect;require('../../commons/global')();

describe('Table View',function(){
    beforeEach(function () {
        if (!isElectron()) {
            console.log("Logging into the application");
            baseMethods.loginToApplication();
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
    });

    it('TableView | Metric KPIs - Verify UXM MAC KPI plot enhancement - XYN-4140', function () {
        console.log("Test started - TableView | Metric KPIs - Verify UXM MAC KPI plot enhancement - XYN-4140");        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        //drop UXM MAC metric 
        baseMethods.plotMetric(config.metricName3, parameter.metric_UXMMACDLSCHDataRate, parameter.PlotAcrossWidgets);
        baseMethods.verifySpinnerVisibility();
        //verify column name and verify transport_channel_id column in header row
        var headerRow = browser.elements(parameter.tableViewHeadRow).getText();
        //var headerColumn=headerRow.elements(parameter.timeColumn);
        expect(headerRow).to.be.includes(config.tableViewHeadData1);
        expect(headerRow).to.be.includes(config.metricName3);
        //drop another UXM MAC metric and verify
        baseMethods.plotMetric(config.metricName4, parameter.metric_UXMMACULSCHDataRate, parameter.PlotAcrossWidgets);
        baseMethods.verifySpinnerVisibility();
        //verify column name and verify transport_channel_id column in header row
        var headerRow = browser.elements(parameter.tableViewHeadRow).getText();
        //var headerColumn=headerRow.elements(parameter.timeColumn);
        expect(headerRow).to.be.includes(config.tableViewHeadData1);
        expect(headerRow).to.be.includes(config.metricName4);
        console.log("Test Completed - TableView | Metric KPIs - Verify UXM MAC KPI plot enhancement - XYN-4140");
    });
});