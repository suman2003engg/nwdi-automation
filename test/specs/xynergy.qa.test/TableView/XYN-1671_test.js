let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
var tableViewObject=require('../../../pageobjects/tableViewPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
let eventsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let xynergyconstants=require('../../test_xynergy.constants.js');
let genericObject=require('../../generic_objects.js');
let config=require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var conf = require('../../../config');
var expect = require('chai').expect;require('../../commons/global')();

describe('Table View',function(){
    beforeEach(function () {
        if (!isElectron()) {
            console.log("Logging into the application");
            baseMethods.loginToApplication();
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
    });

  //  it('Verify export to excel feature for Table view - XYN-1671', function(){
  //  console.log("Test started - To Verify export to excel feature for Table view - XYN-1671");
  //  baseMethods.clickOnAdvancedSearchViewIcon()
  //  genericObject.clickOnDTTab(parameter.ViewsMenu);
  //  browser.pause(300);
  //  genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
  //  browser.pause(300);
  //  baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
  //  var Export = browser.element(parameter.ExportMenu);
  //  Export.click();
  //  var FileDownload = browser.element(parameter.ExportFile);
  //  FileDownload.click();
  //  console.log("Test completed - XYN-1987 - verification of plotted metric RSRP column is added on table view");
  //  });
});