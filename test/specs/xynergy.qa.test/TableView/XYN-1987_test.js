let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
var tableViewObject=require('../../../pageobjects/tableViewPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
let eventsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let xynergyconstants=require('../../test_xynergy.constants.js');
let genericObject=require('../../generic_objects.js');
let config=require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var conf = require('../../../config');
var expect = require('chai').expect;require('../../commons/global')();

describe('Table View',function(){
    beforeEach(function () {
        if (!isElectron()) {
            console.log("Logging into the application");
            baseMethods.loginToApplication();
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
    });

    it('Verify Plotted Metric column on table view - XYN-1987', function(){
        console.log("Test started - XYN-1987 - verification of plotted metric RSRP column is added on table view");        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        browser.pause(1000);        
        var av=browser.elements(parameter.tableAppData);
        var headerRow = av.elements(parameter.tableHeaderRow);
        console.log(headerRow.value.length);     
        var headerColumn=browser.elements(parameter.headerColumn).value.length;
        assert.equal(headerColumn,1);
        //plot any event and verify got plotted properly or not
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        browser.pause(300); 
        EventViewTabCnt =  browser.elements(parameter.eventTabCount).value.length;
        console.log(EventViewTabCnt);
        assert.equal(EventViewTabCnt, 1);
        var fileElement = browser.element(parameter.fileElement).getText();       
        assert.equal(fileElement,config.eventName);
        genericObject.clickOnDTTab(parameter.EventsMenu);
        eventsDataObject.clearEventName();
        console.log("Test completed - XYN-1987 - verification of plotted metric RSRP column is added on table view");
    });

});