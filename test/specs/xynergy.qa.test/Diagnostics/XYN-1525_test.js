let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var diagnosticsObject=require('../../../pageobjects/diagnosticsPage.js');
var searchdataObject=require('../../../pageobjects/searchDataPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();
//commons/global.js')();

describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();           
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        
    });
    
    it('Verify message diagnostics view for rename and extended - XYN-1525', function() {        
    console.log("Test Started - Verify message diagnostics view for rename and extended XYN-1525");
    // baseMethods.SelectTabs("SEARCHDATA");
    // baseMethods.SelectSearchDataTabs("NEWSEARCH");
    // baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.SearchVoiceTextForDataSet);
    // searchdataObject.selectdatasetresult();
    // var selectdataset = browser.element("[data-test-id='search-child-item-filtered']");
    // var visibility = selectdataset.isVisible();
    // assert(visibility,true);
    // baseMethods.verifySpinnerVisibility();        
    // browser.pause(500);
    // searchdataObject.clickOpeninCanvasbutton();
    // browser.pause(1000);
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    baseMethods.dragAndDrop(parameter.DiagnostaticsDraggable, parameter.FreshViewsDroppable);
    var rowClass = browser.elements("//*[@name='diagnosis-grid']//datatable-row-wrapper//datatable-body-row[contains(@class,'datatable-body-row active datatable-row-even')]");
    console.log(rowClass.getText());
    var DiagnosticsDetails = browser.element("[data-test-id='details']");
    var visibility = DiagnosticsDetails.isVisible();
    assert(visibility,true);        
    console.log("Test Completed - Verify message diagnostics view for rename and extended XYN-1525");

});


});