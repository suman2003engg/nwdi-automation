let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var diagnosticsObject=require('../../../pageobjects/diagnosticsPage.js');
var searchdataObject=require('../../../pageobjects/searchDataPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();
//commons/global.js')();

describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();           
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        
    });
    it('Diagnostics - To verify Filtering text with double quotes  XYN-2769', function() {
        console.log("Test Started - Diagnostics - To verify Filtering text with double quotes XYN-2769");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.DiagnostaticsDraggable, parameter.FreshViewsDroppable)
        browser.pause(300);
        var diagnosticsGrid=browser.elements("//app-data-grid[@name='diagnosis-grid']");
        diagnosticsObject.enterFilterTextInDiagnostics('"'+'PHY'+'"');
        browser.pause(400);
        var j = 1;
        var rowCount = diagnosticsGrid.elements("//datatable-row-wrapper").value.length;
        console.log(rowCount);
        while (rowCount > 0) {
            var rowContent = browser.elements("//datatable-row-wrapper[" + j + "]");
            console.log(rowContent.element("//span[@title='PHY']"));
            expect(rowContent.element("//span[@title='PHY']").getText()).to.equals('PHY');
            j++;
            rowCount--;
        }
        console.log("Test Completed - Diagnostics - To verify Filtering text with double quotes - 2769");
    });
});