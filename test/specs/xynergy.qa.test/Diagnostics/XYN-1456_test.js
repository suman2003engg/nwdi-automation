let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var diagnosticsObject=require('../../../pageobjects/diagnosticsPage.js');
var searchdataObject=require('../../../pageobjects/searchDataPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Diagnostics', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();            
        }
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
    });

    it('Verify whether Diagnostics view is getting refreshed when other dataset is loaded - XYN-1456', function() {        
        console.log("Test Started - Verify whether Diagnostics view is getting refreshed when other dataset is loaded - XYN-1456");
        // baseMethods.SelectTabs("SEARCHDATA");
        // baseMethods.SelectSearchDataTabs("NEWSEARCH");
        // baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.SearchVoiceTextForDataSet);
        // searchdataObject.selectdatasetresult();
        // var selectdataset = browser.element("[data-test-id='search-child-item-filtered']");
        // var visibility = selectdataset.isVisible();
        // assert(visibility,true);
        // baseMethods.verifySpinnerVisibility();        
        // browser.pause(500);
        // searchdataObject.clearsearchkeyword();
        // searchdataObject.clickOpeninCanvasbutton();
        // browser.pause(1000);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.DiagnostaticsDraggable, parameter.FreshViewsDroppable);
        var rowClass = browser.elements("//*[@name='diagnosis-grid']//datatable-scroller[@class='datatable-scroll']//datatable-row-wrapper//datatable-body-row[contains(@class,'datatable-body-row active')]");
        console.log(rowClass.getText());      
        searchdataObject.clickDataTabButton();
        browser.pause(1000);
        genericObject.clearsearchkeyword();
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.SelectSearchDataTabs("NEWSEARCH");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,"Run38");
        searchdataObject.selectdatasetresult();
        var selectdataset = browser.element("[data-test-id='search-child-item-filtered']");
        var visibility = selectdataset.isVisible();
        assert(visibility,true);
        baseMethods.verifySpinnerVisibility();        
        browser.pause(500);
        searchdataObject.clickOpeninCanvasbutton();
        browser.pause(1000);
        browser.element("[data-test-id='Datascope-Replace-button']").click();
        browser.element("[data-test-id='Datascope-ReplaceOK-button']").click();   
        browser.pause(1000);
        baseMethods.verifySpinnerVisibility(); 
        var rowClass2 = browser.elements("//*[@name='diagnosis-grid']//datatable-scroller[@class='datatable-scroll']//datatable-row-wrapper//datatable-body-row[contains(@class,'datatable-body-row active')]");
        console.log(rowClass2.getText());   
        assert.notEqual(rowClass.getText(),rowClass2.getText());          
        console.log("Test Completed - Verify whether Diagnostics view is getting refreshed when other dataset is loaded - XYN-1456");

    });

});