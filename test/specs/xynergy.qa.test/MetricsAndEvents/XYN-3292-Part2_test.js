let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let metricsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let timeseries=require("../../../pageobjects/timeseries.js");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Metrics test', function () {
    var init=0;
    var type="metric";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
               
        }
        
    });
    afterEach(function () {
        if(isElectron()){
            metricsDataObject.closeMetricWidget();
        }       
    });

it('Verify whether selection based metrics pop up selections when dropped on Timeseries/Map - XYN-3292 (Part-2)', function(){
    console.log("Test started - XYN-3292 - Verify whether selection based metrics pop up selections when dropped on Timeseries/Map");
    try{genericObject.clickOnDTTab(parameter.ViewsMenu);
    genericObject.dragAndDropViews(parameter.TimeseriesDraggable, parameter.FreshViewsDroppable);
    browser.pause(300);
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    genericObject.dragAndDropViews(parameter.MapDraggable, parameter.WidgetRightBox);
    browser.pause(300);
    baseMethods.plotMetric(config.selectionBaseMetricName, parameter.lteRankByCellType, parameter.PlotAcrossWidgets);
    browser.pause(300);
    var mbox = browser.element(parameter.mBoxTitleId).isVisible();
    assert.equal(mbox, true);
    var mboxTitle = browser.element(parameter.lteRankByCellTypeMBoxTitle).getText();
    assert.equal(mboxTitle, config.selectionBaseMetricName);
    var selectcellType = browser.element(parameter.selectCell).getText();
    assert.equal(selectcellType, config.psCellSpaces);
    metricsDataObject.clickSelectionMetricsOKBtn();
    //verify from ts tab
    timeseries.clickOnTimeSeriesLayerBtn();
    var tslayerinfo = browser.elements(parameter.TimeseriesLayerData).getText();
    browser.pause(300);
    console.log(tslayerinfo);
    expect(tslayerinfo[1]).to.include(config.selectionBaseMetricName);
    expect(tslayerinfo[1]).to.include(config.metricsSelection);
    //verify from map tab
    metricsDataObject.legendTab();
    var maplayerinfo = browser.elements(parameter.dataLayers).getText();
    console.log(maplayerinfo);
    expect(maplayerinfo[2]).to.include(config.selectionBaseMetricName);
    expect(maplayerinfo[2]).to.include(config.metricsSelection);
    }catch(e){
        console.log(e.Message());
        assert.fail();
    }finally{
    //close all views
    baseMethods.closeAllViews();
    }
    console.log("Test completed - XYN-3292 - Verify whether selection based metrics pop up selections when dropped on Timeseries/Map");
});

});