let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let metricsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let timeseries=require("../../../pageobjects/timeseries.js");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Metrics test', function () {
    var init=0;
    var type="metric";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
               
        }
        
    });
    afterEach(function () {
        if(isElectron()){
            metricsDataObject.closeMetricWidget();
        }       
    });


it('Verify whether Unavailable metrics[grayed out] should not plot graph on applicable views and should display error message - XYN-2113', function(){
    console.log("Verify whether Unavailable metrics[grayed out] should not plot graph on applicable views and should display error message - XYN-2113");
    genericObject.clickOnDTTab(parameter.MetricsMenu);
    baseMethods.verifySpinnerVisibility();
    baseMethods.clearAllFavElements(type);
    browser.pause(200);
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    genericObject.dragAndDropViews(parameter.TimeseriesDraggable, parameter.FreshViewsDroppable);
    baseMethods.plotMetric(config.unavailableMetric, parameter.unavailableMetric, parameter.TimeseriesDroppable);
    metricsDataObject.clickOkOnErrorMsg();
    metricsDataObject.clickOnTimeseriesCloseBtn();

    // genericObject.clickOnDTTab(parameter.ViewsMenu);
    // genericObject.dragAndDropViews(parameter.MapDraggable, parameter.FreshViewsDroppable);
    // browser.pause(300);
    // baseMethods.plotMetric(config.unavailableMetric, parameter.unavailableMetric, parameter.MapDroppable);
    // browser.pause(300);
    // metricsDataObject.clickOnMapErrorMsg();

    console.log("Verify whether Unavailable metrics[grayed out] should not plot graph on applicable views and should display error message - XYN-2113");
});

});