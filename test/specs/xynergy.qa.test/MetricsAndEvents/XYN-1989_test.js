let baseMethods = require('../../testbase');
let genericObject = require("../../generic_objects.js");
let parameter = require("../../parameters.js");
let eventsDataObject = require("../../../pageobjects/metricsAndEventsDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Events test', function () {
    var init = 0;
    var type="event";
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }
        baseMethods.clearAllFavElements(type);

    });

    it('Verify whether unavailable events are greyed out - XYN-1989', function () {
        console.log("Verify whether unavailable events are greyed out - XYN-1989");
        genericObject.clickOnDTTab(parameter.EventsMenu);
        eventsDataObject.clickOnExpander();
        browser.pause(300);
        var eventRow = browser.elements(parameter.disabledEventOrMetric);
        //console.log(eventRow.value.length);
        eventsDataObject.clickOnToggle();
        var eventRow1 = browser.elements(parameter.disabledEventOrMetric).value.length;
        assert.equal(eventRow1, config.compareValueWithZero);
        console.log("Test completed for the verification of greyed out events when toggle button is off - XYN-1989");
    });

});