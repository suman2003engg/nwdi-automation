let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let metricsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let timeseries=require("../../../pageobjects/timeseries.js");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Metrics test', function () {
    var init=0;
    var type="metric";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
               
        }
        
    });
    afterEach(function () {
        if(isElectron()){
            metricsDataObject.closeMetricWidget();
        }       
    });

it('To Verify Per Cell KPIs plotting selection option  - XYN-2757', function () {
    console.log("Test started To Verify Per Cell KPIs plotting selection option   - XYN-2757'");
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
    baseMethods.plotMetric(config.ltePciMetric, parameter.ltePciMetric, parameter.TableDroppable);
    browser.pause(500);
    baseMethods.verifySpinnerVisibility();
    var DialogBoxMsg = browser.elements(parameter.dialogBoxTitle).getText();
    assert.equal(DialogBoxMsg,config.dialogBoxTitle);
    var DialogBoxHeader = browser.elements(parameter.dialogBoxHead).getText();
    assert.equal(DialogBoxHeader,config.dialogBoxHeader);
    metricsDataObject.clickSelectionMetricsOKBtn();
    baseMethods.verifySpinnerVisibility();
    browser.pause(500);
    var av=browser.elements(parameter.tableAppData);
    var headerRow = av.elements(parameter.tableHeaderRow);
    console.log(headerRow.value.length);     
    var headerColumn=browser.elements("//span[contains(text(),'"+config.ltePciMetric+"')]").value.length;
    assert.equal(headerColumn,1);
    baseMethods.closeAllViews();
    console.log("Test completed To Verify Per Cell KPIs plotting selection option   - XYN-2757'");

});

});