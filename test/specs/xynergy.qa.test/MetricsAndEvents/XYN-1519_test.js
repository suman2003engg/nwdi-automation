let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let metricsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let timeseries=require("../../../pageobjects/timeseries.js");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Metrics test', function () {
    var init=0;
    var type="metric";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
               
        }
        
    });
    afterEach(function () {
        if(isElectron()){
            metricsDataObject.closeMetricWidget();
        }       
    });

it.skip('Verify whether selected metric have options and those options are functioning - XYN-1519', function () {
    console.log("Test started for'Verify whether selected metric have options and those options are functioning - XYN-1519'");
    var metricValue=config.metricName;
    try{genericObject.clickOnDTTab(parameter.MetricsMenu);
    baseMethods.verifySpinnerVisibility();
    baseMethods.clearAllFavElements(type);
    genericObject.setMetricName(metricValue);
    browser.pause(500);
    metricsDataObject.clickFilteredMetric();
    browser.pause(400);
    var addFavorites = browser.elements(parameter.addToFavorites);
    var addToFavName=addFavorites.element(parameter.addToFavButton).getText();
    assert.equal(config.addToFavText , addToFavName);
    metricsDataObject.clickOnAddToFavElem(type);
    browser.pause(200);
    metricsDataObject.clearMetricName();
    metricsDataObject.closeMetricWidget();
    genericObject.clickOnDTTab(parameter.MetricsMenu);
    metricsDataObject.clickFirstMetric();
    var Removefavorites = browser.elements(parameter.addToFavorites);
    var RemoveFavouriteBtn = Removefavorites.element(parameter.remFavButton).getText();
    assert.equal(config.removeFavText, RemoveFavouriteBtn);
    metricsDataObject.clickOnRemoveFavElem();
    browser.pause(200);
    metricsDataObject.closeMetricWidget();
    genericObject.clickOnDTTab(parameter.MetricsMenu);
    baseMethods.verifySpinnerVisibility();
    genericObject.setMetricName(metricValue);
    browser.pause(200);
    metricsDataObject.clickFirstMetric();
    var addToFavName2=addFavorites.element(parameter.addToFavButton).getText();
    assert.equal(config.addToFavText, addToFavName2);
    }catch(err){
        console.log("test failed");
        console.log(err.message);
    }finally{
    metricsDataObject.clickOnAddToFavElem(type);
    browser.pause(200);
    metricsDataObject.clearMetricName();
    metricsDataObject.clickOnRemoveFavElem();        
    }    
    console.log("Test completed for 'Verify whether selected metric have options and those options are functioning - XYN-1519'");

});

});