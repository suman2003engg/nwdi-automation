let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let metricsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let timeseries=require("../../../pageobjects/timeseries.js");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Metrics test', function () {
    var init=0;
    var type="metric";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
               
        }
        
    });
    afterEach(function () {
        if(isElectron()){
            metricsDataObject.closeMetricWidget();
        }       
    });

it('Verify that when selection based metric is plotted, if data is not avaliable for selected metric it should not clear data of previously plotted metric - XYN-3328', function(){
    console.log("Verify that when selection based metric is plotted, if data is not avaliable for selected metric it should not clear data of previously plotted metric - XYN-3328");
    genericObject.clickOnDTTab(parameter.MetricsMenu);
    baseMethods.verifySpinnerVisibility();
    baseMethods.clearAllFavElements(type);
    browser.pause(200);
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
    baseMethods.plotMetric(config.ltePciMetric, parameter.ltePciMetric, parameter.TableDroppable);
    var DialogBoxMsg = browser.elements(parameter.dialogBoxTitle).getText();
    assert.equal(DialogBoxMsg,config.dialogBoxTitle);
    var DialogBoxHeader = browser.elements(parameter.dialogBoxHead).getText();
    assert.equal(DialogBoxHeader,config.dialogBoxHeader);
    var forMetricValue = browser.element(parameter.selectMetricValue);
    forMetricValue.click();
    metricsDataObject.selectMetricValue(config.firstSelectionBasedMetricValue);
    browser.pause(100);
    metricsDataObject.clickSelectionMetricsOKBtn();
    browser.pause(100);
    genericObject.clickOnDTTab(parameter.MetricsMenu);
    genericObject.dragAndDropViews(parameter.ltePciMetric, parameter.TableDroppable);
    var DialogBoxMsg = browser.elements(parameter.dialogBoxTitle).getText();
    assert.equal(DialogBoxMsg,config.dialogBoxTitle);
    var DialogBoxHeader = browser.elements(parameter.dialogBoxHead).getText();
    assert.equal(DialogBoxHeader,config.dialogBoxHeader);
    var forMetricValue = browser.element(parameter.selectMetricValue);
    forMetricValue.click();
    metricsDataObject.selectMetricValue(config.selectionBasedMetricForTableView);
    browser.pause(300);
    metricsDataObject.clickSelectionMetricsOKBtn();
    browser.pause(800);
    //browser.waitForExist(parameter.noDataAvailableOkBtn);
    metricsDataObject.clickOkBtn();
    browser.pause(300);    
    var headerColumn=browser.elements("//span[contains(text(),'"+config.ltePciMetric+"')]").value.length;
    assert.equal(headerColumn,1);
    console.log("Test completed to verify that when selection based metric is plotted, if data is not avaliable for selected metric it should not clear data of previously plotted metric - XYN-3328");
});    

});