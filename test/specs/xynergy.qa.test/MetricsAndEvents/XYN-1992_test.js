let baseMethods = require('../../testbase');
let genericObject = require("../../generic_objects.js");
let parameter = require("../../parameters.js");
let eventsDataObject = require("../../../pageobjects/metricsAndEventsDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Events test', function () {
    var init = 0;
    var type="event";
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }
        baseMethods.clearAllFavElements(type);

    });

    it('Events | Add to Favorites to be remembered - XYN-1992', function () {
        console.log("Events | Add to Favorites to be remembered - XYN-1992");
        genericObject.clickOnDTTab(parameter.EventsMenu);
        genericObject.setEventName(config.eventName);
        eventsDataObject.clickOnFilteredEvent();
        //console.log(addFavorites.value.length);
        eventsDataObject.clickOnAddToFavElem(type);
        eventsDataObject.closeEventWidget();
        browser.pause(300);
        genericObject.clickOnDTTab(parameter.EventsMenu);
        genericObject.setEventName(config.eventName);
        eventsDataObject.clickOnFilteredEvent();
        assert.equal(true, eventsDataObject.isVisibleRemoveFromFavorites());
        browser.pause(200);
        eventsDataObject.clickOnRemoveFavElem();
        console.log("Test completed: Events | Add to Favorites to be remembered - XYN-1992");
    });

});