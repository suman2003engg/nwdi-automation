let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let metricsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let timeseries=require("../../../pageobjects/timeseries.js");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Metrics test', function () {
    var init=0;
    var type="metric";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
               
        }
        
    });
    afterEach(function () {
        if(isElectron()){
            metricsDataObject.closeMetricWidget();
        }       
    });

it('To verify whether modal pop-up shows up when event Is plotted - XYN-1991', function(){
    console.log("Test started - To verify whether modal pop-up shows up when event Is plotted - XYN-1991 ");
    try{genericObject.clickOnDTTab(parameter.ViewsMenu);
    genericObject.dragAndDropViews(parameter.MapDraggable, parameter.FreshViewsDroppable);
    browser.pause(300);
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    genericObject.dragAndDropViews(parameter.TableDraggable, parameter.WidgetRightBox);
    browser.pause(300);
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    genericObject.dragAndDropViews(parameter.TimeseriesDraggable, parameter.WidgetRightBox);
    browser.pause(300);
    baseMethods.plotMetric(config.ltePciMetric, parameter.ltePciMetric, parameter.PlotAcrossWidgets);
    browser.pause(300);
    var DialogBoxMsg = browser.elements(parameter.dialogBoxTitle).getText();
    assert.equal(DialogBoxMsg,config.dialogBoxTitle);
    var DialogBoxHeader = browser.elements(parameter.dialogBoxHead).getText();
    assert.equal(DialogBoxHeader,config.dialogBoxHeader);
    metricsDataObject.clickSelectionMetricsOKBtn();
    browser.pause(300);
    baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
    var mbox = browser.element(parameter.dialogBoxHead).isVisible();
    assert.equal(mbox, false);}
    catch(err){
        console.log("test failed");
        console.log(err.message);
        assert.fail(err.message);
    }
    finally{
    baseMethods.closeAllViews();
    }
    console.log("Test completed - To verify whether modal pop-up shows up when event Is plotted - XYN-1991 ");
});

});