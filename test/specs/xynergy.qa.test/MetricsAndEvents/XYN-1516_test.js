let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let metricsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let timeseries=require("../../../pageobjects/timeseries.js");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Metrics test', function () {
    var init=0;
    var type="metric";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
               
        }
        
    });
    afterEach(function () {
        if(isElectron()){
            metricsDataObject.closeMetricWidget();
        }       
    });

it('Verify whether Metric widget options working accordingly - XYN-1516', function(){
    console.log("Test started - XYN-1516 - Verify Metric widget options");
    genericObject.clickOnDTTab(parameter.MetricsMenu);
    browser.pause(300);       
    var dockingpinUnpinned = browser.element(parameter.dockpinUnPin);        
    if (dockingpinUnpinned.isVisible()){             
        metricsDataObject.eventWidgetDock();
    }
    browser.pause(300);
    var dockingpinPinned = browser.element(parameter.dockpinStatus);
    assert.equal(dockingpinPinned.isVisible(), true);
    metricsDataObject.closeMetricWidget();
    //Search for metric
    genericObject.clickOnDTTab(parameter.MetricsMenu);
    var dockingpinPinned = browser.element(parameter.dockpinStatus);
    metricsDataObject.enterMetricFilterValue(config.metricName);
    //verify pinned window not closed
    assert.equal(dockingpinPinned.isVisible(), true);
    //close metric widget and verify
    metricsDataObject.closeMetricWidget();
    browser.pause(300);
    var MetricsTabClosed = browser.element(parameter.metricTabStatus);
    assert.equal(MetricsTabClosed.isVisible(), false);
    //clear metric filter and undock and close metric view
    genericObject.clickOnDTTab(parameter.MetricsMenu);
    metricsDataObject.clearMetricName();
    browser.pause(300);
    metricsDataObject.eventWidgetUnDock();
    browser.pause(300);
    metricsDataObject.closeMetricWidget();       
    console.log("Test completed - XYN-1516 - Verify Metric widget options");
});

});