let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let metricsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let timeseries=require("../../../pageobjects/timeseries.js");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Metrics test', function () {
    var init=0;
    var type="metric";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
               
        }
        
    });
    afterEach(function () {
        if(isElectron()){
            metricsDataObject.closeMetricWidget();
        }       
    });

it('To verify whether toggle button under metrics remembers the selection - XYN-2364', function(){
    console.log("Test started - To verify whether toggle button under metrics remembers the selection - XYN-2364");
    genericObject.clickOnDTTab(parameter.MetricsMenu);
    browser.pause(300);                     
    //When 'Show only available metrics' button is on
    metricsDataObject.clickOnToggle();
    var grayMetrics = browser.elements(parameter.greyMetricsList).isVisible();
    assert.equal(grayMetrics, false);
    //close and re-open metrics tab
    metricsDataObject.closeMetricWidget();
    browser.pause(300);
    genericObject.clickOnDTTab(parameter.MetricsMenu);
    browser.pause(300);
    var grayMetrics = browser.elements(parameter.greyMetricsList).isVisible();
    assert.equal(grayMetrics, false);
    //Untoggle at end of script
    //metricsDataObject.clickOnToggle();
    metricsDataObject.closeMetricWidget();
    console.log("Test completed - To verify whether toggle button under metrics remembers the selection - XYN-2364");
});

});