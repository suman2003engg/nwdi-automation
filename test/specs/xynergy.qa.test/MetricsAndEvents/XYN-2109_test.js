let baseMethods = require('../../testbase');
let genericObject = require("../../generic_objects.js");
let parameter = require("../../parameters.js");
let eventsDataObject = require("../../../pageobjects/metricsAndEventsDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Events test', function () {
    var init = 0;
    var type="event";
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }
        baseMethods.clearAllFavElements(type);

    });

    it('Verify whether selected Event have options and those options are functioning - XYN-2109', function () {
        console.log("Test started for'Verify whether selected Event have options and those options are functioning - XYN-2109'");
        genericObject.clickOnDTTab(parameter.EventsMenu);
        baseMethods.verifySpinnerVisibility();
        genericObject.setEventName(config.eventName);
        browser.pause(200);
        eventsDataObject.clickOnFilteredEvent();
        var addFavorites = browser.elements(parameter.addToFavorites);
        var addToFavName = addFavorites.element(parameter.addToFavButton).getText();
        assert.equal(config.addToFavText, addToFavName);
        eventsDataObject.clickOnAddToFavElem(type);
        eventsDataObject.clearEventName();
        eventsDataObject.closeEventWidget();
        genericObject.clickOnDTTab(parameter.EventsMenu);
        eventsDataObject.clickOnFilteredEvent();
        var Removefavorites = browser.elements(parameter.addToFavorites);
        var RemoveFavouriteBtn = Removefavorites.element(parameter.remFavButton).getText();
        assert.equal(config.removeFavText, RemoveFavouriteBtn);
        eventsDataObject.clickOnRemoveFavElem();
        eventsDataObject.closeEventWidget();
        genericObject.clickOnDTTab(parameter.EventsMenu);
        browser.pause(200);
        baseMethods.verifySpinnerVisibility();
        genericObject.setEventName(config.eventName2);
        browser.pause(200);
        eventsDataObject.clickOnFilteredEvent();
        browser.pause(200);
        var addToFavName2 = addFavorites.element(parameter.addToFavButton).getText();
        assert.equal(config.addToFavText, addToFavName2);  
        eventsDataObject.clickOnAddToFavElem(type);
        browser.pause(200);
        eventsDataObject.clickOnFilteredEvent();
        browser.pause(200);
        eventsDataObject.clickOnRemoveFavElem();        
        console.log("Test completed for 'Verify whether selected event have options and those options are functioning - XYN-2109'");
    });

});