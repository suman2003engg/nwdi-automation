let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let metricsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let timeseries=require("../../../pageobjects/timeseries.js");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Metrics test', function () {
    var init=0;
    var type="metric";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
               
        }
        
    });
    afterEach(function () {
        if(isElectron()){
            metricsDataObject.closeMetricWidget();
        }       
    });

it('Verify whether toggle button is working accordingly - XYN-1517', function(){
    console.log("Test started - XYN-1517 - Verify whether toggle button is working accordingly");
    genericObject.clickOnDTTab(parameter.MetricsMenu);
    browser.pause(300);       
    metricsDataObject.clickOnExpander();
    browser.pause(300);  
    //When 'Show only available metrics' button is off (default)
    var totGreyMetrics = browser.elements(parameter.greyMetricsList).value.length;
    if ( totGreyMetrics >=1 ) {
        var grayMetrics = browser.elements(parameter.greyMetricsList).isVisible();
        assert.equal(grayMetrics[1], true);
    }              
    //When 'Show only available metrics' button is on
    metricsDataObject.clickOnToggle();
    browser.pause(300);  
    var grayMetrics_1 = browser.elements(parameter.greyMetricsList).isVisible();
    assert.equal(grayMetrics_1, false); 
    //untoggle and close metric widget
    metricsDataObject.clickOnToggle();        
    metricsDataObject.closeMetricWidget();   
    console.log("Test completed - XYN-1517 - Verify whether toggle button is working accordingly");
});

});