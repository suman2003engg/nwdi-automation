let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let metricsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let timeseries=require("../../../pageobjects/timeseries.js");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Metrics test', function () {
    var init=0;
    var type="metric";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
               
        }
        
    });
    afterEach(function () {
        if(isElectron()){
            metricsDataObject.closeMetricWidget();
        }       
    });

it('Verify that when selection based metric is plotted, each selected one should form different graph layers on timeseries - XYN-3327', function(){
    console.log("Verify that when selection based metric is plotted, each selected one should form different graph layers on timeseries - XYN-3327");
    genericObject.clickOnDTTab(parameter.MetricsMenu);
    baseMethods.verifySpinnerVisibility();
    baseMethods.clearAllFavElements(type);
    browser.pause(200);
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    genericObject.dragAndDropViews(parameter.TimeseriesDraggable, parameter.FreshViewsDroppable);
    baseMethods.plotMetric(config.ltePciMetric, parameter.ltePciMetric, parameter.TimeseriesDroppable);
    var DialogBoxMsg = browser.elements(parameter.dialogBoxTitle).getText();
    assert.equal(DialogBoxMsg,config.dialogBoxTitle);
    var DialogBoxHeader = browser.elements(parameter.dialogBoxHead).getText();
    assert.equal(DialogBoxHeader,config.dialogBoxHeader);
    var forMetricValue = browser.element(parameter.selectMetricValue);
    forMetricValue.click();
    metricsDataObject.selectMetricValue(config.firstSelectionBasedMetricValue);
    browser.pause(100);
    metricsDataObject.clickSelectionMetricsOKBtn();
    browser.pause(100);
    genericObject.clickOnDTTab(parameter.MetricsMenu);
    genericObject.dragAndDropViews(parameter.ltePciMetric, parameter.TimeseriesDroppable);
    var DialogBoxMsg = browser.elements(parameter.dialogBoxTitle).getText();
    assert.equal(DialogBoxMsg,config.dialogBoxTitle);
    var DialogBoxHeader = browser.elements(parameter.dialogBoxHead).getText();
    assert.equal(DialogBoxHeader,config.dialogBoxHeader);
    var forMetricValue = browser.element(parameter.selectMetricValue);
    forMetricValue.click();
    metricsDataObject.selectMetricValue(config.secondSelectionBasedMetricValue);
    browser.pause(300);
    metricsDataObject.clickSelectionMetricsOKBtn();
    browser.pause(500);
    var graphCount=browser.elements("//time-series-chart");
    console.log(graphCount.value.length);
    assert.equal(graphCount.value.length,config.compareValueWithOne);
    var firstMetricNameOnGraph=config.ltePciMetric+" ("+config.firstSelectionBasedMetricValue+")";
    console.log(firstMetricNameOnGraph);
    var secondMetricNameOnGraph=config.ltePciMetric+" ("+config.secondSelectionBasedMetricValue+")";
    console.log(secondMetricNameOnGraph);
    expect(graphCount.getText()).to.be.includes(firstMetricNameOnGraph);
    expect(graphCount.getText()).to.be.includes(secondMetricNameOnGraph);
    console.log("Test completed to verify that when selection based metric is plotted, each selected one should form different graph layers on timeseries - XYN-3327");
});

});