let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let config = require("../../../config.js");
var templatesObject = require('../../../pageobjects/templatesPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
var messageObject = require('../../../pageobjects/messagePage');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Templates test', function () {
    var init=0;
    var type="templates";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();            
        }              
    });   
    it('To verify Templates | 5GNR DL IP Data Stall Template | Filter Issue on changing Scope - XYN-2980', function(){
        console.log("Test started - To verify Templates | 5GNR DL IP Data Stall Template | Filter Issue on changing Scope - XYN-2980");
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        baseMethods.selectTemplate(config.templateName1);
        //if modelbox opened
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            browser.element(parameter.openSavedSearchBtn).click();                        
        }
        browser.pause(2000);
        //Fileter Messages view for not available data
        messageObject.enterMessageFilterValue(config.findMessageFilterValueInvalid);
        browser.pause(2000);
        var noDatamsg = browser.element(parameter.noData).isVisible();
        assert.equal(noDatamsg, true);
        //Add one more dataset to the existing view        
        templatesObject.repeatforScopeTest(config.searchDatasetForRegularTestMO, parameter.dataScopeAdd);
        //replace the existing view with other dataset                 
        templatesObject.repeatforScopeTest(config.searchDatasetForPComparison5GTest, parameter.dataScopeReplaceBtn);
        console.log("Test completed - To verify Templates | 5GNR DL IP Data Stall Template | Filter Issue on changing Scope - XYN-2980");
    });
});