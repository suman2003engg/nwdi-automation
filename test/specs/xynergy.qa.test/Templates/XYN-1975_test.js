let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let config = require("../../../config.js");
var templatesObject = require('../../../pageobjects/templatesPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
var messageObject = require('../../../pageobjects/messagePage');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Templates test', function () {
    var init=0;
    var type="templates";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();            
        }              
    });
    it('Verify templates shouldnot open when no data scope is defined - XYN-1975', function(){
        console.log("Test started - Verify templates shouldn't open when no data scope is defined - XYN-1975");
        //After Login, click on AV and then DT for Views
        browser.element(parameter.AVMenu).click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        //select any template
        baseMethods.selectTemplate(config.templateName1);
        //catch the error dialog
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            assert.equal(browser.element(parameter.wrngContent).getText(), config.noScopeisSet); 
            //click ok
            browser.element(parameter.sameDatasetDialogWindowClose).click();                       
        }
        browser.pause(300);
        console.log("Test completed - Verify templates shouldn't open when no data scope is defined - XYN-1975");
    });    
});