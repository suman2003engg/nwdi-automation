let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let config = require("../../../config.js");
var templatesObject = require('../../../pageobjects/templatesPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
var messageObject = require('../../../pageobjects/messagePage');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Templates test', function () {
    var init=0;
    var type="templates";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();            
        }              
    });        
    it('To verify Templates implementation - XYN-2888', function(){
        console.log("Test started - To verify Templates implementation - XYN-2888");
        //This test step comes after XYN-3025
        //1. template 1 - 5GNR DL IP Data Stall - Re-align after closing messages view
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchALFDataset);
        baseMethods.openInCanvasValidation() ;        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        baseMethods.selectTemplate(config.templateName1);
        //if modelbox opened
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        if (mboxOpened){
            browser.element(parameter.openSavedSearchBtn).click();                        
        }
        browser.pause(2000);
        //verify Diagnostics, Messages and Time Series views are opened
        assert.equal(browser.element(parameter.viewDiagnostics).isVisible(), true);
        assert.equal(browser.element(parameter.viewMessages).isVisible(), true);
        assert.equal(browser.element(parameter.viewTimeSeries).isVisible(), true);
        //view confirmation
        assert.equal(browser.element(config.mvviewStyle1).isVisible(), true);
        //cloes messages view
        browser.element(parameter.msgViewCloseBtn).click();
        //after close
        assert.equal(browser.element(parameter.viewDiagnostics).isVisible(), true);
        assert.equal(browser.element(parameter.viewMessages).isVisible(), false);
        assert.equal(browser.element(parameter.viewTimeSeries).isVisible(), true);
        //style
        assert.equal(browser.element(config.mvviewStyle1).isVisible(), false);
        console.log("Test completed - To verify Templates implementation - XYN-2888");
    });    
});