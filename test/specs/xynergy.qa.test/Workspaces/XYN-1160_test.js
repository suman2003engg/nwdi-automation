let baseMethods = require('../../testbase.js');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var browseDataObject = require('../../../pageobjects/browseDataPage.js');
var workspacesObject = require('../../../pageobjects/workspacesPage.js');
var scopeDataObject = require('../../../pageobjects/scopeWidgetPage.js');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;require('../../commons/global.js')();

describe('Workspace page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });

    it('Workspace - To verify if restore workspace remembers the view state saved with multiple views: XYN-1160' ,function(){
    console.log("Test Started - Workspace - To verify if restore workspace remembers the view state saved with multiple views: XYN-1160");              
    baseMethods.SelectTabs("SEARCHDATA");
        /* Import SprintMO Dataset to AV */
    baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchDatasetForRegularTest);
    searchdataObject.selectdatasetresult();
    browser.element(parameter.searchDataOpenBtn).click();
    baseMethods.openInCanvasValidation();
    baseMethods.verifySpinnerVisibility();

        /* Drop Map view and plot KPI and verify the same */
    genericObject.clickOnDTTab(parameter.ViewsMenu);        
    baseMethods.dragAndDrop(parameter.MapDraggable, parameter.FreshViewsDroppable);
    baseMethods.verifySpinnerVisibility();
    baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
    baseMethods.verifySpinnerVisibility();
    browser.element(parameter.mapLayersTab).click();
    var layerValue = baseMethods.verifyLayerData(config.metricName);
    assert(layerValue,true);
    browser.element(parameter.incorrectQueryClose).click();
        /* Drop Message view with right split */
    genericObject.clickOnDTTab(parameter.ViewsMenu);        
    baseMethods.dragAndDrop(parameter.MessagesDraggable, parameter.WidgetRightBox);
    baseMethods.verifySpinnerVisibility();
        /* Drop Time series view bottom of Map view and plot KPI2 and verify same */
    genericObject.clickOnDTTab(parameter.ViewsMenu);        
    baseMethods.dragAndDrop(parameter.TimeseriesDraggable, parameter.WidgetBottomBox);
    baseMethods.plotMetric(config.metricNameRSRQ, parameter.RSRQMetric, parameter.TimeseriesDroppable);
    baseMethods.verifySpinnerVisibility();
    browser.element(parameter.TimeseriesLayerButton).click();
    var metricPlot2 = baseMethods.verifyTimeSeriesMetricLayerData(config.metricNameRSRQ);
    assert.equal(metricPlot2, true);
    browser.element(parameter.incorrectQueryClose).click();
        /* Save the views stats as workspace */
    var workspacename = baseMethods.saveFileName("a");
    console.log(workspacename);
    workspacesObject.saveWorkspace(workspacename);
    var workspacesaved = browser.element("[data-test-id='"+workspacename+"']").isVisible();
    assert(workspacesaved,true);
        /* Drop Sequence Siagram  on top of Message view and restore the workspace saved above and verify if it restored all view in same state */
    browser.execute(() => {
        let obj = document.querySelector("[data-test-id='h-box-top']");
        obj.outerHTML = ''
    });
    browser.execute(() => {
        let obj = document.querySelector("[data-test-id='h-box-top']");
        obj.outerHTML = ''
    });
    genericObject.clickOnDTTab(parameter.ViewsMenu);        
    baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.WidgetTopBox);
    baseMethods.verifySpinnerVisibility();
    workspacesObject.openSavedWorkspace(workspacename);
    var checktab = browser.elements("[title='Map']").getText();
    console.log(checktab);
    expect(checktab).to.include("Map");
    var checktab = browser.elements("[title='Time Series']").getText();
    console.log(checktab);
    expect(checktab).to.include("Time Series");
    var checktab = browser.elements("[title='Messages']").getText();
    console.log(checktab);
    expect(checktab).to.include("Messages");
    browser.element(parameter.mapLayersTab).click();
    var layerValue1 = baseMethods.verifyLayerData(config.metricName);
    assert(layerValue1,true);
    browser.element(parameter.incorrectQueryClose).click();
    //There is a bug in application workflow, so commiting out below and will enable once it fix.
    //browser.element(parameter.TimeseriesLayerButton).click();
    var metricPlot3 = baseMethods.verifyTimeSeriesMetricLayerData(config.metricNameRSRQ);
    assert.equal(metricPlot3, true);
    browser.element(parameter.incorrectQueryClose).click();
    var messageData = browser.element("//div[@class='as-split-gutter']").isVisible();
    assert.equal(messageData,true);
        /* Close all views and delete the workspace */
    baseMethods.closeAllViews();        
    browser.pause(500);
    var wspacedel = workspacesObject.deleteWorkspace(workspacename);
    assert.equal(wspacedel,false);
    console.log('Workspace ' + workspacename + ' is deleted successfully');
    browser.element(parameter.incorrectQueryClose).click();
    browser.element(parameter.DataMenu).click();
    searchdataObject.newSearchPage();
    console.log("Test Completed - Workspace - Verified if restore workspace remembers the view state saved with multiple views: XYN-1160");
    });
});