let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    afterEach(function () {
        if(isElectron()){
        console.log("Clearing the Search keyword and results");
        baseMethods.SelectTabs("SEARCHDATA");
        searchdataObject.newSearchPage();
        console.log("Cleared the Search keyword and results");
        }
        
    });

    it('Verify if user is able to search by channel name with <=,=,>=,< conditions - XYN-1476', function(){
        console.log("Test Started - Verify if user is able to search by channel name with <=,=,>=,< conditions - XYN-1476");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryChannel(config.searchItemChannel,config.searchConditionLessThanEqualTo,config.searchTextForChannel);
        baseMethods.verifySpinnerVisibility();
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.treeNodeDropDownCollapsed).click();
        var result = searchdataObject.lessthanequalto();       
        assert(result,true);
        searchdataObject.clearsearchkeyword();
        console.log("verified Less than equal to");

        baseMethods.searchNQLQueryChannel(config.searchItemChannel,config.searchConditionEqualTo,config.searchTextForChannel);
        baseMethods.verifySpinnerVisibility();
        var result = searchdataObject.equalto();       
        assert(result,true);
        searchdataObject.clearsearchkeyword();
        console.log("verified equal to");

        baseMethods.searchNQLQueryChannel(config.searchItemChannel,config.searchConditionGreaterThanEqualTo,config.searchTextForChannel);        
        var result = searchdataObject.greaterthanequalto();   
        assert(result,true);
        searchdataObject.clearsearchkeyword();
        console.log("verified Greater than equal to");

        baseMethods.searchNQLQueryChannel(config.searchItemChannel,config.searchConditionLessThan,config.searchTextForChannel);   
        var result = searchdataObject.lessthan();
        assert(result,true);
        console.log("verified Less than to");
        console.log("Test completed - Verify if user is able to search by channel name with <=,=,>=,< conditions - XYN-1476");

    });
    
    it('Verify if user is able to search by channel name with >,!=,in conditions - XYN-1476', function(){
        console.log("Test Started - Verify if user is able to search by channel name with >,!=,in conditions - XYN-1476");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryChannel(config.searchItemChannel,config.searchConditionGreaterThan,config.searchTextForChannel);  
        baseMethods.verifySpinnerVisibility();
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.treeNodeDropDownCollapsed).click();  
        var result = searchdataObject.greaterthan();       
        assert(result,true);
        searchdataObject.clearsearchkeyword();        
        console.log("Verified greater than case");

        baseMethods.searchNQLQueryChannel(config.searchItemChannel,config.searchConditionNotEqualTo,config.searchTextForChannel);
        baseMethods.verifySpinnerVisibility();
        var result = searchdataObject.notequalto();       
        assert(result,true);
        searchdataObject.clearsearchkeyword();
        console.log("Verified not equal to case");

        var newQueryTab = browser.element(parameter.searchTextArea);
        newQueryTab.click();
        newQueryTab.keys("channel in ");
        var braces = browser.element(parameter.braces);
        braces.click();
        braces.keys(config.searchTextForChannelIn);
        var searchButton = browser.element(parameter.searchButton);
        searchButton.click();
        baseMethods.verifySpinnerVisibility();
        var result = searchdataObject.inconditions();
        // browser.pause(2000);
        // searchdataObject.selectdatasetresult();
        // var selectdataset = browser.element(parameter.filteredItem);
        // var visibility = selectdataset.isVisible();
        assert(result,true);
        //searchdataObject.clearsearchkeyword();
        console.log("Verified in case");
        console.log("Test Completed - Verify if user is able to search by channel name with >,!=,in conditions - XYN-1476");
    }); 

});