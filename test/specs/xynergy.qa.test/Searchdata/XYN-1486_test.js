let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    it('Verify if the Time filters are displayed on  clicking on [Filters] button - XYN-1486', function(){
        console.log("Test started - Verify if the Time filters are displayed on  clicking on [Filters] button - XYN-1486");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);
        var selectFilterBtn = searchdataObject.searchDataFilterButton();        
        var span = browser.elements("//div[@class='time-filter-modes']//li").getText();
        var timefiltercount = browser.elements("//div[@class='time-filter-modes']//li").value.length;
        console.log(timefiltercount);
        var j=1;
        var iTrue=0;
        while ((timefiltercount >= j) && (iTrue == 0)){
            var rowContent = browser.elements("//div[@class='time-filter-modes']//li[" + j +"]").getText(); 
            console.log(rowContent);  
            if (rowContent.includes("Last")){
                iTrue=1;
            }
            j++;
        }
        expect(span).to.include("No Time Filter");
        expect(span).to.include("Start Time");
        expect(span).to.include("End Time");
        expect(browser.element(parameter.dropdownHours).getText()).to.include("Hours"); 
        expect(browser.element(parameter.dropdownDays).getText()).to.include("Days");   
        expect(browser.element(parameter.dropdownWeeks).getText()).to.include("Weeks");
        console.log("Test completed - Verify if the Time filters are displayed on  clicking on [Filters] button - XYN-1486");
    });

});