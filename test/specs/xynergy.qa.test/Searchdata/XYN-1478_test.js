let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    it('XYN-1478_Verify if user gets a message when there are no records matching the search criteria', function () {
        console.log("Test started- XYN-1478_Verify if user gets a message when there are no records matching the search criteria");
       //baseMethods.SerchQueryInNewSearch("Dataset","contains","voice");
       baseMethods.SelectTabs("SEARCHDATA");
       baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,config.searchTextForFIleNameForError);
       var sText=browser.element(parameter.errorMessage).getText();
       console.log(sText+" text is displayed");
       assert.equal(sText,config.searchErrorMsg,sText+" actual status is not same as expected status"+config.searchErrorMsg);
       browser.element(parameter.errorMessageOK).click();
        console.log("Test completed - XYN-1478_Verify if user gets a message when there are no records matching the search criteria");
    });  

});