let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    it('Verify if user is able to search dataset which was saved moment ago using Save Search Results as - XYN-2784, XYN-1480,XYN-2929', function(){
        console.log("Test started - Verify if user is able to search dataset saved moment ago using Save Search Result as and in History and Browse Data - XYN-2784, XYN-1480,XYN-2929");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,config.searchTextForFIleName);
        searchdataObject.clickSaveasbutton();
        searchdataObject.selectsavesearchresultsas();
        //baseMethods.saveDatasetName("QATest");
        var dataset = baseMethods.saveFileName(config.datasetName);
        console.log(dataset);
        searchdataObject.enterSaveSearchResultNameValue(dataset);
        browser.element(parameter.saveSearchResultSaveBtn).click();
        browser.element(parameter.dataSetSaveCancelBtn).click();
        //baseMethods.SelectTabs("IMPORTDATA");
        baseMethods.selectdatasetinImportHistory(dataset);
        var open = browser.element(parameter.openBtn);
        var ienalbled = open.isEnabled();
        assert(ienalbled,true);
        browser.element(parameter.historyFilterClear).click();
        baseMethods.selectdatasetinBrowseDatasets(dataset);
        var open = browser.element(parameter.datasetsOpenBtn);
        var benalbled = open.isEnabled();
        assert(benalbled,true);
        browser.element(parameter.datasetsFilterClear).click();
        console.log("Test Completed - To verify if user is able to search dataset saved moment ago using Save Search Results as and in History and Browse data - XYN-2784, XYN-1480,XYN-2929");
    });

});