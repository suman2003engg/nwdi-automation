let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    it('XYN-1473_Verify if user is able to search by dataset ', function(){
        console.log("Test started - XYN-1473_Verify if user is able to search by dataset");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);
        // var selectdataset = browser.element(parameter.SearchResultPane);
        // var visibility = selectdataset.isVisible();
        // console.log(visibility);
        // assert(visibility, true);
        browser.pause(1000);
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.treeNodeDropDownCollapsed).click();
        var listItem = browser.elements(parameter.listItem).getText();
        console.log(listItem);
        var listItemlength = browser.elements(parameter.listItem).value.length;
        console.log("Dataset results count is " + listItemlength);
        var j=0;
        while (listItemlength > j){
            //var rowContent = browser.element(listItem[j]).getText();
            //console.log(rowContent);  
            expect(listItem[j].toLowerCase()).to.include(config.searchDatasetForRegularTest.toLowerCase());
            j++;
        }
        console.log("Verified each record from filter results if it matches with the search keyword");
        // var bStatus=baseMethods.verifyDataSetNameInSearchResult(config.datasetName);
        // console.log(bStatus);
        // assert(bStatus, true);
        console.log("Test completed - XYN-1473_Verify if user is able to search by dataset");
    });

});