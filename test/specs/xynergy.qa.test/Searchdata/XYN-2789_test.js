let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    it('XYN-2789_To verify search data retainity', function () {
        console.log("Test started- XYN-2789_To verify search data retainity");
        baseMethods.SelectTabs("SEARCHDATA");
       baseMethods.SerchQueryInNewSearch(config.searchItemFileName,config.searchCondition,config.searchTextForFIleName);
       var bResultStatus=browser.isExisting(parameter.searchResultPane);
       assert.equal(bResultStatus,true,bResultStatus+" actual status is not same as expected status"+true);
       browser.pause(1000);
       var sExpectedText=browser.element(parameter.resultFirstItem).getText();
       var sText=browser.element(parameter.filteredItem).getText();
       var sText1=sText.split(" ");
       console.log(sText1[0]);
       expect(sExpectedText.toLowerCase()).to.include(sText1[1].toLowerCase());
      // assert.equal(sText,config.searchSelectDataSet,sText+" actual status is not same as expected status"+config.searchSelectDataSet);
       // baseMethods.clickOnSearchData();
       baseMethods.SelectTabs("IMPORTDATA");
       var bImportdataexistance=browser.isExisting(parameter.addData);
       assert.equal(bImportdataexistance,true,bImportdataexistance+" actual status is not same as expected status"+true);
       baseMethods.SelectTabs("SEARCHDATA");
       var sText=browser.element(parameter.filteredItem).getText();
       console.log(sText);
       expect(sText.toLowerCase()).to.include(sText1[1].toLowerCase());
       //assert.equal(sText,config.searchSelectDataSet,sText+" actual status is not same as expected status"+config.searchSelectDataSet);
        console.log("Test completed - XYN-2789_To verify search data retainity");
    });  

});