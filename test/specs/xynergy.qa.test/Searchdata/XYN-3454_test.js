let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    it('Verify that when user opens the same dataset again then it should not trigger add/replace message - XYN-3454', function(){
        console.log("Test started - Verify that when user opens the same dataset again then it should not trigger add/replace message - XYN-3454");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);        
        var searchButton = browser.element(parameter.searchButton);
        searchButton.click();
        var openButton = browser.element(parameter.searchDataOpenBtn);
        openButton.click();
        browser.pause(100);
        var dataTab = browser.element(parameter.DataMenu);
        dataTab.click();        
        openButton.click();
        browser.pause(100);
        var openDataTitleBar = browser.element(parameter.sameDatasetDialogWindow);
        var visibility = openDataTitleBar.isVisible();         
        assert(visibility,true);
        var openDataTitleBarClose = browser.element(parameter.sameDatasetDialogWindowClose);
        openDataTitleBarClose.click();        
        dataTab.click();             
        console.log("Test completed - Verify that when user opens the same dataset again then it should not trigger add/replace message - XYN-3454");
    }); 

});