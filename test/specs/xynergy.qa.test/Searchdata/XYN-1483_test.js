let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    it('Verify if user is able to open the selected search result in canvas - XYN-1483', function(){
        console.log("Test Started - Verify if user is able to open the selected search result in canvas - XYN-1483");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,config.searchTextForFIleName);
        searchdataObject.selectdatasetresult();
        var selectdataset = browser.element(parameter.filteredItem);
        var visibility = selectdataset.isVisible();
        assert(visibility,true);
        var selecteddataset = selectdataset.getText();
        console.log(selecteddataset);
        var sText = selecteddataset.split(" ");
        searchdataObject.clickOpeninCanvasbutton();
        baseMethods.openInCanvasValidation();
        baseMethods.verifySpinnerVisibility();
        var scope = browser.element(parameter.scope);
        if(!(scope.getAttribute("class")).includes('enabled')){
            scope.click();
        }
        browser.pause(1000);
        var verifyDataset = browser.element("[data-test-id='"+sText[1]+"']");
        console.log(sText[1]);
        browser.pause(300);
        var visibility1 = verifyDataset.isVisible();
        //console.log(visibility1);
        assert(visibility1,true);
        //browser.pause(200);
        browser.element(parameter.DataMenu).click();
        console.log("Test completed - Verify if user is able to open the selected search result in canvas - XYN-1483");
    }); 

});