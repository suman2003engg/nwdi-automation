let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    it('Verify if user is able to save the search parameters - XYN-1479', function(){
        console.log("Test started - Verify if user is able to save the search parameters - XYN-1479");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);
        searchdataObject.selectdatasetresult();
        var saveresultas = searchdataObject.clickSaveasbutton();
        expect(browser.element(parameter.saveResultBtn).getText()).to.include(config.saveSearchTerm);
        expect(browser.element(parameter.saveSearchResult).getText()).to.include(config.saveSearchResultAs);
        searchdataObject.selectsavesearchteamsas();
        var datasetname = baseMethods.saveFileName(config.searchTermName);
        searchdataObject.enterSaveSearchTermsResultNameValue(datasetname);
        browser.element(parameter.saveSearchResultSaveBtn).click();
        browser.pause(500);
        expect(browser.element(parameter.mmc).getText()).to.include(config.searchTermSave);
        expect(browser.element(parameter.openSavedResult).getText()).to.include(config.open);
        expect(browser.element(parameter.dataSetSaveCancelBtn).getText()).to.include(config.close);
        searchdataObject.DataSetSaveCancelBtn();
        console.log("Test completed - Verify if user is able to save the search parameters - XYN-1479");
    });

});