let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    afterEach(function () {
        if(isElectron()){
        console.log("Clearing the Search keyword and results");
        baseMethods.SelectTabs("SEARCHDATA");
        searchdataObject.newSearchPage();
        console.log("Cleared the Search keyword and results");
        }
        
    });

    it('Verify the functinality of Open on Search Data- XYN-1488', function(){
        console.log("Test Started - Verify the functinality of cancel button for Open Search Terms for Search Data XYN-1488");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.SelectSearchDataTabs("NEWSEARCH");
        baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,config.searchTextForFIleName);        
        searchdataObject.selectdatasetresult();
        var selectdataset = browser.element(parameter.filteredItem);
        var visibility = selectdataset.isVisible();
        assert(visibility, true);
        searchdataObject.clickSaveasbutton();
        searchdataObject.selectsavesearchteamsas();
        // var savesearchterm = browser.element("[data-test-id='modal-title-bar']");
        // var visibility = savesearchterm.isVisible();
        // assert(visibility, true);
        var datasetname = baseMethods.saveFileName(config.searchTermName);
        console.log(datasetname);
        searchdataObject.enterSaveSearchTermsResultNameValue(datasetname);
        browser.element(parameter.saveSearchResultSaveBtn).click();
        browser.element(parameter.dataSetSaveCancelBtn).click();
        searchdataObject.clearsearchkeyword();
        baseMethods.SelectTabs("SEARCHDATA");      
        baseMethods.SelectSearchDataTabs("OPENSEARCHTERMS");
        baseMethods.verifySpinnerVisibility();
        textbox = browser.element(parameter.openSearchFilter).click();     
        textbox.keys(datasetname);
        browser.element(parameter.openFilteredItem).click();
        searchdataObject.clickCancelinOpenSavedSearch();
        console.log("Test completed - Verify the functinality of cancel button for Open Search Terms for Search Data XYN-1488");
    });

    it('Verify the functinality of Open on Search Data- XYN-1488', function(){
        console.log("Test Started - Verify the functinality of open button for Open Search Terms for Search Data XYN-1488");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.SelectSearchDataTabs("NEWSEARCH");
        baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,config.searchTextForFIleName);        
        searchdataObject.selectdatasetresult();
        var selectdataset = browser.element(parameter.filteredItem);
        var visibility = selectdataset.isVisible();
        assert(visibility, true);
        searchdataObject.clickSaveasbutton();
        searchdataObject.selectsavesearchteamsas();
        // var savesearchterm = browser.element("[data-test-id='modal-title-bar']");
        // var visibility = savesearchterm.isVisible();
        // assert(visibility, true);
        var datasetname = baseMethods.saveFileName(config.searchTermName);
        console.log(datasetname);
        searchdataObject.enterSaveSearchTermsResultNameValue(datasetname);
        browser.element(parameter.saveSearchResultSaveBtn).click();
        browser.element(parameter.dataSetSaveCancelBtn).click();
        browser.pause(500);
        searchdataObject.clearsearchkeyword();
        baseMethods.SelectTabs("SEARCHDATA");      
        baseMethods.SelectSearchDataTabs("OPENSEARCHTERMS");
        baseMethods.verifySpinnerVisibility();
        textbox = browser.element(parameter.openSearchFilter).click();     
        textbox.keys(datasetname);
        browser.element(parameter.openFilteredItem).click();
        browser.element(parameter.openSavedSearchBtn).click();
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        var selectdataset = browser.element(parameter.filteredItem);
        selectdataset.isVisible();
        assert(selectdataset, true);
        baseMethods.verifySpinnerVisibility(); 
        browser.pause(500);
        var selectSearchHeader = browser.element(parameter.newSearchHeader).getText();
        expect(selectSearchHeader).to.include(config.searchTermName);
        var selectTextFileName = browser.element(parameter.savedSearchQuery).getText();
        expect(selectTextFileName).to.include(config.searchItemFileName);
        expect(selectTextFileName).to.include(config.searchCondition);
        expect(selectTextFileName).to.include(config.searchTextForFIleName);
        console.log("Test Completed - Verify the functinality of open button for Open Search Terms for Search Data XYN-1488")

    });

});