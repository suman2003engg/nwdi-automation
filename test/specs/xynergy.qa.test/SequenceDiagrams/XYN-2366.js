let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Sequence Diagrams', function () {
    var init = 0;
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }

    });

    it('Verify whether event symbol is displaying on the sequence diagram- XYN-2366', function () {
        console.log("Test started- event symbol is displaying on the sequence diagram XYN-2366");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        var j = 1;
        rowCount = browser.elements(parameter.seqRows).value.length;
        //console.log(rowCount);
        while (rowCount > 0) {
            var rowImg = browser.elements("//xyn-sequence-item[" + j + "]//div[@class='end-marker left']");
            var rowContent = browser.elements("//xyn-sequence-item[" + j + "]//div//span[@class='content-txt']").getText();
            //console.log(rowContent);
            if (rowContent == config.eventName) {
                var imgVal = browser.elements("//xyn-sequence-item[" + j + "]//div[@class='end-marker left']//img").isVisible();
                assert.equal(imgVal, true);
                break;
            }

            j++;
            rowCount--;
        }
        baseMethods.ClearSearchedEvent();
        console.log("Test completed- event symbol is displaying on the sequence diagram XYN-2366");
    });

});