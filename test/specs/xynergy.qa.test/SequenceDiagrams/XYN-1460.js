let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Sequence Diagrams', function () {
    var init = 0;
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }


    });

    it("Verify that time stamp is showing time on sequence diagram on clicking - XYN-1460", function () {
        console.log("Timestamp should be visible on sequence diagram when clicked on Show Timestamps");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        var buttonActive = browser.element(parameter.timeStampsBtn).getAttribute("class");
        expect(buttonActive).to.include(config.activeBtn);
        browser.pause(300);
        var result = browser.element(parameter.timeStampsText).getAttribute("style");
        expect(result).to.include(config.block);
        SequenceDiagramObject.clickOnShowTimestampsBtn();
        buttonActive = browser.element(parameter.timeStampsBtn).getAttribute("class");
        expect(buttonActive).not.to.include(config.activeBtn);
        console.log("Test completed - Timestamp should be visible on sequence diagram when clicked on Show Timestamps - XYN-1460");

    });

});