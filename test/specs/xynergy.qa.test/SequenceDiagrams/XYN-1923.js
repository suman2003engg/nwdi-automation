let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Sequence Diagrams', function () {
    var init = 0;
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }

    });

    it('To verify legend and Node name in sequence diagram- XYN-1923', function () {
        console.log("Test started- To verify legend and node name in sequence diagram- XYN-1923");
        baseMethods.clickOnAdvancedSearchViewIcon_ALF();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        var LegendTitle = browser.element("[data-test-id='legend-title']").getText();
        assert(LegendTitle == "Data Source");
        var NodeHeader = browser.element("[data-test-id='right-node-header-text']").getText();
        assert(NodeHeader == "UXM")
        console.log("Test completed- To verify legend and node name in sequence diagram- XYN-1923");

    });

});