let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Sequence Diagrams', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
        }
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);

    });
    it('Sequence diagram - dropping an unavailable event onto sequence diagram - XYN-4139', function () {
        console.log("Test started - Sequence diagram - dropping an unavailable event onto sequence diagram - XYN-4139");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        //look for unavailable event and plot on SD and verify
        baseMethods.plotUnAvailableEvent(config.eventName3, parameter.nrSCGFailure, parameter.seqDroppable);
        assert.equal(browser.element(parameter.noDataMsg).isVisible(), true);
        var noDataMessageInfo = browser.element(parameter.noDataMsg).getText();
        expect(noDataMessageInfo).to.include(config.noDataMsg + config.eventName3); 
        SequenceDiagramObject.clickOKNoDataMsgBtn();
        //plot Available Event and verify
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        SequenceDiagramObject.clickColumnBtn();
        baseMethods.clickOnSequenceData(config.dataLayersTab);        
        browser.pause(300);
        var verifyToolTipForEvent = browser.element(parameter.tooltipValue).isVisible();
        assert.equal(verifyToolTipForEvent, true);
        //close column window
        SequenceDiagramObject.clickColumnBtn();
        //Again, look for unavailable event and plot on SD and verify
        baseMethods.plotUnAvailableEvent(config.eventName3, parameter.nrSCGFailure, parameter.seqDroppable);
        assert.equal(browser.element(parameter.noDataMsg).isVisible(), true);
        var noDataMessageInfo = browser.element(parameter.noDataMsg).getText();
        expect(noDataMessageInfo).to.include(config.noDataMsg + config.eventName3); 
        SequenceDiagramObject.clickOKNoDataMsgBtn();
        console.log("Test completed - Sequence diagram - dropping an unavailable event onto sequence diagram - XYN-4139");
    });

}); 