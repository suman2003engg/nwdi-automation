let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Sequence Diagrams', function () {
    var init = 0;
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }

    });

    it('To verify selection of messages for latencty calculation- XYN-1466', function () {
        console.log("Test started- To verify selection of messages for latencty calculation- XYN-1466");
        // baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        //browser.waitForExist("[data-test-id='diagram-view-droppable']");
        SequenceDiagramObject.clickOnCalculateLatency();
        SequenceDiagramObject.selectDataToCalculateLatency();
        SequenceDiagramObject.clickOnCalculateBtn();
        var calculatedLatency = browser.element(parameter.calculateLatency);
        var latency = calculatedLatency.isVisible();
        assert(latency, true);
        console.log("Test completed- To verify selection of messages for latencty calculation- XYN-1466");

    });

});