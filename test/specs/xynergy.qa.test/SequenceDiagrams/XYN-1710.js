let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Sequence Diagrams', function () {
    var init = 0;
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }

    });

    it('Verify events and latency calculation sync with data layers, XYN-1710', function () {
        console.log("Test started to verify sync between Events and latency calculation with data layers - XYN-1710");
        // baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        baseMethods.clickOnColumnsbutton();
        var datalayer = browser.element(parameter.dataLayersTab);
        datalayer.click();
        SequenceDiagramObject.clickOnCalculateLatency();
        SequenceDiagramObject.selectDataToCalculateLatency();
        SequenceDiagramObject.clickOnCalculateBtn();
        var CalculatedLatency = browser.element(parameter.calculateLatency);
        var bool = CalculatedLatency.isVisible();
        assert(bool, true);
        baseMethods.clickOnHideBtn();
        var CalculatedLatencyVisible = browser.isVisible(parameter.calculateLatency);
        assert.equal(CalculatedLatencyVisible, false);
        baseMethods.clickOnCloseBtn();
        var CalculatedLatencyVisible = browser.isVisible(parameter.calculateLatency);
        assert.equal(CalculatedLatencyVisible, false);
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        var datalayerEvent = browser.element(parameter.seqLayerData);
        var text = datalayerEvent.getText();
        expect(text).to.include(config.eventName);
        baseMethods.clickOnHideBtn();
        var j = 1;
        rowCount = browser.elements("//xyn-sequence-item").value.length;
        //console.log(rowCount);
        while (rowCount > 0) {
            var rowImg = browser.elements("//xyn-sequence-item[" + j + "]//div[@class='end-marker left']");
            var rowContent = browser.elements("//xyn-sequence-item[" + j + "]//div//span[@class='content-txt']").getText();
            //console.log(rowContent);
            if (rowContent == config.eventName) {
                var img = rowImg.element("//img").isVisible();
                assert.equal(img, false);
                break;
            }

            j++;
            rowCount--;
        }
        baseMethods.clickOnCloseBtn();
        var NoLayers = browser.isVisible(parameter.noLayerMsg);
        assert.equal(NoLayers, true);
        baseMethods.ClearSearchedEvent();
        console.log("Test Completed - Verified sync between Events and latency calculation with data layers - XYN-1710");
    });

});