let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var SequenceDiagramObject = require('../../../pageobjects/sequenceDiagramPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Sequence Diagrams', function () {
    var init = 0;
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if (isElectron()) {
            if (init == 0) {
                baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
                init = 1;
            }
        }

    });

    it('Verify of two lanes in sequence diagram- XYN-1739', function () {
        console.log("Test started- verification of two lanes in the sequence diagram XYN-1739");
        // baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.SequencediagramDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        var j = 1;
        var sequenceBlock = browser.elements(parameter.seqRows);
        var headerBlock = sequenceBlock.elements(parameter.seqHeaderBlock);
        var leftItem = headerBlock.element(parameter.seqHeaderLineLeft);
        expect(leftItem.getText()).to.include(config.sequenceLeftLaneItemName);
        var rightItem = headerBlock.element(parameter.seqHeaderLineRight);
        expect(rightItem.getText()).to.include(config.sequenceRightLaneItemName);
        baseMethods.ClearSearchedEvent();
        console.log("Test completed- verification of two lanes in the sequence diagram XYN-1739");
    });

});