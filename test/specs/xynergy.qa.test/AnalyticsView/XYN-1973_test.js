let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var config = require('../../../config');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Analytics View', function () {
    var init=0;
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
        }       
    });
    afterEach(function () {
        
    });
    it('Verify if views are aligned in Alphabetical order - XYN-1973', function () {
        console.log("Test started XYN-1973, Verify if views are aligned in Alphabetical order");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        var elem=browser.elements("//analytics-view//views-menu//*[contains(@data-test-id,'WidgetUniqueId')]");
        var i=1,j;
        while(i<elem.value.length){
            console.log(i);
            var ele1=browser.element("//analytics-view//views-menu//*[contains(@data-test-id,'WidgetUniqueId')]["+i+"]");
            console.log(ele1.getText());
            j=i+1;
            var ele2=browser.element("//analytics-view//views-menu//*[contains(@data-test-id,'WidgetUniqueId')]["+j+"]");
            console.log(ele2.getText());
            i++;
            var res=ele2.getText().localeCompare(ele1.getText());
            console.log("res",res);
            assert.equal(res,parseInt(config.compareValueWithOne));           
        }
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        console.log("Test completed XYN-1973, Verify if views are aligned in Alphabetical order");
    });
    

});