let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var config = require('../../../config');
let parameter = require('../../parameters.js');
var workspacesObject=require('../../../pageobjects/workspacesPage');
var assert = require('assert');
var tags = require('mocha-tags');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Analytics View', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            
        }       
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
    });
    //tags('sanity').
    it('To verify Close All functioning in Views - XYN-2319', function () {
        console.log("Test started XYN-2319, To verify Close All functioning in Views");
        var workspacename=baseMethods.saveFileName("a-closeViews");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.TableDraggable, parameter.WidgetRightBox);
        browser.pause(400);
        var widgetsCountBeforeCloseAllViews=browser.elements(parameter.widgetHeaderContainer);
        console.log(widgetsCountBeforeCloseAllViews.getText());
        console.log(widgetsCountBeforeCloseAllViews.value.length);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        genericObject.clickOnCloseAllViews();
        genericObject.clickOnViewsSaveAsBtn();
        genericObject.inputWorkSpaceName(workspacename);
        genericObject.clickOnViewsWorkSpaceSave();
        browser.pause(100);
        genericObject.clickOnCrossOnViewsTabToCloseTheTab();
        var widgetsCountAfterCancellingCloseAllViews=browser.elements(parameter.widgetHeaderContainer);
        workspacesObject.deleteWorkspace(workspacename);       
        console.log(widgetsCountAfterCancellingCloseAllViews.getText());       
        console.log(widgetsCountAfterCancellingCloseAllViews.value.length);
        //assert.equal(widgetsCountAfterCancellingCloseAllViews.value.length,parseInt(config.compareValueWithZero));
        expect(widgetsCountBeforeCloseAllViews.value.length).to.be.not.equal(widgetsCountAfterCancellingCloseAllViews.value.length);

        console.log("Test completed XYN-2319, To verify Close All functioning in Views");
    });

});