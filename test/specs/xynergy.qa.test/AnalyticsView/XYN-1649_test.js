let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var config = require('../../../config');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Analytics View', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            
        }   
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);    
    });
    it('Verify Drop here to add to all applicable views for KPI/event plots - XYN-1649', function () {
        console.log("Test started XYN-1649, Verify Drop here to add to all applicable views for KPI/event plots");
        var elem=browser.element(parameter.MessageTemplate);
        assert.equal(elem.getText(),config.message_template_text);
        console.log("Test completed XYN-1649, Verify Drop here to add to all applicable views for KPI/event plots");
    });
});