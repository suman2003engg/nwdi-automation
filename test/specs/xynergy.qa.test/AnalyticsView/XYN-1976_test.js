let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var config = require('../../../config');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Analytics View', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            
        }   
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);    
    });
    it('To Verify dialog box in Analytic view - XYN-1976', function () {
        console.log("Test started XYN-1976, to verify text message in analytics view");
        var elem=browser.element(parameter.MessageTemplate);
        assert.equal(elem.getText(),config.message_template_text);
        console.log("Test completed XYN-1976, to verify text message in analytics view");
    });
});