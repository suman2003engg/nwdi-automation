let baseMethods = require('../../testbase');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
let browseDataObject = require('../../../pageobjects/browseDataPage.js');
let importDataObject = require("../../../pageobjects/importDataPage");
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var scopeDataObject = require('../../../pageobjects/scopeWidgetPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Browse data page', function () {
    var dataset = baseMethods.saveFileName(config.datasetName);
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    it('Browse Data - To verify Filtering text with double quotes in Datasets- XYN-2765 , Verification of delete button in Browse data - XYN-2865', function () {
    console.log("Test started - Browse Data - To verify Filtering text with double quotes in Datasets- XYN-2765 , Verification of delete button in Browse data - XYN-2865");
    baseMethods.SelectTabs(config.searchDataTab);
    baseMethods.searchNQLQueryCustom(config.searchItemdataset, config.searchCondition, config.searchDatasetForRegularTest);
    searchdataObject.clickSaveasbutton();
    searchdataObject.selectsavesearchresultsas();
    var datasetname = baseMethods.saveFileName(config.datasetName);
    console.log(datasetname);
    browser.pause(1000);
    searchdataObject.enterSaveSearchResultNameValue(datasetname);
    browser.pause(500);
    browser.element(parameter.saveSearchResultSaveBtn).click();
    browser.pause(500);
    browser.element(parameter.dataSetSaveCancelBtn).click();
    baseMethods.SelectTabs(config.browseDataTab);
    browseDataObject.LastFilters(config.LastFilterWeeksValue);
    browseDataObject.LastdropdownWeeks();
    browseDataObject.clickApplyBtn();
    browser.pause(200);
    browseDataObject.enter_datsetname('"' + datasetname + '"');
    browser.pause(1000);
    var openDisabled = browser.element(parameter.openDataDisable).getAttribute(config.class);
    expect(openDisabled).to.include(config.disabled);
    var delDisabled = browser.element(parameter.deldataDisable).getAttribute(config.class);
    expect(delDisabled).to.include(config.disabled);
    browser.element(parameter.checkMarkLeft).click();
    browser.pause(500);
    browser.element(parameter.datasetsOpenBtn).click();
    browser.pause(500);
    var ScopePanel = browser.element(parameter.scope);
    var visibility = ScopePanel.isVisible();
    assert(visibility, true);
    browser.element(parameter.DataMenu).click();
    browser.pause(200);
    browser.element(parameter.datasetsFilterClear).click();
    browser.pause(200);
    baseMethods.SelectTabs(config.searchDataTab);
    browser.pause(200);
    searchdataObject.clearsearchkeyword();
    console.log("Test completed - Browse Data - To verify Filtering text with double quotes in Datasets- XYN-2765 , Verification of delete button in Browse data - XYN-2865");
});
});