let baseMethods = require('../../testbase');
var genericObject = require('../../../pageobjects/genericobjects.js');
let parameter = require('../../parameters.js');
let browseDataObject = require('../../../pageobjects/browseDataPage.js');
let config = require('../../../config.js');
var assert = require('assert');
require('../../commons/global.js')();
describe('Browse data test', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    it('Browse Data - Replace already opened Data XYN-2983',function(){
        console.log("XYN-2983 test started");
        baseMethods.SelectTabs(config.browseDataTab);
        browseDataObject.applyFilterByDays(config.LastFilterDaysValue);
        browseDataObject.enter_datsetname(config.searchDatasetForRegularTestMO);
        browser.pause(2000);
        browser.element(parameter.checkMarkLeft).click();
        browser.pause(200);
        browser.element(parameter.datasetsOpenBtn).click();
        browser.pause(500);
        assert.equal(genericObject.isDataSetInScopePresent(config.searchDatasetForRegularTestMO),true);
        browser.element(parameter.DataMenu).click();
        browser.pause(200);
        browser.element(parameter.datasetsFilterClear).click();
        browser.pause(200);
        browseDataObject.enter_datsetname(config.searchALFDataset);
        browser.pause(2000);
        browser.element(parameter.checkMarkLeft).click();
        browser.pause(200);
        browser.element(parameter.datasetsOpenBtn).click();
        browser.pause(200);
        genericObject.replaceDataSetWithoutSaving();
        browser.pause(500);
        assert.equal(genericObject.isDataSetInScopePresent(config.searchALFDataset),true);
        assert.equal(genericObject.isDataSetInScopePresent(config.searchDatasetForRegularTestMO),false);       
        console.log("XYN-2983 Test completed");
    });
});