let baseMethods = require('../../testbase');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
let browseDataObject = require('../../../pageobjects/browseDataPage.js');
let importDataObject = require("../../../pageobjects/importDataPage");
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var scopeDataObject = require('../../../pageobjects/scopeWidgetPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Browse data page', function () {
    var dataset = baseMethods.saveFileName(config.datasetName);
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    it('Verify delete datasets associated with a workspace - XYN-2868', function(){
        console.log("Test started - Verify delete datasets associated with a workspace - XYN-2868");
        var dataset = baseMethods.saveFileName(config.datasetName);
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
        console.log(dataset);
        importDataObject.enterDataSetName(dataset);
        browser.pause(200);
        browseDataObject.fileUpload(dataset);
        browser.pause(3000);
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(dataset);
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        baseMethods.openInCanvasValidation();
        browser.pause(500);
        baseMethods.verifySpinnerVisibility();
        console.log("1");
        browser.pause(500);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.MapDroppable);
        browser.element(parameter.mapLayersTab).click();
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        browser.pause(1000);
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        //browser.element(parameter.datasetsFilterClear).click();
        baseMethods.selectdatasetinBrowseDatasets(config.searchALFDataset);
        open.click();
        //baseMethods.clickOnSearchViewIcon(config.searchALFDataset);
        browser.element(parameter.dataScopeReplaceBtn).click();
        browser.element(parameter.dataScopeReplaceSaveBtn).click();
        var workspacename = baseMethods.saveFileName("a");
        searchdataObject.enterSaveWorkspaceNameValue(workspacename);
        browser.element(parameter.workspaceSaveBtn).click();
        browser.pause(2000);
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element("[data-test-id='"+workspacename+"']").click();
        browser.element(parameter.workspaceOpenBtn).click();
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        var checktab = browser.elements("[title='Map']").getText();
        console.log(checktab);
        //tabvisible = browser.elements(checktab[0]).isVisible();
        //var tabvisible = checktab.isVisible();
        //assert(tabvisible,true);
        expect(checktab).to.include("Map");
        browser.element(parameter.mapLayersTab).click();
        browser.pause(1000);
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        browser.pause(500);
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        // if (openBtn.isVisible()){
        //     browser.element(parameter.selectCheckBox).click();
        //     browser.element(parameter.datasetsFilterClear).click();
        // }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        //browser.element(parameter.datasetsFilterClear).click();
        baseMethods.selectdatasetinBrowseDatasets(dataset);
        browseDataObject.deleteBtn();
        browseDataObject.confirmDeleteBtn();
        browser.pause(2000);
        var workspaceList = browser.elements(parameter.workspaceList).getText();
        console.log(workspaceList);
        var listItemLength = browser.elements(parameter.workspaceList).value.length;
        console.log(listItemLength);
        var result = false;
        var i = 0;      
        var value1=0;
        while (listItemLength > i) {
            //value1= workspaceList[i];
            //console.log(value1);
            //console.log(compare);
            if (workspaceList == workspacename){
                result = true;
            }
            else {
                console.log("One or more value(s) from results is not correct with search query");
                result = false;
            }
            i++;
        }
        assert(result,true);
        browseDataObject.confirmDeleteBtn();
        baseMethods.verifySpinnerVisibility();
        browser.pause(3000);
        browser.element(parameter.datasetsFilterClear).click();
        browser.pause(1000);
        baseMethods.cLickFilesTab(config.datasetsTab);
        browseDataObject.filterDataSetName(dataset);
        browser.pause(1000);
        var errorMsg = browser.element(parameter.noData).isVisible();
        assert(errorMsg,true);
        browser.element(parameter.datasetsFilterClear).click();
        console.log("Test completed - Verify delete datasets associated with a workspace - XYN-2868");
    });
});
