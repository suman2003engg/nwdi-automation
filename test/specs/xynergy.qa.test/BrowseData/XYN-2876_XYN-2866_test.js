let baseMethods = require('../../testbase');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
let browseDataObject = require('../../../pageobjects/browseDataPage.js');
let importDataObject = require("../../../pageobjects/importDataPage");
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var scopeDataObject = require('../../../pageobjects/scopeWidgetPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Browse data page', function () {
    var dataset = baseMethods.saveFileName(config.datasetName);
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });

    afterEach(function () {
        if (isElectron()) {
            console.log("Test Completed");
        }
    });
    it('Verification of adding datasets using a deleted dataset name: XYN-2876 ,  Verify if user is able to delete datasets XYN-2866', function () {
        console.log("Test start - Delete a dataset and create it again with same name- XYN-2876, Verify if user is able to delete datasets XYN-2866");
        //Import data to create a dataset        
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
        console.log(dataset);
        importDataObject.enterDataSetName(dataset);
        browser.pause(200);
        browseDataObject.fileUpload(dataset);
    });
    it('Verification of adding datasets using a deleted dataset name: XYN-2876 ,  Verify if user is able to delete datasets XYN-2866', function () {
        console.log("Test start - Delete a dataset and create it again with same name- XYN-2876, Verify if user is able to delete datasets XYN-2866");
        //Import data to create a dataset        
        // baseMethods.SelectTabs(config.browseDataTab);
        // browseDataObject.LastFilters(config.LastFilterDaysValue);
        // browseDataObject.LastdropdownDays();
        // browseDataObject.clickApplyBtn();
        // browser.pause(200);
        // browseDataObject.enter_datsetname('"' + dataset + '"');
        // browser.pause(2000);
        // browser.element(parameter.checkMarkLeft).click();
        // browser.pause(200);
        baseMethods.selectDataSetFromBrowseData(dataset);
        browseDataObject.deleteBtn();
        browser.pause(5000);
        var delDatasetsWarning = browser.element(parameter.delDatasetsWarning).isVisible();
        assert(delDatasetsWarning, true);
        browseDataObject.confirmDeleteBtn();
        browser.pause(3000);
        browseDataObject.filterDataSetName(dataset);
        browser.pause(1000);
        var errorMsg = browser.element(parameter.noData).isVisible();
        if (errorMsg == true) {
            browser.pause(200);
            assert(errorMsg, 1);
            browser.pause(200);
            browseDataObject.importIconClick();
            browser.pause(200);
            browseDataObject.addDataIconClick();
            browser.pause(200);
            importDataObject.enterDataSetName(dataset);
            browser.pause(300);
            browseDataObject.fileUpload(dataset);
            baseMethods.SelectTabs(config.browseDataTab);
            browser.pause(200);
            browser.element(parameter.checkMarkLeft).click();
            browser.pause(200);
            var replaceddataset = browser.element(parameter.replacedData);
            expect(replaceddataset.getText()).to.include(dataset);
            browser.element(parameter.datasetsFilterClear).click();
            console.log("Test end - Delete a dataset and create it again with same name XYN-2876,  Verify if user is able to delete datasets XYN-2866");

        }
        else {
            console.log("Dataset not deleted");
            browser.element(parameter.datasetsFilterClear).click();
        }
    });

});
