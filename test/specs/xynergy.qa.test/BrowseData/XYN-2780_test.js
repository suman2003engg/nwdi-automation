let baseMethods = require('../../testbase');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
let browseDataObject = require('../../../pageobjects/browseDataPage.js');
let importDataObject = require("../../../pageobjects/importDataPage");
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var scopeDataObject = require('../../../pageobjects/scopeWidgetPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Browse data test', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    it('Browse Data - the selections should be remembered. XYN-2780',function(){
        console.log("XYN-2780 test started");
        baseMethods.SelectTabs(config.browseDataTab);
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        browseDataObject.enter_datsetname(config.searchDatasetForRegularTestMO);
        browser.pause(2000);
        browser.element(parameter.checkMarkLeft).click();
        browser.pause(200);
        importDataObject.clickImportDataMenu();
        baseMethods.SelectTabs(config.browseDataTab);
        browser.pause(4000);


        console.log("XYN-2780 Test completed");

    })
});