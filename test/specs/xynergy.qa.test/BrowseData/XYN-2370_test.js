let baseMethods = require('../../testbase');
var genericObject = require('../../../pageobjects/genericobjects.js');
let parameter = require('../../parameters.js');
let browseDataObject = require('../../../pageobjects/browseDataPage.js');
let config = require('../../../config.js');
var assert = require('assert');
require('../../commons/global.js')();
describe('Browse data test', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    //Uncomment below script once it got fixed for XYN-2370
    /*
    it('To verify whether filtering is working for every column: XYN-2370', function(){
        console.log("Test start - To verify whether filtering is working for every column - For 'Last' tab - Days");               
        baseMethods.SelectTabs("BROWSEDATA");         
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.clickApplyBtn();
        browseDataObject.clickLastDownBtn();        
        browseDataObject.clickApplyBtn();
        browseDataObject.clickLastUpBtn();
        browseDataObject.clickApplyBtn(); 
        var errorMsg = browser.element("[data-test-id='No Data Available']").isVisible();
        if (errorMsg == true){    
            console.log("No data available in given date range");   
        } 
        else {
            var gridXPath = "//tab[@name='Datasets']//div[@class='content-section']//datatable-scroller//div[@class='datatable-row-center datatable-row-group']/datatable-body-cell[3]";
            browseDataObject.getCreatedGridList(gridXPath);        
        }     
        console.log("Test End - To verify whether filtering is working for every column - For 'Last' tab - Days");
    });
    it('To verify whether filtering is working for every column: XYN-2370', function(){
        console.log("Test start - To verify whether filtering is working for every column - For 'Last' tab - Weeks");               
        baseMethods.SelectTabs("BROWSEDATA");         
        browseDataObject.LastFilters(config.LastFilterWeeksValue);                
        browseDataObject.LastdropdownWeeks()
        browseDataObject.clickApplyBtn();
        baseMethods.verifySpinnerVisibility();
        var errorMsg = browser.element("[data-test-id='No Data Available']").isVisible();
        if (errorMsg == true){    
            console.log("No data available in given date range");   
        } 
        else {
            var gridXPath = "//tab[@name='Datasets']//div[@class='content-section']//datatable-scroller//div[@class='datatable-row-center datatable-row-group']/datatable-body-cell[3]";
            browseDataObject.getCreatedGridList(gridXPath);        
        }
        console.log("Test End - To verify whether filtering is working for every column - For 'Last' tab - Weeks");
    });    
    it('To verify whether filtering is working for every column: XYN-2370', function () {
        console.log("Test start - To verify whether filtering is working for every column - For 'Start Time' tab");
        baseMethods.SelectTabs("BROWSEDATA");
        var Dates = browseDataObject.getDates();
        var StartDate = Dates[0];
        var EndDate = Dates[1]
        console.log(StartDate, EndDate);
        baseMethods.setStartTime(StartDate, EndDate);
        browser.pause(500);
        new_sTime = config.StartTime + config.amORpm;
        new_eTime = config.EndTime + config.amORpm;
        console.log(new_sTime + "," + new_eTime);
        browser.pause(500);
        browseDataObject.setTime_1_2(new_sTime, new_eTime)
        browser.pause(500);
        browseDataObject.clickApplyBtn();
        browser.pause(500);
        var errorMsg = browser.element("[data-test-id='No Data Available']").isVisible();
        if (errorMsg == true){    
            console.log("No data available in given date range");   
        } 
        else {
            var fTime = StartDate + " " + config.StartTime + " " + config.amORpm;
            var lTime = EndDate + " " + config.EndTime + " " + config.amORpm;
            console.log(fTime + "," + lTime);
            new_fTime = browseDataObject.ConvToUNIXdates(fTime);
            new_lTime = browseDataObject.ConvToUNIXdates(lTime);
            var list = browser.elements("//tab[@name='Datasets']//div[@class='content-section']//datatable-scroller//div[@class='datatable-row-center datatable-row-group']/datatable-body-cell[3]");
            var noOfRows = list.value.length
            console.log(noOfRows);
            var j=0;
            while (noOfRows >=1 ){
                var elem = browser.elements("//tab[@name='Datasets']//div[@class='content-section']//datatable-scroller//div[@class='datatable-row-center datatable-row-group']/datatable-body-cell[3]").getText();
                browser.pause(300);
                console.log(elem[j]);
                var listTime = elem[j];
                randomTime = browseDataObject.ConvToUNIXdates(listTime);
                console.log("Unix style timing are:" + new_fTime + "," + new_lTime + "," + randomTime);
                var res = browseDataObject.compareDates(new_fTime, new_lTime, randomTime);
                console.log(res);
                assert(res, true);
                j++;
                noOfRows--;
            }
        }
        console.log("Test End - To verify whether filtering is working for every column - For 'Start Time' tab");
    });     */
});