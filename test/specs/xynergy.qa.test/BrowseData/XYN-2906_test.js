let baseMethods = require('../../testbase');
var genericObject = require('../../../pageobjects/genericobjects.js');
let parameter = require('../../parameters.js');
let browseDataObject = require('../../../pageobjects/browseDataPage.js');
let config = require('../../../config.js');
var assert = require('assert');
require('../../commons/global.js')();
describe('Browse data test', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    it.skip('Verify delete file for dataset associated with workspace - XYN-2906', function(){
        console.log("Test started - Verify delete file for dataset associated with workspace - XYN-2906");
        var dataset = baseMethods.saveFileName(config.datasetName);
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
        console.log(dataset);
        importDataObject.enterDataSetName(dataset);
        browser.pause(200);
        browseDataObject.fileUpload(dataset);
        browser.pause(2000);
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.cLickFilesTab(config.filesTab);
        browseDataObject.enter_Filename('"' +dataset+ '"');
        browser.pause(2000);
        //baseMethods.checkDatasetFilter(dataset);

        //browser.waitForVisible(parameter.selectCheckBox);
        browseDataObject.filesCheckmark();
        var open = browser.element(parameter.filesOpenBtn);
        open.click();
        baseMethods.openInCanvasValidation();
        browser.pause(500);
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.MapDroppable);
        browser.element(parameter.mapLayersTab).click();
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        browser.pause(1000);
        var verifyBtn1 = browser.element(parameter.filesOpenBtn).isExisting();
        if(verifyBtn1 == true){
            //console.log(1);
            browseDataObject.filesCheckmark();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else if(verifyBtn1 == false){
            //console.log(2);
            browser.element(parameter.datasetsFilterClear).click();
        }
        //browser.element(parameter.datasetsFilterClear).click();
        browser.pause(1000);
        //browser.element(parameter.datasetsFilterClear).click();
        browseDataObject.enter_Filename(config.searchALFDataset);
        browser.pause(1000);
        browseDataObject.filesCheckmark();
        open.click();
        //baseMethods.clickOnSearchViewIcon(config.searchALFDataset);
        browser.element(parameter.dataScopeReplaceBtn).click();
        browser.element(parameter.dataScopeReplaceSaveBtn).click();
        var workspacename = baseMethods.saveFileName("a");
        searchdataObject.enterSaveWorkspaceNameValue(workspacename);
        browser.element(parameter.workspaceSaveBtn).click();
        browser.pause(2000);
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element("[data-test-id='"+workspacename+"']").click();
        browser.element(parameter.workspaceOpenBtn).click();
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        var checktab = browser.elements("[title='Map']").getText();
        console.log(checktab);
        //tabvisible = browser.elements(checktab[0]).isVisible();
        //var tabvisible = checktab.isVisible();
        //assert(tabvisible,true);
        expect(checktab).to.include("Map");
        browser.element(parameter.mapLayersTab).click();
        browser.pause(1000);
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        browser.pause(500);
        //browser.element(parameter.datasetsFilterClear).click();
        if(open.isExisting()){
            //console.log(1);
            browseDataObject.filesCheckmark();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            //console.log(2);
            browser.element(parameter.datasetsFilterClear).click();
        }
        //browser.element(parameter.datasetsFilterClear).click();
        baseMethods.cLickFilesTab(config.filesTab);
        browseDataObject.enter_Filename(dataset);
        browser.pause(2000);
        browseDataObject.filesCheckmark();
        browser.element(parameter.filesDeleteBtn).click();
        browseDataObject.confirmDeleteBtn();
        browser.pause(2000);
        var workspaceList = browser.elements(parameter.workspaceList).getText();
        console.log(workspaceList);
        var listItemLength = browser.elements(parameter.workspaceList).value.length;
        console.log(listItemLength);
        var result = false;
        var i = 0;      
        while (listItemLength > i) {
            if (workspaceList == workspacename){
                result = true;
            }
            else {
                console.log("One or more value(s) from results is not correct with search query");
                result = false;
            }
            i++;
        }
        assert(result,true);
        browseDataObject.confirmDeleteBtn();
        baseMethods.verifySpinnerVisibility();
        browser.pause(3000);
        browser.element(parameter.datasetsFilterClear).click();
        browseDataObject.enter_Filename('"' +dataset+ '"');
        browser.pause(1000);
        var errorMsg = browser.element(parameter.noData).isVisible();
        assert(errorMsg,true);
        browser.element(parameter.datasetsFilterClear).click();
        baseMethods.cLickFilesTab(config.datasetsTab);
        console.log("Test completed - Verify delete file for dataset associated with workspace - XYN-2906");
    });
});