let baseMethods = require('../../testbase');
var genericObject = require('../../../pageobjects/genericobjects.js');
let parameter = require('../../parameters.js');
let browseDataObject = require('../../../pageobjects/browseDataPage.js');
let config = require('../../../config.js');
var assert = require('assert');
require('../../commons/global.js')();
describe('Browse data test', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    it('Verify Save Workspace from Add/replace window under Browse Data - XYN-2904', function(){
        console.log("Test started - Verify save workspace from Add/Replace window under Browse Data - XYN-2904");
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(config.searchDatasetForRegularTest);
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        baseMethods.openInCanvasValidation();
        baseMethods.verifySpinnerVisibility();
        console.log("1");
        browser.pause(500);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MessagesDraggable,parameter.MessageDroppable);
        browser.element(parameter.DataMenu).click();
        browser.element(parameter.datasetsFilterClear).click();
        baseMethods.selectdatasetinBrowseDatasets(config.searchALFDataset);
        open.click();
        //baseMethods.clickOnSearchViewIcon(config.searchALFDataset);
        browser.element(parameter.dataScopeReplaceBtn).click();
        browser.element(parameter.dataScopeReplaceSaveBtn).click();
        var workspacename = baseMethods.saveFileName("a");
        searchdataObject.enterSaveWorkspaceNameValue(workspacename);
        browser.element(parameter.workspaceSaveBtn).click();
        browser.pause(3000);
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element("[data-test-id='"+workspacename+"']").click();
        browser.element(parameter.workspaceOpenBtn).click();
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        var checktab = browser.elements("[title='Messages']").getText();
        console.log(checktab);
        //tabvisible = browser.elements(checktab[0]).isVisible();
        //var tabvisible = checktab.isVisible();
        //assert(tabvisible,true);
        expect(checktab[0]).to.include("Messages");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        browser.element(parameter.datasetsFilterClear).click();
        console.log("Test completed - Verify save workspace from Add/Replace window under Browse Data - XYN-2904");
    });
});