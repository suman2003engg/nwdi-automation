let baseMethods = require('../../testbase');
var genericObject = require('../../../pageobjects/genericobjects.js');
let parameter = require('../../parameters.js');
let browseDataObject = require('../../../pageobjects/browseDataPage.js');
let config = require('../../../config.js');
var assert = require('assert');
require('../../commons/global.js')();
describe('Browse data test', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    it('Verify Add Data to already opened Data with no changes in scope - XYN-2982', function(){
        console.log("Test started - Verify Add Data to already opened Data with no changes in scope - XYN-2982");
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(config.searchDatasetForRegularTest);
        var datasetID = browser.element(parameter.replacedData);
        var dataset1 = datasetID.getText();
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        baseMethods.openInCanvasValidation();
        browser.pause(500);
        baseMethods.verifySpinnerVisibility();
        console.log("1");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.MapDroppable);
        browser.element(parameter.mapLayersTab).click();
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        browser.pause(1000);
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        //browser.element(parameter.datasetsFilterClear).click();
        baseMethods.selectdatasetinBrowseDatasets(config.searchALFDataset);
        var datasetID1 = browser.element(parameter.replacedData);
        var dataset2 = datasetID1.getText();
        open.click();
        browser.element(parameter.dataScopeAddBtn).click();
        baseMethods.verifySpinnerVisibility();
        browser.element(parameter.scopeWidget).click();
        scopeDataObject.scopeFilter(dataset1);
        var verify1 = browser.elements("[data-test-id='"+dataset1+"']");
        expect(verify1.getText()).to.include(dataset1);
        scopeDataObject.scopeFilter(dataset2);
        var verify2 = browser.elements("[data-test-id='"+dataset2+"']");
        expect(verify2.getText()).to.include(dataset2);
        browser.element(parameter.scopeFilterClear).click();
        browser.element(parameter.incorrectQueryClose).click();
        var checktab = browser.elements("[title='Map']").getText();
        console.log(checktab);
        expect(checktab).to.include("Map");
        browser.element(parameter.mapLayersTab).click();
        browser.pause(1000);
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        browser.pause(500);
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        //browser.element(parameter.datasetsFilterClear).click();
        baseMethods.cLickFilesTab(config.datasetsTab);
        console.log("Test completed - Verify Add Data to already opened Data with no changes in scope - XYN-2982");
    });
	
	it('Verify Add Data to already opened Data with changes in scope - XYN-2982', function(){
        console.log("Test started - Verify Add Data to already opened Data with changes in scope - XYN-2982");
        baseMethods.SelectTabs("BROWSEDATA");
        browseDataObject.LastFilters(config.LastFilterDaysValue);
        browseDataObject.LastdropdownDays();
        browseDataObject.clickApplyBtn();
        browser.pause(200);
        baseMethods.selectdatasetinBrowseDatasets(config.searchDatasetForRegularTestMO);
        var datasetID = browser.element(parameter.replacedData);
        var dataset1 = datasetID.getText();
        var open = browser.element(parameter.datasetsOpenBtn);
        open.click();
        baseMethods.openInCanvasValidation();
        browser.pause(500);
        baseMethods.verifySpinnerVisibility();
        console.log("1");
        browser.pause(500);
        var scopeTab = browser.element(parameter.scopeWidget);
        if(!(scopeTab.getAttribute("class")).includes('selected')){
            scopeTab.click();
        }
        scopeDataObject.scopeFilter(config.sprintMOFileName1);
        var fileUncheck = browser.element("[data-test-id='"+config.sprintMOFileName1+"']");
        fileUncheck.click();
        browser.element(parameter.scopeFilterClear).click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MapDraggable,parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.MapDroppable);
        browser.element(parameter.mapLayersTab).click();
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        browser.pause(1000);
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        //browser.element(parameter.datasetsFilterClear).click();
        baseMethods.selectdatasetinBrowseDatasets(config.searchALFDataset);
        var datasetID1 = browser.element(parameter.replacedData);
        var dataset2 = datasetID1.getText();
        open.click();
        browser.element(parameter.dataScopeAddBtn).click();
        baseMethods.verifySpinnerVisibility();
        browser.element(parameter.scopeWidget).click();
        scopeDataObject.scopeFilter(dataset1);
        var verify1 = browser.elements("[data-test-id='"+dataset1+"']");
        expect(verify1.getText()).to.include(dataset1);
        scopeDataObject.scopeFilter(dataset2);
        var verify2 = browser.elements("[data-test-id='"+dataset2+"']");
        expect(verify2.getText()).to.include(dataset2);
        browser.element(parameter.scopeFilterClear).click();
        scopeDataObject.scopeFilter(config.sprintMOFileName1);
        var fileUncheck = browser.element("[data-test-id='"+config.sprintMOFileName1+"']");
        var fileStatus = fileUncheck.isSelected();
        //console.log(fileStatus);
        assert.equal(fileStatus,false);
        browser.element(parameter.scopeFilterClear).click();
        browser.element(parameter.incorrectQueryClose).click();
        var checktab = browser.elements("[title='Map']").getText();
        console.log(checktab);
        expect(checktab).to.include("Map");
        browser.element(parameter.mapLayersTab).click();
        browser.pause(1000);
        var layerValue = baseMethods.verifyLayerData(config.metricName);
        assert(layerValue,true);
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        browser.pause(500);
        var checkBox = browser.element(parameter.openBtn);
        if(!(checkBox.getAttribute("class")).includes('button-disabled')){
            browser.element(parameter.selectCheckBox).click();
            browser.element(parameter.datasetsFilterClear).click();
        }
        
        else{
            browser.element(parameter.datasetsFilterClear).click();
        }
        //browser.element(parameter.datasetsFilterClear).click();
        baseMethods.cLickFilesTab(config.datasetsTab);
        console.log("Test completed - Verify Add Data to already opened Data with changes in scope - XYN-2982");
    });
});