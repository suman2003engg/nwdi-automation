let baseMethods = require('../../testbase');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
let browseDataObject = require('../../../pageobjects/browseDataPage.js');
let importDataObject = require("../../../pageobjects/importDataPage");
var searchdataObject = require('../../../pageobjects/searchdataPage.js');
var scopeDataObject = require('../../../pageobjects/scopeWidgetPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Browse data page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });


    it('Browse Data - To verify Filtering text with double quotes in Files- XYN-2766 , Verification of delete button in Browse data - XYN-2865', function () {
        console.log("Test started - Browse Data - To verify Filtering text with double quotes in Files- XYN-2766 , Verification of delete button in Browse data - XYN-2865");
        browseDataObject.importIconClick();
        browser.pause(500);
        browseDataObject.clickImportAddDataIcon();
        var dataset = baseMethods.saveFileName(config.datasetName);
        console.log(dataset);
        importDataObject.enterDataSetName(dataset);
        browser.pause(300);
        browseDataObject.fileUpload(dataset);
        baseMethods.SelectTabs(config.browseDataTab);
        baseMethods.cLickFilesTab(config.filesTab);
        browser.pause(1000);
        browseDataObject.enter_Filename('"' + dataset + '"');
        browser.pause(1000);
        var openDisabled = browser.element(parameter.openDataDisable).getAttribute(config.class);
        expect(openDisabled).to.include(config.disabled);
        var delDisabled = browser.element(parameter.deldataDisable).getAttribute(config.class);
        expect(delDisabled).to.include(config.disabled);
        browseDataObject.filesCheckmark();
        browser.pause(200);
        browser.element(parameter.filesOpenBtn).click();
        browser.pause(200);
        var modalBox = browser.element(parameter.modalBox).isVisible();
        if (modalBox == true) {
            browser.element(parameter.dataScopeAdd).click();
            browser.pause(200);
            var ScopePanel = browser.element(parameter.scope);
            var visibility = ScopePanel.isVisible();
            assert(visibility, true);
            browser.element(parameter.DataMenu).click();
            browser.pause(2000);
            browser.element(parameter.fileNameClear).click();
        }
        else {
            browser.pause(200);
            var ScopePanel = browser.element(parameter.scope);
            var visibility = ScopePanel.isVisible();
            assert(visibility, true);
            browser.element(parameter.DataMenu).click();
            browser.pause(2000);
            browser.element(parameter.fileNameClear).click();
        }
        console.log("Test completed - Browse Data - To verify filtering text with double quotes in Files- XYN-2766 , Verification of delete button in Browse data - XYN-2865");
    });
});
