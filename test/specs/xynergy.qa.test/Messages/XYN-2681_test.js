let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('Verify updating selected row to view using Find Next - XYN-2681', function () {
        console.log("Test started - Verify updating selected row to view using Find Next - XYN-2681");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);        
        messageObject.enterValueInFindFilter(config.findAllMessageFilterValue);
        messageObject.clickFindall();
        baseMethods.verifySpinnerVisibility();
        var j = 1;
        var count = browser.elements(parameter.MessagesFindGridRowContent).value.length;
        while (count >= j) {
            var findRowContent = browser.elements(parameter.MessagesFindGridRowContent+"[" + j + "]").getText();
            expect(findRowContent.toLowerCase()).to.include(config.findAllMessageFilterValue);            
            if (count == j) {
                //select the last row to continue with 'click Find Next button'
                browser.element(parameter.MessagesFindGridRowContent+"[" + j + "]").click();               
            }
            j++;
        }        
        //now click multiple times on Find Next button
        for (var i=1; i<=count; i++){
            browser.element(parameter.FindNextButton).click();
            browser.pause(300);
            findRowContent = browser.elements(parameter.MessagesFindGridRowContent+"[" + i + "]").getText();
            expect(findRowContent.toLowerCase()).to.include(config.findAllMessageFilterValue);
        }               
        console.log("Test completed - Verify updating selected row to view using Find Next - XYN-2681");
    });
});
