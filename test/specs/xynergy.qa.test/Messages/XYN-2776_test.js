let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('To verify NR RRC Messages in Messages view & "L3SM" messages colouring - XYN-2776', function () {
        console.log("Test started - To verify messages colouring - XYN-2776");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);        
        messageObject.enterMessageFilterValue(config.filterVal8);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        var j = 1;
        var count = browser.elements(parameter.messagesTableRows).value.length;
        while (count >= j) {
            var findRowContent = browser.elements(parameter.MessagesRowContent+"[" + j + "]").getText();
            expect(findRowContent.toLowerCase()).to.include(config.filterVal8.toLowerCase());
            var msgBGColor = browser.element(parameter.msgColumnInFilter).getAttribute(config.style);
            expect(msgBGColor).to.include(config.rrcConnectionReleaseBGColor);                                    
            j++;
        }
        console.log("Test completed - To verify messages colouring - XYN-2776");
    });
});
