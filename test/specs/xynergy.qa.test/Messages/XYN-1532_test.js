let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('To verify whether filter is working accordingly - XYN-1532', function () {
        console.log("Test started to verify filter functionality");
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        messageObject.enterMessageFilterValue(config.findMessageFilterValue);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        var j = 1;
        rowCount = browser.elements(parameter.MessagesRowContent).value.length;
        console.log(rowCount);
        while (rowCount > 0) {
            var rowContent = browser.elements(parameter.MessagesRowContent+"[" + j + "]");
            console.log(rowContent.getText());
            expect(rowContent.getText().toLowerCase()).to.include(config.findMessageFilterValue);
            j++;
            rowCount--;
        }
    });
});
