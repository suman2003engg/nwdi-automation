let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('XYN-2374_To verify the columns in the Find result', function () {       
        console.log("XYN-2374 Test execution started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        browser.pause(500);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable,parameter.MessageDroppable);
        browser.pause(300);
        messageObject.enterValueInFindFilter(config.findMessageFilterValue2);
        messageObject.clickFindall();
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        //var sMessageContent=browser.element("div[class='relative-component message-view'] .findall-result-container .datatable-header .datatable-header-cell:nth-child(4)").getText();
        var sMessageContent=browser.element(parameter.MessageContent).getText();
        assert.equal(sMessageContent,config.columnname1,sMessageContent+" actual column name is not same as expected "+config.columnname1);
        var sDevicename=parameter.Device;
        browser.execute(function(sDevicename){
           //document.querySelector("div[class='relative-component message-view'] .findall-result-container .datatable-header .datatable-header-cell:nth-child(5)").scrollIntoView();
           document.querySelector(sDevicename).scrollIntoView();
        },sDevicename);
        browser.pause(500);
        //var sDevice=browser.element("div[class='relative-component message-view'] .findall-result-container .datatable-header .datatable-header-cell:nth-child(5)").getText();
        var sDevice=browser.element(parameter.Device).getText();
        assert.equal(sDevice,config.columnname2,sDevice+" actual column name is not same as expected "+config.columnname2);
       console.log("XYN-2374 Test execution ended");
    });
});
