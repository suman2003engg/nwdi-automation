let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('Verification of message column color per protocol - XYN-3000', function () {
        console.log("Test started - Verification of message column color per protocol - XYN-3000");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        var selectRowElement = browser.element(parameter.SelectRowBtn);
        selectRowElement.click();
        browser.pause(300);
        var option=browser.element(parameter.Protocol);
        option.click();   
        var res = messageObject.messagesRowContentAttribute(config.protocol_PHY.toLowerCase(), config.protocol_PHY_RGBColorCode);
        assert(res, true);
        messageObject.clearFilterContent();
        var res = messageObject.messagesRowContentAttribute(config.protocol_RRC.toLowerCase(), config.protocol_RRC_RGBColorCode);
        assert(res, true);
        messageObject.clearFilterContent();
        var res = messageObject.messagesRowContentAttribute(config.protocol_EPC.toLowerCase(), config.protocol_EPC_RGBColorCode);
        assert(res, true);
        messageObject.clearFilterContent();
        var res = messageObject.messagesRowContentAttribute(config.protocol_MAC.toLowerCase(), config.protocol_MAC_RGBColorCode);
        assert(res, true);
        messageObject.clearFilterContent();
        console.log("Test completed - Verification of message column color per protocol - XYN-3000");
    });
});
