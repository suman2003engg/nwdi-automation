let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('Verify whether context menu FIND contains all sub features - XYN-1124', function () {
        console.log("Test started to check sub features of context menu FIND");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(1000);

        var findAllButtonPresence, findAllButtonVisible;
        findAllButtonPresence = browser.element(parameter.FindAllButton).isExisting();
        findAllButtonVisible = browser.element(parameter.FindAllButton).isVisible();

        var findNextButtonPresence, findNextButtonVisible;
        findNextButtonPresence = browser.element(parameter.FindNextButton).isExisting();
        findNextButtonVisible = browser.element(parameter.FindNextButton).isVisible();

        var closeButtonPresence, closeButtonVisible;
        closeButtonPresence = browser.element(parameter.CloseButton).isExisting();
        closeButtonVisible = browser.element(parameter.CloseButton).isVisible();

        var abortButtonPresence, abortButtonVisible;
        abortButtonPresence = browser.element(parameter.AbortButton).isExisting();
        abortButtonVisible = browser.element(parameter.AbortButton).isVisible();
        var priresult = false, totalresult = false;

        if (findAllButtonPresence && findAllButtonVisible && findNextButtonPresence && findNextButtonVisible && closeButtonPresence
            && closeButtonVisible && abortButtonPresence && abortButtonVisible) {
            priresult = true;
        }
        assert.equal(priresult, true);
    });
});
