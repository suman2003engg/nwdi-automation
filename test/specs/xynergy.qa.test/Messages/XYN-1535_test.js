let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('Verify whether filtering in findAll grid happens only on filtered results in message view panel - XYN-1535', function () {
        console.log("Test started for findAll button filters only from the filtered results of Message View panel");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        messageObject.enterMessageFilterValue(config.findMessageFilterValue3);
        browser.pause(100);
        var rowClass = browser.elements(parameter.MessagesSelectedRow);
        console.log(rowClass.getText());
        messageObject.enterValueInFindFilter(config.findMessageFilterValueInvalid);
        browser.pause(100);
        messageObject.clickFindall(parameter.FindAllButton);
        //browser.pause(9000);
        var nodatamessage=parameter.MessagesNoDataFoundForFilteredValue;
        // browser.waitUntil(() => {
        //     browser.elements(parameter.MessagesNoDataFoundForFilteredValue).value.length > 0
        //   }, 9000, 'no data found for the invalid filter text in FindAll:');
        browser.waitForExist(nodatamessage);
        if (browser.elements(parameter.MessagesNoDataFoundForFilteredValue).value.length > 0) {
            var noResult = browser.element(parameter.MessagesNoDataFoundForFilteredValue);
            console.log("no data found for the invalid filter text in FindAll: ", config.findMessageFilterValue2);
            console.log(noResult.getText());
            expect(noResult.getText().toLowerCase()).to.include(config.unableToFindData);
        }
        messageObject.removeFindFilterText();
        messageObject.enterValueInFindFilter(config.findMessageFilterValue);
        browser.pause(100);
        messageObject.clickFindall(parameter.FindAllButton);
        browser.pause(1000);
        var j = 2, count = browser.elements(parameter.MessagesFindGridRowContent).value.length;
        console.log("count: ", count);
        while (count > 2) {
            var findRowContent = browser.elements(parameter.MessagesFindGridRowContent+"[" + j + "]").getText();
            expect(findRowContent.toLowerCase()).to.include(config.findMessageFilterValue);
            count--;
            j++;
        }
        messageObject.removeFindFilterText();
        messageObject.enterValueInFindFilter(config.findMessageFilterValueInvalid);
        browser.pause(100);
        messageObject.clickFindall(parameter.FindAllButton);
        browser.pause(2000);
        if (browser.elements(parameter.MessagesNoDataFoundForFilteredValue).value.length > 0) {
            var noResult = browser.element(parameter.MessagesNoDataFoundForFilteredValue);
            console.log("no data found for the invalid filter text in FindAll: ", config.findMessageFilterValue2);
            console.log(noResult.getText());
            expect(noResult.getText().toLowerCase()).to.include(config.unableToFindData);
        }
        console.log("Test completed for findAll button filters only from the filtered results of Message View panel-XYN-1535");
    });
});
