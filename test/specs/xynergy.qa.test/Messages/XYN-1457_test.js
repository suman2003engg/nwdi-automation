let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('Verify optimization message search - XYN-1457', function () {
        console.log("Test started for verification of optimization message search");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        var findElement = browser.element(parameter.CloseButton).isVisible();
        if (findElement == true) {
            browser.element(parameter.CloseButton).click();
        }
        browser.element(parameter.HamburgerMenu).click();
        browser.element("[data-test-id='find-btn']").click();
        messageObject.enterValueInFindFilter(config.findMessageFilterValue);
        messageObject.clickFindall(parameter.FindAllButton);
        browser.pause(300);
        var eventsdataTab = browser.elements("//*[@name='messages-search-grid']//datatable-scroller[@class='datatable-scroll']/datatable-row-wrapper");
        var objectCount = eventsdataTab.value;
        var elementCount = objectCount.length;
        var count = false;
        if (elementCount > 2) {
            count = true;
        }
        assert.equal(count, true);
        console.log("Test completed for verification of optimization message search");
    });
});
