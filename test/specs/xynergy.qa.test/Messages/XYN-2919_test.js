let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('Verification of resizing of "Message View" on enabling the decoded message - XYN-2919', function () {
        console.log("Test started - Verification of resizing of Message View on enabling the decoded message - XYN-2919");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();
        var splitonMessagesDecodeView = browser.element(parameter.splitOnDecodeMessages).isVisible();
        assert.equal(splitonMessagesDecodeView, false);
        messageObject.clickDecodeMessageIcon();
        splitonMessagesDecodeView = browser.element(parameter.splitOnDecodeMessages).isVisible();
        assert.equal(splitonMessagesDecodeView, true);        
        var openDecodeView = browser.element(parameter.decodeMessages).isVisible();
        assert.equal(openDecodeView, true);
        messageObject.clickDecodeMessageIcon();
        openDecodeView = browser.element(parameter.decodeMessages).isVisible();
        assert.equal(openDecodeView, false);
        console.log("Test completed - Verification of resizing of Message View on enabling the decoded message - XYN-2919");
    });
});
