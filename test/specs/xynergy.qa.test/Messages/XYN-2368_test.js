let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('Verify Abort button beside Find Search bar - XYN-2368', function () {
        console.log("Test started for verification of Abort button beside Find Search bar-XYN-2368");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(500);
        messageObject.enterValueInFindFilter(config.findMessageFilterValue);
        browser.pause(500);
        messageObject.clickFindall(parameter.FindAllButton);
        messageObject.clickFindnext(parameter.FindNextButton);
        messageObject.clickAbort();
        var buttonname = browser.element(parameter.AbortButton).getText();
        assert.equal(config.findMessageAbortButtonText, buttonname);
        console.log("Test completed for verification of Abort button beside Find Search bar-XYN-2368");
    });
   });
