let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Messages', function () {
    var init=0;
    var selectedTimeColValue=parameter.MessagesSelectedRow+"//div[2]//datatable-body-cell[2]";
    var selectedTimeColValueInFindGrid=parameter.MessagesFindGridSelectedRowContent+"//div[2]//datatable-body-cell[2]";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
        }       
    });
    afterEach(function () {
        if(isElectron()){
        console.log("closing all open views");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        console.log("closing all open views");
            var closeAllViews=browser.element("[data-test-id='close-all-btn']");
            if(closeAllViews.isVisible()){
            closeAllViews.click();
            browser.pause(300);
            console.log("clicking on close btn");
            //var closeAllViewsTitleBar=browser.element("[data-test-id='Close All Views-Title-Bar']");
            var modalBox=browser.element("//modal-box");
            var mBoxCloseBtn=modalBox.element("[data-test-id='close-all-views']");
            if(mBoxCloseBtn.isVisible()){
            mBoxCloseBtn.click();
            console.log("clicked on close btn");
            }
            browser.pause(300);
            //close views menu
            var viewsHeader=browser.element("//views-menu");
            var closeViewsMenu=viewsHeader.element("[data-test-id='close-button']");
            if(closeViewsMenu.isVisible()){
                closeViewsMenu.click();
            }
            }
        }
        
    });    
    it('Verify Find options visibility on click of Find buton - XYN-1125', function () {
        console.log("Verify Find options visibility on click of Find buton - XYN-1125");
        var assertValue = false;
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(1000);
        var findElement = browser.element(parameter.CloseButton).isVisible();
        if (findElement == true) {
            browser.element(parameter.CloseButton).click();
        }
        browser.element(parameter.HamburgerMenu).click();
        browser.element("[data-test-id='find-btn']").click();
        var findNextBtn = browser.element(parameter.FindNextButton).isVisible();
        var findAllBtn = browser.element(parameter.FindAllButton).isVisible();
        var closeBtn = browser.element(parameter.CloseButton).isVisible();
        var abortBtn = browser.element(parameter.AbortButton).isVisible();
        if (findNextBtn && findAllBtn && closeBtn && abortBtn == true) {
            assertValue = true;
        }
        assert.equal(assertValue, true);
    });

    it('Verify cancel input button in find option - XYN-1125', function () {
        console.log("Verify cancel input button in find option - XYN-1125");
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(1000);
        messageObject.enterValueInFindFilter("Automation");
        var cancelBtn = browser.element("[data-test-id='cancel-input']").isVisible();
        assert.equal(cancelBtn, true);
    });

    // it('Verify no-data found error message after searching invalid value - XYN-1125', function () {
    //     console.log("Verify no-data found error message after searching invalid value");
    //     baseMethods.clickOnAdvancedSearchViewIcon()
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.WidgetRoot);
    //     browser.pause(1000);
    //     messageObject.enterValueInFindFilter("Automation");
    //     var errorMsg = browser.element("[data-test-id='no-data-msg']").isVisible();
    //     assert.equal(errorMsg, true);
    // });

    it('Verify data after searching valid value - XYN-1125', function () {
        console.log("Verify data after searching valid value ");
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(1000);
        messageObject.enterValueInFindFilter("MAC");
        messageObject.clickFindall();
        browser.pause(1500);
        var eventsdataTab = browser.elements("//div[@data-test-id='data-grid-container']/ngx-datatable[1]/div[1]/datatable-body[1]/datatable-selection[1]/datatable-scroller[1]/datatable-row-wrapper");
        var objectCount = eventsdataTab.value;
        var elementCount = objectCount.length;
        var count = false;
        if (elementCount > 2) {
            count = true;
        }
        assert.equal(count, true);
    });

    it('Verify find otion visibility after clicking close button - XYN-1125', function () {
        console.log("Verify find otion visibility after clicking close button - XYN-1125 ");
        var assertValue = false;
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(1000);
        var findElement = browser.element(parameter.CloseButton).isVisible();
        if (findElement == true) {
            browser.element(parameter.CloseButton).click();
        }
        var findNextBtn = browser.element(parameter.FindNextButton).isVisible();
        var findAllBtn = browser.element(parameter.FindAllButton).isVisible();
        var closeBtn = browser.element(parameter.CloseButton).isVisible();
        var abortBtn = browser.element(parameter.AbortButton).isVisible();
        if (findNextBtn && findAllBtn && closeBtn && abortBtn == true) {
            assertValue = true;
        }
        assert.equal(assertValue, false);
    });    
});
