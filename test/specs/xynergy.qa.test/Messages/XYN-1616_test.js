let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('XYN-1616_Verify if Messages widget is showing messages', function () {
        console.log("XYN-1616 Test execution started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        browser.pause(500);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable,parameter.MessageDroppable);
        browser.pause(10000);
        //var oMessages = browser.elements("//div[@class='message-view'][1]//datatable-scroller[@class='datatable-scroll']/datatable-row-wrapper");
        //var oMessages = browser.elements("//div[@class='relative-component message-view'][1]//datatable-scroller[@class='datatable-scroll']/datatable-row-wrapper");
        browser.waitForVisible(parameter.MessageWindow,10000)
        var oMessages = browser.elements(parameter.MessageWindow);
        //var oMessages = messageObject.messageWindowRowsCount();
        var objectCount = oMessages.value;
        var iMessageCount = objectCount.length;
        assert.equal(iMessageCount>3,true,iMessageCount+" row count is less then 3")
        console.log(iMessageCount);
        browser.pause(500);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable,parameter.WidgetRightBox);
        browser.pause(500);
        browser.waitForVisible(parameter.MessageWindow2,10000)
        //var oMessages = browser.elements("//div[@class='vertical-splitter-container'][1]//datatable-scroller[@class='datatable-scroll']/datatable-row-wrapper");
        var oMessages = browser.elements(parameter.MessageWindow1);
        //var oMessages = messageObject.messageLeftWindowRowsCount();
        var objectCount = oMessages.value;
        var iMessageCount = objectCount.length;
        assert.equal(iMessageCount>3,true,iMessageCount+" row count is less then 3")
        console.log(iMessageCount);
        //var oMessages = browser.elements("//div[@class='vertical-splitter-container'][2]//datatable-scroller[@class='datatable-scroll']/datatable-row-wrapper");
        var oMessages = browser.elements(parameter.MessageWindow2);
        //var oMessages = messageObject.messageRightWindowRowsCount();
        var objectCount = oMessages.value;
        var iMessageCount = objectCount.length;
        assert.equal(iMessageCount>3,true,iMessageCount+" row count is less then 3")
        console.log(iMessageCount);
      
       console.log("XYN-1616 Test execution ended");
    });
});
