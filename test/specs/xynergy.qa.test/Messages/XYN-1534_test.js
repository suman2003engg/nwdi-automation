let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('XYN-1534_Verify whether pop-up screen is displaying when Find All or Find Next does not return any result', function () {
        console.log("XYN-1534 Test execution started");
        baseMethods.clickOnAdvancedSearchViewIcon();
        browser.pause(500);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable,parameter.MessageDroppable);
        browser.pause(500);
        messageObject.enterValueInFindFilter(config.invalidValue);
        messageObject.clickFindall();
        baseMethods.verifySpinnerVisibility();
        browser.pause(10000);
        browser.waitForVisible(parameter.NoDataErrorText, 10000);
        var sText=browser.element(parameter.NoDataErrorText).getText();
        assert.equal(sText,config.unableToFindData,sText+" actual text is not same expected text "+config.unableToFindData)
        messageObject.removeFindFilterText();
        messageObject.enterValueInFindFilter(config.invalidValue);
        messageObject.clickFindnext();
        var bStatus=baseMethods.verifyFindNextPopupMessage(config.unableToFindData);
        assert.equal(bStatus,true,"verifyFindNextPopupMessage function returns false");
       console.log("XYN-1534 Test execution ended");
    });
});
