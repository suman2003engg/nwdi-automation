let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('Verify whether error message is displaying when there is no sync happening - XYN-2037', function () {
        console.log("Test started for verification of error message in Messages View- XYN-2037");
        this.skip();
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        messageObject.enterMessageFilterValue(config.findMessageFilterValue2);
        browser.pause(300);
        var eventsdataTab = browser.elements(parameter.MessagesRowContent);
        var objectCount = eventsdataTab.value;
        var elementCount = objectCount.length;
        var count = false;
        if (elementCount > 2) {
            count = true;
        }
        assert.equal(count, true);
        var ErrorMsg = browser.element("[class='syncMessage']").getText();
        console.log(ErrorMsg);
        assert.equal("", ErrorMsg);
        console.log("Test completed for verification of error message in Messages View");
    });
});
