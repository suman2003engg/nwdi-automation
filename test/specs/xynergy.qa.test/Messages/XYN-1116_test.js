let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Messages', function () {
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        } 
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);      
    });
    it('Verify whether user can navigate through message data using Find Next after finding all filtered results and the result sync with the message in message view panel - XYN-1116', function () {
        console.log("Test started for find next button working after finding all filtered results and also the message view panel syncing with the selected record from the grid- XYN-1116");
        //this.skip();
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.MessageDroppable);
        browser.pause(100);
        messageObject.enterValueInFindFilter(config.findMessageFilterValue);
        baseMethods.verifySpinnerVisibility();
        messageObject.clickFindall(parameter.FindAllButton);
        var j = 1;
        var findAllRow = browser.elements(parameter.MessagesFindGridRowContent);
        console.log(findAllRow.value.length);
        console.log("timeing");
        messageObject.clickFindnext(parameter.FindNextButton);
        browser.pause(300);
        var actualTime = browser.elements(selectedTimeColValueInFindGrid).getText();
        console.log(actualTime);
        while (j <= 4) {
            messageObject.clickFindnext(parameter.FindNextButton);
            console.log("next row contents");
            var findNextRow = browser.elements(parameter.MessagesFindGridSelectedRowContent);
            console.log(findNextRow.getText());
            var timeDiff = browser.elements(selectedTimeColValueInFindGrid).getText();
            if (j > 2) {
                if(timeDiff!=actualTime){
                expect(findNextRow.getText().toLowerCase()).to.include(config.findMessageFilterValue);
                assert.notEqual(timeDiff, actualTime);
                }
            }
            console.log("actual time before assigning", actualTime);
            actualTime = timeDiff;
            findAllRow = findNextRow.getText();
            console.log("actual time after assigning", timeDiff);
            j++;
        }
        // var rowClass = browser.elements("//datatable-row-wrapper/datatable-body-row[contains(@class,'datatable-body-row active')]");
        // console.log(rowClass.getText());
        var timeOnView = browser.elements(selectedTimeColValue);
        console.log("time on view", timeOnView.getText());
        // var rowclassText = rowClass.getText();
        expect(findNextRow.getText().toLowerCase()).to.include(config.findMessageFilterValue);
        assert.equal(actualTime, timeOnView.getText());
        console.log("Test completed for find next button XYN-1116");
    });
 });
