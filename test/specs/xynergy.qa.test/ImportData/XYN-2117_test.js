let baseMethods = require('../../testbase');
//require('../../configurations');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let importDataObject= require("../../../pageobjects/importDataPage");
let searchdataObject= require("../../../pageobjects/searchDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
var uploadDataSetName=config.addDatasetName;

describe.skip('Import data test', function () {
    
    before(function(){
        uploadDataSetName=baseMethods.saveFileName(uploadDataSetName);
    });
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    it('Verify the fields of Add data page, XYN-2117', function () {
        console.log("Test started- Verify the fields of Add data page XYN-2117");
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
        //importDataObject.enterDataSetName(config.searchDatasetForRegularTest);
        browser.pause(200);
        var pageElemnts=browser.elements("[title='Add Data']");
        pageElemnts.getText();
        console.log(pageElemnts.getText());
        //expect(appendMessage).to.include(config.appendMessageValue);   
        console.log("Test Completed- Verify the fields of Add data page XYN-2117");    
    });
});