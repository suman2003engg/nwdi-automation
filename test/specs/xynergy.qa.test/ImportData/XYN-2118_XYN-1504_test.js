let baseMethods = require('../../testbase');
//require('../../configurations');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let importDataObject= require("../../../pageobjects/importDataPage");
let searchdataObject= require("../../../pageobjects/searchDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
var uploadDataSetName=config.addDatasetName;

describe('Import data test', function () {
    
    before(function(){
        uploadDataSetName=baseMethods.saveFileName(uploadDataSetName);
    });
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            browser.waitForExist("[data-test-id='Analytics View']");
        }
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
    });
    it('Verify upload file types- XYN-2118, XYN-1504', function(){
        console.log("Test Started - Verify upload file types- XYN-2118, XYN-1504");
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
        importDataObject.enterDataSetName(config.datasetForDifferentFilteTypes);
        browser.pause(600);   
        var absolutePathNMFType = path.resolve(__dirname, config.fileNameWithnmfType);      
        importDataObject.browseFile(absolutePathNMFType);
        browser.pause(500);
        importDataObject.clickImportBtn();
        browser.pause(400);
        importDataObject.waitForProgressPage();
        var dataProgress=browser.elements("//import-data-progress-overview//div[" +i+ "]");
        var eachElement=dataProgress.elements("//div[@class='import-progress-overview']");
        console.log(eachElement.getText());
        assert.equal(eachElement.getText().includes("Files"),true);
        assert.equal(eachElement.getText().includes("Processing Status"),true);
        assert.equal(eachElement.getText().includes("%"),true);
        importDataObject.getProgressDataSet(config.datasetForDifferentFilteTypes);
        browser.pause(9000);
        browser.waitForExist(parameter.historyOpenBtn);
        browser.pause(400);
        importDataObject.enterValueInHistoryFilter(config.datasetForDifferentFilteTypes);
        browser.pause(400);
        var getUploadedDataset=importDataObject.getDataSetCount(parameter.historyDataGrid,parameter.gridRows,config.datasetForDifferentFilteTypes);
        console.log(getUploadedDataset);
        browser.pause(300);           
        expect(getUploadedDataset).to.be.equal(parseInt(config.compareValueWithOne));
        console.log("Test completed - Verify upload file types- XYN-2118");
    });
});