let baseMethods = require('../../testbase');
//require('../../configurations');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let importDataObject= require("../../../pageobjects/importDataPage");
let searchdataObject= require("../../../pageobjects/searchDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
var uploadDataSetName=config.addDatasetName;

describe('Import data test', function () {
    
    before(function(){
        uploadDataSetName=baseMethods.saveFileName(uploadDataSetName);
    });
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    it('Verify if user is able to delete the already selected files, XYN-1495', function () {
        console.log("Test started- Verify if user is able to delete the already selected files XYN-1495");
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
        var absolutePath = path.resolve(__dirname, config.fileNameWithPath1);      
        importDataObject.browseFile(absolutePath);
        browser.pause(1000);
        var checkBox=browser.element("//*[@name='import-files']"+parameter.checkMarkLeft);
        console.log(checkBox.isVisible());
        checkBox.click();
        browser.pause(500);
        importDataObject.clickDeleteSelectedFile();
        browser.pause(300);           
        var emptyRow=browser.elements("//app-data-grid[@name='import-files']//div[@class='empty-row']");
        count=emptyRow.isExisting();
        assert.equal(count,true);
        console.log("Test completed- Verify if user is able to delete the already selected files XYN-1495");
    });
});