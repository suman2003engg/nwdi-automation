let baseMethods = require('../../testbase');
//require('../../configurations');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let importDataObject= require("../../../pageobjects/importDataPage");
let searchdataObject= require("../../../pageobjects/searchDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
var uploadDataSetName=config.addDatasetName;

describe('Import data test', function () {
    
    before(function(){
        uploadDataSetName=baseMethods.saveFileName(uploadDataSetName);
    });
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    it('Verify if user is able to navigate to Import data page, XYN-2116', function () {
        console.log("Test started- Verify if user is able to navigate to Import data page XYN-2116");
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
        //importDataObject.enterDataSetName(config.searchDatasetForRegularTest);
        browser.pause(200);
        assert.equal(browser.element("[data-test-id='Add Data']").isExisting(),true);
        assert.equal(browser.element("[data-test-id='Progress Status']").isExisting(),true);
        assert.equal(browser.element("[data-test-id='History']").isExisting(),true);
        //expect(appendMessage).to.include(config.appendMessageValue);   
        console.log("Test Completed- Verify if user is able to navigate to Import data page XYN-2116");    
    });
});