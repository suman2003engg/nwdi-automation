let baseMethods = require('../../testbase');
//require('../../configurations');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let importDataObject= require("../../../pageobjects/importDataPage");
let searchdataObject= require("../../../pageobjects/searchDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
var uploadDataSetName="XYN-2095";

describe('Import data test', function () {
    
    before(function(){
        uploadDataSetName=baseMethods.saveFileName(uploadDataSetName);
    });
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    it('Verify whether file selection gets retained after import is initiated, XYN-2095', function () {
        console.log("Test started- Verify whether file selection gets retained after import is initiated XYN-2095");
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
        browser.pause(100);
        importDataObject.enterDataSetName(uploadDataSetName);
        browser.pause(600);   
        var absolutePathISFType = path.resolve(__dirname, config.fileNameWithPath1);      
        importDataObject.browseFile(absolutePathISFType);    
        browser.pause(500);
        importDataObject.clickImportBtn();
        browser.pause(400);
        importDataObject.waitForProgressPage();
        importDataObject.clickImportAddDataMenu();
        browser.pause(100);
        assert.equal(importDataObject.verifySelectedFile(config.fileName),false);
        console.log("Test Completed- Verify whether file selection gets retained after import is initiated XYN-2095");    
    });
});