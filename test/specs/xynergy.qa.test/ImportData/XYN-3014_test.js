let baseMethods = require('../../testbase');
//require('../../configurations');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let importDataObject= require("../../../pageobjects/importDataPage");
let searchdataObject= require("../../../pageobjects/searchDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
var uploadDataSetName=config.addDatasetName;

describe('Import data test', function () {
    
    before(function(){
        uploadDataSetName=baseMethods.saveFileName(uploadDataSetName);
    });
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            browser.waitForExist("[data-test-id='Analytics View']");
        }
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
    });
    it('Verify the functionality of Open a dataset- XYN-3014', function(){
        console.log("Test Started - Verify the functionality of Open a dataset- XYN-3014");
        importDataObject.clickHistoryMenu();
        browser.pause(200);
        importDataObject.enterValueInHistoryFilter(config.searchDatasetForRegularTest);
        baseMethods.verifySpinnerVisibility();
        browser.pause(600);
        browser.element(parameter.gridRows).click();
        browser.waitForExist(parameter.historyOverviewPage);
        importDataObject.clickOnOpenBtnFromHistoryOverviewPage();
        browser.pause(200);
        if(genericObject.isOpenDataModalBoxDisplayed()){
            genericObject.replaceDataSetWithoutSaving();
        }
        browser.pause(500);
        assert.equal(genericObject.isDataSetInScopePresent(config.searchDatasetForRegularTest),true);
        console.log("Test completed - Verify the functionality of Open a dataset- XYN-3014");
    });

});