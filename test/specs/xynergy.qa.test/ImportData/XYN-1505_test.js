let baseMethods = require('../../testbase');
//require('../../configurations');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let importDataObject= require("../../../pageobjects/importDataPage");
let searchdataObject= require("../../../pageobjects/searchDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
var uploadDataSetName=config.addDatasetName;

describe('Import data test', function () {
    
    before(function(){
        uploadDataSetName=baseMethods.saveFileName(uploadDataSetName);
    });
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    it('Verify History featue, XYN-1505', function () {
        console.log("Test started- Verify History featue XYN-1505");
        importDataObject.clickImportDataMenu();
        importDataObject.clickHistoryMenu();
        var columns=browser.elements("//*[@name='history-datasets']//datatable-header");
        console.log(columns.getText());
        for(var i=0;i<config.historyPageColumnHeaders.length;i++){
            assert.equal(columns.getText().includes(config.historyPageColumnHeaders[i]),true);
        }  
        console.log("Test Completed- Verify History featue XYN-1505");    
    });
});