let baseMethods = require('../../testbase');
//require('../../configurations');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let importDataObject= require("../../../pageobjects/importDataPage");
let searchdataObject= require("../../../pageobjects/searchDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
var uploadDataSetName=config.addDatasetName;

describe('Import data test', function () {
    
    before(function(){
        uploadDataSetName=baseMethods.saveFileName(uploadDataSetName);
    });
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            browser.waitForExist("[data-test-id='Analytics View']");
        }
        importDataObject.clickImportDataMenu();
        //importDataObject.clickImportAddDataMenu();
    });
    it('Verify if user is able to search records by status from the data table from history- XYN-1508', function(){
        console.log("Test Started -  Verify if user is able to search records by status from the data table from history ,XYN-1508");
        //browser.element(parameter.historyOpenBtn);
        importDataObject.clickHistoryMenu();
        browser.pause(400);
        var status="Completed";
        importDataObject.enterValueInHistoryFilter(status);
        browser.pause(400);
        var elem=browser.elements(parameter.historyDataGrid);
        var rows=elem.elements(parameter.gridRows);
        var i=1;
        console.log("count");
        if(rows.value.length>=0){
            while(i<rows.value.length){
                console.log(rows);
                console.log(browser.element("//app-data-grid[@name='history-datasets']//datatable-row-wrapper["+i+"]").getText());
                var searchValue=browser.element("//app-data-grid[@name='history-datasets']//datatable-row-wrapper["+i+"]").getText();
                console.log(status+" regrevrev");
                assert.equal(searchValue.includes(status),true);     
                i++; 
                if(i<rows.value.length-3){
                    break;
                }          
            }
        }
        console.log("Test completed - Verify if user is able to search records by status from the data table from history XYN-1508");
    });
});