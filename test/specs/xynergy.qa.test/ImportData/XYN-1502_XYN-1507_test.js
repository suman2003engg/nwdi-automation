let baseMethods = require('../../testbase');
//require('../../configurations');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let importDataObject= require("../../../pageobjects/importDataPage");
let searchdataObject= require("../../../pageobjects/searchDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
var uploadDataSetName=config.addDatasetName;

describe('Import data test', function () {
    
    before(function(){
        uploadDataSetName=baseMethods.saveFileName(uploadDataSetName);
    });
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            browser.waitForExist("[data-test-id='Analytics View']");
        }
        importDataObject.clickImportDataMenu();
        //importDataObject.clickImportAddDataMenu();
    });
    it('Verify if user is able to search records from the data table from history- XYN-1502, XYN-1507', function(){
        console.log("Test Started - Verify if user is able to search records from the data table from history- XYN-1502, XYN-1507");
        //browser.element(parameter.historyOpenBtn);
        importDataObject.clickHistoryMenu();
        browser.pause(400);
        importDataObject.enterValueInHistoryFilter(config.searchALFDataset);
        browser.pause(400);
        var getUploadedDataset=importDataObject.getDataSetCount(parameter.historyDataGrid,parameter.gridRows,dataSetName);
        console.log(getUploadedDataset);
        browser.pause(300);           
        expect(getUploadedDataset).to.be.equal(parseInt(config.compareValueWithOne));
        console.log("Test completed - Verify if user is able to search records from the data table from history- XYN-1502,XYN-1507");
    });
});