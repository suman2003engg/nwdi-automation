let baseMethods = require('../../testbase');
//require('../../configurations');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let importDataObject= require("../../../pageobjects/importDataPage");
let searchdataObject= require("../../../pageobjects/searchDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
var uploadDataSetName=config.addDatasetName;

describe('Import data test', function () {
    
    before(function(){
        uploadDataSetName=baseMethods.saveFileName(uploadDataSetName);
    });
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    it('Verification of import data append message, XYN-1493', function () {
        console.log("Test started- import data append message XYN-1493");
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
        importDataObject.enterDataSetName(config.searchDatasetForRegularTest);
        browser.pause(600);
        var appendMessage=baseMethods.getAppendMessage();
        console.log(appendMessage);
        expect(appendMessage).to.include(config.appendMessageValue);   
        console.log("Test Completed- import data append message XYN-1493");    
    });
});