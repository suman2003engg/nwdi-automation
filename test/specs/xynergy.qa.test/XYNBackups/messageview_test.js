let baseMethods = require('../../testbase.js');
let genericObject = require('../../../pageobjects/genericobjects');
var messageObject = require('../../../pageobjects/messagePage');
var config = require('../../../config');
var searchdataObject=require('../../../pageobjects/searchDataPage');
let parameter = require('../../parameters.js');
var xynergyconstants = require('../../test_xynergy.constants.js');
var assert = require('assert');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Messages', function () {
    var init=0;
    var selectedTimeColValue=parameter.MessagesSelectedRow+"//div[2]//datatable-body-cell[2]";
    var selectedTimeColValueInFindGrid=parameter.MessagesFindGridSelectedRowContent+"//div[2]//datatable-body-cell[2]";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
        }       
    });
    afterEach(function () {
        if(isElectron()){
        console.log("closing all open views");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        console.log("closing all open views");
            var closeAllViews=browser.element("[data-test-id='close-all-btn']");
            if(closeAllViews.isVisible()){
            closeAllViews.click();
            browser.pause(300);
            console.log("clicking on close btn");
            //var closeAllViewsTitleBar=browser.element("[data-test-id='Close All Views-Title-Bar']");
            var modalBox=browser.element("//modal-box");
            var mBoxCloseBtn=modalBox.element("[data-test-id='close-all-views']");
            if(mBoxCloseBtn.isVisible()){
            mBoxCloseBtn.click();
            console.log("clicked on close btn");
            }
            browser.pause(300);
            //close views menu
            var viewsHeader=browser.element("//views-menu");
            var closeViewsMenu=viewsHeader.element("[data-test-id='close-button']");
            if(closeViewsMenu.isVisible()){
                closeViewsMenu.click();
            }
            }
        }
        
    });

    it('Verify whether context menu FIND contains all sub features - XYN-1124', function () {
        console.log("Test started to check sub features of context menu FIND");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(1000);

        var findAllButtonPresence, findAllButtonVisible;
        findAllButtonPresence = browser.element(parameter.FindAllButton).isExisting();
        findAllButtonVisible = browser.element(parameter.FindAllButton).isVisible();

        var findNextButtonPresence, findNextButtonVisible;
        findNextButtonPresence = browser.element(parameter.FindNextButton).isExisting();
        findNextButtonVisible = browser.element(parameter.FindNextButton).isVisible();

        var closeButtonPresence, closeButtonVisible;
        closeButtonPresence = browser.element(parameter.CloseButton).isExisting();
        closeButtonVisible = browser.element(parameter.CloseButton).isVisible();

        var abortButtonPresence, abortButtonVisible;
        abortButtonPresence = browser.element(parameter.AbortButton).isExisting();
        abortButtonVisible = browser.element(parameter.AbortButton).isVisible();
        var priresult = false, totalresult = false;

        if (findAllButtonPresence && findAllButtonVisible && findNextButtonPresence && findNextButtonVisible && closeButtonPresence
            && closeButtonVisible && abortButtonPresence && abortButtonVisible) {
            priresult = true;
        }
        assert.equal(priresult, true);
    });

    it('To verify whether FindNext button is working depending on filtered messages - XYN-1531', function () {
        console.log("Test started to verify findNext functionality based on filtered messages");
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        messageObject.enterMessageFilterValue(config.findMessageFilterValue3);
        browser.pause(3000);
        //baseMethods.verifySpinnerVisibility();
        messageObject.enterValueInFindFilter(config.findMessageFilterValue);
        var rowClass = browser.elements(parameter.MessagesSelectedRow);
        console.log(rowClass.getText());
        // //datatable-body-row[@class='datatable-body-row active datatable-row-odd']//div[@class='datatable-row-center datatable-row-group']//datatable-body-cell[@class='datatable-body-cell sort-active']//div[@class='datatable-body-cell-label']//div//span[contains(text(),'09/25/2013 21:32:00.451')]
        var actualTime = browser.elements(selectedTimeColValue);
        var i = 0;
        while (i < 2) {
            messageObject.clickFindnext(parameter.FindNextButton);
            var rowNext = browser.elements(parameter.MessagesSelectedRow);
            var timeDiff = browser.elements(selectedTimeColValue);
            console.log(timeDiff.getText());
            expect(rowNext.getText().toLowerCase()).to.include(config.findMessageFilterValue);
            assert.notEqual(timeDiff, actualTime);
            console.log(rowNext.getText());
            i++;
        }
    });

    it('To verify whether filter is working accordingly - XYN-1532', function () {
        console.log("Test started to verify filter functionality");
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        messageObject.enterMessageFilterValue(config.findMessageFilterValue);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        var j = 1;
        rowCount = browser.elements(parameter.MessagesRowContent).value.length;
        console.log(rowCount);
        while (rowCount > 0) {
            var rowContent = browser.elements(parameter.MessagesRowContent+"[" + j + "]");
            console.log(rowContent.getText());
            expect(rowContent.getText().toLowerCase()).to.include(config.findMessageFilterValue);
            j++;
            rowCount--;
        }
    });

    it('To verify whether full-screen icon is working - XYN-1621', function () {
        console.log("Test started to verify full-screen icon");
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        // var divElements = browser.elements("//widget-container//splitter-component//div[@class='vertical-splitter-bar']");
        // console.log("number of elements: ", divElements.value.length);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.WidgetRightBox);
        var divElements = browser.elements("//widget-container//splitter-component//div[@class='vertical-splitter-bar']");
        console.log("number of elements: ", divElements.value.length);
        var divElements1 = browser.elements("//widget-container//splitter-component[1]//div[3][@class='vertical-splitter-container']").getAttribute("style");
        //expect(divElements1.getAttribute('style')).to.include('block');
        console.log(divElements1);
        divElements1.forEach(element=>{
            console.log(element);
            if(element.includes('block')){
                console.log("there are multiple views");
            }
        })
        var countBefore=browser.element('div[data-test-id="widget-max-btn"]').value.length;
        console.log(countBefore);
        messageObject.clickFullScreenIcon();
        console.log("screen");
        browser.pause(1000);
        var countAfter=browser.element('div[data-test-id="widget-max-btn"]').value.length;
        console.log(countAfter);
        var divElements2 = browser.elements("//widget-container//splitter-component//div[@class='vertical-splitter-bar']");
        console.log("number of div elements: ", divElements2.value.length);
        var hidden = browser.elements("//widget-container//splitter-component[1]//div[3][contains(@style,'none')]").getAttribute('style');
        console.log("style", divElements2.getAttribute('style'));
        console.log(hidden);
        hidden.forEach(element=>{
            console.log(element);
            expect(element).to.include('none');
        })
        //expect(hidden.getAttribute('style')).to.include('none');
    });

    //-----------XYN-1125------------//

    it('Verify Find options visibility on click of Find buton - XYN-1125', function () {
        console.log("Verify Find options visibility on click of Find buton - XYN-1125");
        var assertValue = false;
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(1000);
        var findElement = browser.element(parameter.CloseButton).isVisible();
        if (findElement == true) {
            browser.element(parameter.CloseButton).click();
        }
        browser.element(parameter.HamburgerMenu).click();
        browser.element("[data-test-id='find-btn']").click();
        var findNextBtn = browser.element(parameter.FindNextButton).isVisible();
        var findAllBtn = browser.element(parameter.FindAllButton).isVisible();
        var closeBtn = browser.element(parameter.CloseButton).isVisible();
        var abortBtn = browser.element(parameter.AbortButton).isVisible();
        if (findNextBtn && findAllBtn && closeBtn && abortBtn == true) {
            assertValue = true;
        }
        assert.equal(assertValue, true);
    });

    it('Verify cancel input button in find option - XYN-1125', function () {
        console.log("Verify cancel input button in find option - XYN-1125");
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(1000);
        messageObject.enterValueInFindFilter("Automation");
        var cancelBtn = browser.element("[data-test-id='cancel-input']").isVisible();
        assert.equal(cancelBtn, true);
    });

    // it('Verify no-data found error message after searching invalid value - XYN-1125', function () {
    //     console.log("Verify no-data found error message after searching invalid value");
    //     baseMethods.clickOnAdvancedSearchViewIcon()
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.WidgetRoot);
    //     browser.pause(1000);
    //     messageObject.enterValueInFindFilter("Automation");
    //     var errorMsg = browser.element("[data-test-id='no-data-msg']").isVisible();
    //     assert.equal(errorMsg, true);
    // });

    it('Verify data after searching valid value - XYN-1125', function () {
        console.log("Verify data after searching valid value ");
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(1000);
        messageObject.enterValueInFindFilter("MAC");
        messageObject.clickFindall();
        browser.pause(1500);
        var eventsdataTab = browser.elements("//div[@data-test-id='data-grid-container']/ngx-datatable[1]/div[1]/datatable-body[1]/datatable-selection[1]/datatable-scroller[1]/datatable-row-wrapper");
        var objectCount = eventsdataTab.value;
        var elementCount = objectCount.length;
        var count = false;
        if (elementCount > 2) {
            count = true;
        }
        assert.equal(count, true);
    });

    it('Verify find otion visibility after clicking close button - XYN-1125', function () {
        console.log("Verify find otion visibility after clicking close button - XYN-1125 ");
        var assertValue = false;
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(1000);
        var findElement = browser.element(parameter.CloseButton).isVisible();
        if (findElement == true) {
            browser.element(parameter.CloseButton).click();
        }
        var findNextBtn = browser.element(parameter.FindNextButton).isVisible();
        var findAllBtn = browser.element(parameter.FindAllButton).isVisible();
        var closeBtn = browser.element(parameter.CloseButton).isVisible();
        var abortBtn = browser.element(parameter.AbortButton).isVisible();
        if (findNextBtn && findAllBtn && closeBtn && abortBtn == true) {
            assertValue = true;
        }
        assert.equal(assertValue, false);
    });
    it('Verify optimization message search - XYN-1457', function () {
        console.log("Test started for verification of optimization message search");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        var findElement = browser.element(parameter.CloseButton).isVisible();
        if (findElement == true) {
            browser.element(parameter.CloseButton).click();
        }
        browser.element(parameter.HamburgerMenu).click();
        browser.element("[data-test-id='find-btn']").click();
        messageObject.enterValueInFindFilter(config.findMessageFilterValue);
        messageObject.clickFindall(parameter.FindAllButton);
        browser.pause(300);
        var eventsdataTab = browser.elements("//*[@name='messages-search-grid']//datatable-scroller[@class='datatable-scroll']/datatable-row-wrapper");
        var objectCount = eventsdataTab.value;
        var elementCount = objectCount.length;
        var count = false;
        if (elementCount > 2) {
            count = true;
        }
        assert.equal(count, true);
        console.log("Test completed for verification of optimization message search");
    });

    // it('XYN-2322_Verify if Messages widget is showing messages', function () {
    //     this.skip();
    //     console.log("XYN-2322 Test execution started");
    //     baseMethods.clickOnAdvancedSearchViewIcon();
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.MessageDroppable);
    //     messageObject.enterValueInFindFilter("paging");
    //     messageObject.clickFindall();
    //     browser.pause(500);
    //     var f = baseMethods.verifyTextInDecodedMessagesGrid("paging");
    //     console.log(f);
    //     assert.equal(f, true, "verifyTextInDecodedMessagesGrid function not return as expcted");
    //     var actualMsg = baseMethods.VerifyActiveRowCellValue("systemInformation");
    //     console.log(actualMsg)
    //     assert.equal(actualMsg, true, actualMsg + ":expected message is not same as acutal message:" + true);
    //     console.log("XYN-2322 Test execution ended");
    // });
    //2375
    it('Verify whether Find section will expand and show all the result - XYN-2375,XYN-2363', function () {
        console.log("Test started for find section to display results-XYN-2375,XYN-2363");
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        messageObject.enterValueInFindFilter(config.findMessageFilterValue2);
        browser.pause(400);
        messageObject.waitForEnabled();
        messageObject.clickFindall(parameter.FindAllButton);
        browser.pause(400);
        var findAllRow = browser.elements(parameter.MessagesFindGridRowContent);
        var count;
        count = findAllRow.value.length;
        console.log("count of filtered rows: ", count);
        expect(count).to.be.gt(2);
        console.log("Test completed for find section to display result- XYN-2375,XYN-2363");
    });
    //1538 //my-json-viewer/section[@class='ngx-json-viewer']
    it('Verify whether decoded message is displayed and matching result is highlighted- XYN-1538,XYN-1540,XYN-1619', function () {
        console.log("Test started for decoded message highlighting the matching records-XYN-1538,XYN-1540,XYN-1619");
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        messageObject.clickDecodeMessageIcon();
        messageObject.enterValueInFindFilter(config.findMessageFilterValue2);
        browser.pause(400);
        messageObject.clickFindall(parameter.FindAllButton);
        browser.pause(400);
        messageObject.clickFindnext(parameter.FindNextButton);
        var decodedMessage = browser.elements(parameter.DecodedMessageForFilteredValue);
        console.log(decodedMessage.getText());
        expect(decodedMessage.getText().toLowerCase()).to.include(config.findMessageFilterValue2);
        console.log("Test completed for decoded message highlighting the matching records-XYN-1538,XYN-1540,XYN-1619");
    });
    //1535 //my-json-viewer/section[@class='ngx-json-viewer']
    it('Verify whether filtering in findAll grid happens only on filtered results in message view panel - XYN-1535', function () {
        console.log("Test started for findAll button filters only from the filtered results of Message View panel");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        messageObject.enterMessageFilterValue(config.findMessageFilterValue3);
        browser.pause(100);
        var rowClass = browser.elements(parameter.MessagesSelectedRow);
        console.log(rowClass.getText());
        messageObject.enterValueInFindFilter(config.findMessageFilterValueInvalid);
        browser.pause(100);
        messageObject.clickFindall(parameter.FindAllButton);
        //browser.pause(9000);
        var nodatamessage=parameter.MessagesNoDataFoundForFilteredValue;
        // browser.waitUntil(() => {
        //     browser.elements(parameter.MessagesNoDataFoundForFilteredValue).value.length > 0
        //   }, 9000, 'no data found for the invalid filter text in FindAll:');
        browser.waitForExist(nodatamessage);
        if (browser.elements(parameter.MessagesNoDataFoundForFilteredValue).value.length > 0) {
            var noResult = browser.element(parameter.MessagesNoDataFoundForFilteredValue);
            console.log("no data found for the invalid filter text in FindAll: ", config.findMessageFilterValue2);
            console.log(noResult.getText());
            expect(noResult.getText().toLowerCase()).to.include(config.unableToFindData);
        }
        messageObject.removeFindFilterText();
        messageObject.enterValueInFindFilter(config.findMessageFilterValue);
        browser.pause(100);
        messageObject.clickFindall(parameter.FindAllButton);
        browser.pause(1000);
        var j = 2, count = browser.elements(parameter.MessagesFindGridRowContent).value.length;
        console.log("count: ", count);
        while (count > 2) {
            var findRowContent = browser.elements(parameter.MessagesFindGridRowContent+"[" + j + "]").getText();
            expect(findRowContent.toLowerCase()).to.include(config.findMessageFilterValue);
            count--;
            j++;
        }
        messageObject.removeFindFilterText();
        messageObject.enterValueInFindFilter(config.findMessageFilterValueInvalid);
        browser.pause(100);
        messageObject.clickFindall(parameter.FindAllButton);
        browser.pause(2000);
        if (browser.elements(parameter.MessagesNoDataFoundForFilteredValue).value.length > 0) {
            var noResult = browser.element(parameter.MessagesNoDataFoundForFilteredValue);
            console.log("no data found for the invalid filter text in FindAll: ", config.findMessageFilterValue2);
            console.log(noResult.getText());
            expect(noResult.getText().toLowerCase()).to.include(config.unableToFindData);
        }
        console.log("Test completed for findAll button filters only from the filtered results of Message View panel-XYN-1535");
    });
    //1116
    it('Verify whether user can navigate through message data using Find Next after finding all filtered results and the result sync with the message in message view panel - XYN-1116', function () {
        console.log("Test started for find next button working after finding all filtered results and also the message view panel syncing with the selected record from the grid- XYN-1116");
        //this.skip();
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.MessageDroppable);
        browser.pause(100);
        messageObject.enterValueInFindFilter(config.findMessageFilterValue);
        baseMethods.verifySpinnerVisibility();
        messageObject.clickFindall(parameter.FindAllButton);
        var j = 1;
        var findAllRow = browser.elements(parameter.MessagesFindGridRowContent);
        console.log(findAllRow.value.length);
        console.log("timeing");
        messageObject.clickFindnext(parameter.FindNextButton);
        browser.pause(300);
        var actualTime = browser.elements(selectedTimeColValueInFindGrid).getText();
        console.log(actualTime);
        while (j <= 4) {
            messageObject.clickFindnext(parameter.FindNextButton);
            console.log("next row contents");
            var findNextRow = browser.elements(parameter.MessagesFindGridSelectedRowContent);
            console.log(findNextRow.getText());
            var timeDiff = browser.elements(selectedTimeColValueInFindGrid).getText();
            if (j > 2) {
                if(timeDiff!=actualTime){
                expect(findNextRow.getText().toLowerCase()).to.include(config.findMessageFilterValue);
                assert.notEqual(timeDiff, actualTime);
                }
            }
            console.log("actual time before assigning", actualTime);
            actualTime = timeDiff;
            findAllRow = findNextRow.getText();
            console.log("actual time after assigning", timeDiff);
            j++;
        }
        // var rowClass = browser.elements("//datatable-row-wrapper/datatable-body-row[contains(@class,'datatable-body-row active')]");
        // console.log(rowClass.getText());
        var timeOnView = browser.elements(selectedTimeColValue);
        console.log("time on view", timeOnView.getText());
        // var rowclassText = rowClass.getText();
        expect(findNextRow.getText().toLowerCase()).to.include(config.findMessageFilterValue);
        assert.equal(actualTime, timeOnView.getText());
        console.log("Test completed for find next button XYN-1116");
    });
    // XYN-1115
    it('Verify whether user can navigate through message data sequentially using Find Next after finding all filtered results - XYN-1115', function () {
        console.log("Test started for find next button working after finding all filtered results- XYN-1115");
        //baseMethods.clickOnAdvancedSearchViewIcon()
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        messageObject.enterValueInFindFilter(config.findMessageFilterValue);
        browser.pause(300);
        messageObject.clickFindall(parameter.FindAllButton);
        var j = 1;
        var findAllRow = browser.elements(parameter.MessagesFindGridRowContent);
        console.log(findAllRow.value.length);
        messageObject.clickFindnext(parameter.FindNextButton);
        browser.pause(300);
        console.log("timeing");
        var actualTime = browser.elements(selectedTimeColValueInFindGrid).getText();
        console.log(actualTime);
        while (j <= 2) {

            messageObject.clickFindnext(parameter.FindNextButton);
            browser.pause(1000);
            console.log("next row contents");
            var findNextRow = browser.elements(parameter.MessagesFindGridSelectedRowContent);
            console.log(findNextRow.getText());
            var timeDiff = browser.elements(selectedTimeColValueInFindGrid).getText();
            console.log("printing each rows in findall");
            if (j > 1) {
                expect(findNextRow.getText().toLowerCase()).to.include(config.findMessageFilterValue);
                assert.notEqual(timeDiff, actualTime);
            }
            console.log("actual time before assigning", actualTime);
            actualTime = timeDiff;
            console.log("actual time after assigning", actualTime);
            j++;
        }
        console.log("Test completed for find next button working after finding all filtered results-XYN-1115");
    });

    it('Verify Abort button beside Find Search bar - XYN-2368', function () {
        console.log("Test started for verification of Abort button beside Find Search bar-XYN-2368");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(500);
        messageObject.enterValueInFindFilter(config.findMessageFilterValue);
        browser.pause(500);
        messageObject.clickFindall(parameter.FindAllButton);
        messageObject.clickFindnext(parameter.FindNextButton);
        messageObject.clickAbort();
        var buttonname = browser.element(parameter.AbortButton).getText();
        assert.equal(config.findMessageAbortButtonText, buttonname);
        console.log("Test completed for verification of Abort button beside Find Search bar-XYN-2368");
    });
    //test case currently not in scope
    // it('Verify  message search - XYN-1456', function () {
    //     console.log("Test started for verification of optimization message search");
    //     var assertValue = false;
    //     //baseMethods.clickOnAdvancedSearchViewIcon();
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
    //     browser.pause(500);
    //     browser.element(parameter.HamburgerMenu).click();
    //     var findElement = browser.element(parameter.FindMsgBtn).isVisible();
    //     if (findElement == true) {
    //         browser.element(parameter.FindMsgBtn).click();
    //         assertValue = true;
    //     }
    //     assert.equal(assertValue, true);
    //     console.log("Test completed for verification of optimization message search");

    // });

    it('Verify search text retain  in find search bar - XYN-1536', function () {
        console.log("Test started for verification of search text retain in find search bar");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(500);
        browser.element(parameter.HamburgerMenu).click();
        browser.element(parameter.FindMsgBtn).click();
        messageObject.enterValueInFindFilter(config.findMessageFilterValue);
        var SearchTerm = browser.element("[data-test-id='input-find-msg']").getText();
        messageObject.clickFindall(parameter.FindAllButton);
        messageObject.enterMessageFilterValue(config.findMessageFilterValue);
        var SearchTerm2 = browser.element("[data-test-id='msg-filter']").getText();
        assert.equal(SearchTerm, SearchTerm2);
        var eventsdataTab = browser.elements("//*[@name='messages-search-grid']//datatable-row-wrapper[2]");
        var objectCount = eventsdataTab.value;
        var elementCount = objectCount.length;
        var count = false;
        if (elementCount == 0) {
            count = true;
        }
        assert.equal(count, true);
        console.log("Test completed for verification of search text retain in find search bar");
    });

    it('Verify messages filter text with column selection - XYN-2315', function () {
        console.log("Test started for verification of messages filter text with column selection");
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(500);
        messageObject.enterMessageFilterValue(config.findMessageFilterValue);
        var selectRowElement = browser.element(parameter.SelectRowBtn);
        selectRowElement.click();
        browser.pause(1000);
        var option=browser.element(parameter.Message);
        option.click();
        browser.pause(2000);
        baseMethods.verifySpinnerVisibility();
        var rowData = browser.elements(parameter.MessagesRowContent);
        var objectCount = rowData.value;
        var elementCount = objectCount.length;
        var count = false;
        if (elementCount > 2) {
            count = true;
        }
        assert.equal(count, true);
        genericObject.clickOnDataMenu();
        //browser.element("[data-test-id='search-child-item-filtered']").click();
        browser.pause(200);
        searchdataObject.clearsearchkeyword();
        baseMethods.clickOnSearchViewIcon(config.datasetWithTwoDevice);
        genericObject.clickOnDatasetReplaceButton();
        genericObject.clickOnReplaceOkWithoutSaving();
        browser.pause(2000);
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        messageObject.enterMessageFilterValue(config.findMessageFilterValue);
        var rowDataAfterDatasetReplace = browser.elements(parameter.MessagesRowContent).value.length;
        expect(rowDataAfterDatasetReplace).to.be.gt(2);

        console.log("Test completed for verification of messages filter text with column selection");

    });

    it('Verify ALF Index column in Messages View - XYN-2067', function () {
        console.log("Test started for verification of ALF Index column in Messages View - XYN-2067");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        browser.pause(800);
        var col=browser.elements(parameter.MessageColumnSelector);
        console.log(col.value.length);
        //col.click();
        var ColumnName = browser.element("[data-test-id='ALF Index']").getText();
        console.log(ColumnName);
        assert.equal("ALF Index ", ColumnName);
        console.log("Test completed for verification of ALF Index column in Messages View");
    });

    it('Verify whether error message is displaying when there is no sync happening - XYN-2037', function () {
        console.log("Test started for verification of error message in Messages View- XYN-2037");
        this.skip();
        //baseMethods.clickOnAdvancedSearchViewIcon();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        messageObject.enterMessageFilterValue(config.findMessageFilterValue2);
        browser.pause(300);
        var eventsdataTab = browser.elements(parameter.MessagesRowContent);
        var objectCount = eventsdataTab.value;
        var elementCount = objectCount.length;
        var count = false;
        if (elementCount > 2) {
            count = true;
        }
        assert.equal(count, true);
        var ErrorMsg = browser.element("[class='syncMessage']").getText();
        console.log(ErrorMsg);
        assert.equal("", ErrorMsg);
        console.log("Test completed for verification of error message in Messages View");
    });

    // it('Verify UE Log testing - Message View- XYN-2070', function () {
    //     console.log("Test started for verification UE Log testing in Messages View");
    //     this.skip();
    //     baseMethods.clickOnAdvancedSearchViewIcon()
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.MessageDroppable);
    //     browser.pause(1000);
    //     messageObject.enterMessageFilterValue(config.findMessageFilterValue4);
    //     browser.pause(1000);
    //     var j = 1;
    //     rowCount = browser.elements("//datatable-row-wrapper").value.length;
    //     console.log(rowCount);
    //     while (rowCount > 0) {
    //         var rowContent = browser.elements("//datatable-row-wrapper[" + j + "]");
    //         console.log(rowContent.getText());
    //         expect(rowContent.getText().toLowerCase()).to.include(config.findMessageFilterValue5);
    //         j++;
    //         rowCount--;
    //     }
    //     console.log("Test completed for verification UE Log testing in Messages View");

    // });

    // it.skip('XYN-2374_To verify the columns in the Find result', function () {       
    //     console.log("XYN-2374 Test execution started");
    //     baseMethods.clickOnAdvancedSearchViewIcon();
    //     browser.pause(500);
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     genericObject.dragAndDropViews(parameter.MessagesDraggable,parameter.MessageDroppable);
    //     browser.pause(300);
    //     messageObject.enterValueInFindFilter(config.findMessageFilterValue2);
    //     messageObject.clickFindall();
    //     baseMethods.verifySpinnerVisibility();
    //     browser.pause(500);
    //     //var sMessageContent=browser.element("div[class='relative-component message-view'] .findall-result-container .datatable-header .datatable-header-cell:nth-child(4)").getText();
    //     var sMessageContent=browser.element(parameter.MessageContent).getText();
    //     assert.equal(sMessageContent,config.columnname1,sMessageContent+" actual column name is not same as expected "+config.columnname1);
    //     var sDevicename=parameter.Device;
    //     browser.execute(function(sDevicename){
    //        //document.querySelector("div[class='relative-component message-view'] .findall-result-container .datatable-header .datatable-header-cell:nth-child(5)").scrollIntoView();
    //        document.querySelector(sDevicename).scrollIntoView();
    //     },sDevicename);
    //     browser.pause(500);
    //     //var sDevice=browser.element("div[class='relative-component message-view'] .findall-result-container .datatable-header .datatable-header-cell:nth-child(5)").getText();
    //     var sDevice=browser.element(parameter.Device).getText();
    //     assert.equal(sDevice,config.columnname2,sDevice+" actual column name is not same as expected "+config.columnname2);
    //    console.log("XYN-2374 Test execution ended");
    // });

    // it.skip('XYN-1534_Verify whether pop-up screen is displaying when Find All or Find Next does not return any result', function () {
    //     console.log("XYN-1534 Test execution started");
    //     baseMethods.clickOnAdvancedSearchViewIcon();
    //     browser.pause(500);
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     genericObject.dragAndDropViews(parameter.MessagesDraggable,parameter.MessageDroppable);
    //     browser.pause(500);
    //     messageObject.enterValueInFindFilter(config.invalidValue);
    //     messageObject.clickFindall();
    //     baseMethods.verifySpinnerVisibility();
    //     browser.pause(10000);
    //     browser.waitForVisible(parameter.NoDataErrorText, 10000);
    //     var sText=browser.element(parameter.NoDataErrorText).getText();
    //     assert.equal(sText,config.unableToFindData,sText+" actual text is not same expected text "+config.unableToFindData)
    //     messageObject.removeFindFilterText();
    //     messageObject.enterValueInFindFilter(config.invalidValue);
    //     messageObject.clickFindnext();
    //     var bStatus=baseMethods.verifyFindNextPopupMessage(config.unableToFindData);
    //     assert.equal(bStatus,true,"verifyFindNextPopupMessage function returns false");
    //    console.log("XYN-1534 Test execution ended");
    // });

    // it.skip('XYN-1616_Verify if Messages widget is showing messages', function () {
    //     console.log("XYN-1616 Test execution started");
    //     baseMethods.clickOnAdvancedSearchViewIcon();
    //     browser.pause(500);
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     genericObject.dragAndDropViews(parameter.MessagesDraggable,parameter.MessageDroppable);
    //     browser.pause(10000);
    //     //var oMessages = browser.elements("//div[@class='message-view'][1]//datatable-scroller[@class='datatable-scroll']/datatable-row-wrapper");
    //     //var oMessages = browser.elements("//div[@class='relative-component message-view'][1]//datatable-scroller[@class='datatable-scroll']/datatable-row-wrapper");
    //     browser.waitForVisible(parameter.MessageWindow,10000)
    //     var oMessages = browser.elements(parameter.MessageWindow);
    //     //var oMessages = messageObject.messageWindowRowsCount();
    //     var objectCount = oMessages.value;
    //     var iMessageCount = objectCount.length;
    //     assert.equal(iMessageCount>3,true,iMessageCount+" row count is less then 3")
    //     console.log(iMessageCount);
    //     browser.pause(500);
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     genericObject.dragAndDropViews(parameter.MessagesDraggable,parameter.WidgetRightBox);
    //     browser.pause(500);
    //     browser.waitForVisible(parameter.MessageWindow2,10000)
    //     //var oMessages = browser.elements("//div[@class='vertical-splitter-container'][1]//datatable-scroller[@class='datatable-scroll']/datatable-row-wrapper");
    //     var oMessages = browser.elements(parameter.MessageWindow1);
    //     //var oMessages = messageObject.messageLeftWindowRowsCount();
    //     var objectCount = oMessages.value;
    //     var iMessageCount = objectCount.length;
    //     assert.equal(iMessageCount>3,true,iMessageCount+" row count is less then 3")
    //     console.log(iMessageCount);
    //     //var oMessages = browser.elements("//div[@class='vertical-splitter-container'][2]//datatable-scroller[@class='datatable-scroll']/datatable-row-wrapper");
    //     var oMessages = browser.elements(parameter.MessageWindow2);
    //     //var oMessages = messageObject.messageRightWindowRowsCount();
    //     var objectCount = oMessages.value;
    //     var iMessageCount = objectCount.length;
    //     assert.equal(iMessageCount>3,true,iMessageCount+" row count is less then 3")
    //     console.log(iMessageCount);
      
    //    console.log("XYN-1616 Test execution ended");
    // });

    // it.skip('XYN-2322_Verify if Messages widget is showing messages', function () {
    //     console.log("XYN-2322 Test execution started");
    //     baseMethods.clickOnAdvancedSearchViewIcon();
    //     browser.pause(500);
    //     genericObject.clickOnDTTab(parameter.ViewsMenu);
    //     genericObject.dragAndDropViews(parameter.MessagesDraggable,parameter.MessageDroppable);
    //     browser.pause(500);
    //     messageObject.enterValueInFindFilter(config.findMessageFilterValue2);
    //     messageObject.clickFindall();
    //     browser.pause(5000);
    //     var f=baseMethods.verifyTextInDecodedMessagesGrid(config.findMessageFilterValue2);
    //     console.log(f);
    //     assert.equal(f,true,"verifyTextInDecodedMessagesGrid function not return as expcted");
    //    var actualMsg=baseMethods.VerifyActiveRowCellValue(config.selectDecodedMessage);
    //    browser.pause(500);
    //    console.log(actualMsg)
    //    assert.equal(actualMsg,true,true+":expected message is not same as acutal message:"+actualMsg);
    //    console.log("XYN-2322 Test execution ended");
    // });
    it('Verify error message if on filtering there is no data present on Messages - XYN-3032', function () {
        console.log("Test started - Verify error message if on filtering there is no data present on Messages - XYN-3032");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);        
        messageObject.enterMessageFilterValue(config.filterVal9);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        var noDatamsg = browser.element(parameter.noData).getText();
        expect(noDatamsg, config.noDataAvailable);
        messageObject.enterMessageFilterValue(config.filterVal9);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        var noDatamsg = browser.element(parameter.noData).getText();
        expect(noDatamsg, config.noDataAvailable);
        console.log("Test completed - Verify error message if on filtering there is no data present on Messages - XYN-3032");
    });
    it('Sync between messages and sequence diagram view - XYN-2397', function () {
        console.log("Test started - Sync between messages and sequence diagram view - XYN-2397");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.SequencediagramDraggable, parameter.WidgetRightBox);
        browser.pause(300);
        var dataTable = browser.elements(parameter.messagesTableRows).value.length;
        console.log(dataTable);
        var j=1;
        while (j <= dataTable ){
            //click few rows to get verify sync
            browser.element(parameter.analyticsview_1).click();            
            messageObject.clickMessagesDataRow(j);
            /*on clicking on Messages table row, Sequence diagram should display either of the following
            1. message 'Unavailable within 2 sec of the selected bin-raw' OR 2. class //div[@class='list-line-center selected']*/
            browser.element(parameter.analyticsview_2).click();
            var errMsg = browser.element(parameter.incorrectQuery).isVisible();
            if (errMsg){
                console.log(browser.element(parameter.incorrectQuery).getText());
                expect(errMsg, config.errMsg);            
            }
            else{
                var syncAvailable = browser.element(parameter.seqSyncRow).isVisible();
                assert.equal(syncAvailable, true);
            }
            j = j+5;
        }
        console.log("Test completed - Sync between messages and sequence diagram view - XYN-2397");
    });
    it('Verify updating selected row to view using Find Next - XYN-2681', function () {
        console.log("Test started - Verify updating selected row to view using Find Next - XYN-2681");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);        
        messageObject.enterValueInFindFilter(config.findAllMessageFilterValue);
        messageObject.clickFindall();
        baseMethods.verifySpinnerVisibility();
        var j = 1;
        var count = browser.elements(parameter.MessagesFindGridRowContent).value.length;
        while (count >= j) {
            var findRowContent = browser.elements(parameter.MessagesFindGridRowContent+"[" + j + "]").getText();
            expect(findRowContent.toLowerCase()).to.include(config.findAllMessageFilterValue);            
            if (count == j) {
                //select the last row to continue with 'click Find Next button'
                browser.element(parameter.MessagesFindGridRowContent+"[" + j + "]").click();               
            }
            j++;
        }        
        //now click multiple times on Find Next button
        for (var i=1; i<=count; i++){
            browser.element(parameter.FindNextButton).click();
            browser.pause(300);
            findRowContent = browser.elements(parameter.MessagesFindGridRowContent+"[" + i + "]").getText();
            expect(findRowContent.toLowerCase()).to.include(config.findAllMessageFilterValue);
        }               
        console.log("Test completed - Verify updating selected row to view using Find Next - XYN-2681");
    });
    it('To verify NR RRC Messages in Messages view & "L3SM" messages colouring - XYN-2776', function () {
        console.log("Test started - To verify messages colouring - XYN-2776");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);        
        messageObject.enterMessageFilterValue(config.filterVal8);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        var j = 1;
        var count = browser.elements(parameter.messagesTableRows).value.length;
        while (count >= j) {
            var findRowContent = browser.elements(parameter.MessagesRowContent+"[" + j + "]").getText();
            expect(findRowContent.toLowerCase()).to.include(config.filterVal8.toLowerCase());
            var msgBGColor = browser.element(parameter.msgColumnInFilter).getAttribute(config.style);
            expect(msgBGColor).to.include(config.rrcConnectionReleaseBGColor);                                    
            j++;
        }
        console.log("Test completed - To verify messages colouring - XYN-2776");
    });
    it('Verification of message column color per protocol - XYN-3000', function () {
        console.log("Test started - Verification of message column color per protocol - XYN-3000");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        var selectRowElement = browser.element(parameter.SelectRowBtn);
        selectRowElement.click();
        browser.pause(300);
        var option=browser.element(parameter.Protocol);
        option.click();   
        var res = messageObject.messagesRowContentAttribute(config.protocol_PHY.toLowerCase(), config.protocol_PHY_RGBColorCode);
        assert(res, true);
        messageObject.clearFilterContent();
        var res = messageObject.messagesRowContentAttribute(config.protocol_RRC.toLowerCase(), config.protocol_RRC_RGBColorCode);
        assert(res, true);
        messageObject.clearFilterContent();
        var res = messageObject.messagesRowContentAttribute(config.protocol_EPC.toLowerCase(), config.protocol_EPC_RGBColorCode);
        assert(res, true);
        messageObject.clearFilterContent();
        var res = messageObject.messagesRowContentAttribute(config.protocol_MAC.toLowerCase(), config.protocol_MAC_RGBColorCode);
        assert(res, true);
        messageObject.clearFilterContent();
        console.log("Test completed - Verification of message column color per protocol - XYN-3000");
    });
    it('Verification of resizing of "Message View" on enabling the decoded message - XYN-2919', function () {
        console.log("Test started - Verification of resizing of Message View on enabling the decoded message - XYN-2919");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MessagesDraggable, parameter.FreshViewsDroppable);
        baseMethods.verifySpinnerVisibility();
        var splitonMessagesDecodeView = browser.element(parameter.splitOnDecodeMessages).isVisible();
        assert.equal(splitonMessagesDecodeView, false);
        messageObject.clickDecodeMessageIcon();
        splitonMessagesDecodeView = browser.element(parameter.splitOnDecodeMessages).isVisible();
        assert.equal(splitonMessagesDecodeView, true);        
        var openDecodeView = browser.element(parameter.decodeMessages).isVisible();
        assert.equal(openDecodeView, true);
        messageObject.clickDecodeMessageIcon();
        openDecodeView = browser.element(parameter.decodeMessages).isVisible();
        assert.equal(openDecodeView, false);
        console.log("Test completed - Verification of resizing of Message View on enabling the decoded message - XYN-2919");
    });
});
