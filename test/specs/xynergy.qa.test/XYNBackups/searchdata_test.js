let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    afterEach(function () {
        if(isElectron()){
        console.log("Clearing the Search keyword and results");
        baseMethods.SelectTabs("SEARCHDATA");
        searchdataObject.newSearchPage();
        console.log("Cleared the Search keyword and results");
        }
        
    });

    it('Verify if user is able to search by FileName XYN-1474', function(){
        console.log("Test started - to verify if user is able to search by Filename XYN-1474");
        //baseMethods.navigateToAdvancedSearchMenu(); 
        browser.element(parameter.DataMenu).click();
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,config.searchTextForFIleName);
        //baseMethods.searchNQLQueryForSingleDevice();
        browser.element(parameter.treeNodeDropDown).click();
        var listItem = browser.elements(parameter.listItem).getText();
        console.log(listItem);
        var listItemlength = browser.elements(parameter.listItem).value.length;
        console.log("Filename results count is " + listItemlength);
        var j=0;
        while (listItemlength > j){
            //var rowContent = browser.element(listItem[j]).getText();
            //console.log(rowContent);  
            expect(listItem[j]).to.include(config.searchTextForFIleName);
            j++;
        }
        console.log("Verified each record from filter results if it matches with the search keyword");
        searchdataObject.selectdatasetresult();
        var selectdataset = browser.element(parameter.filteredItem);
        var visibility = selectdataset.isVisible();
        assert(visibility, true);
        console.log("Test completed - to verify if user is able to search by Filename");
    });

    it('XYN-2789_To verify search data retainity', function () {
        console.log("Test started- XYN-2789_To verify search data retainity");
        baseMethods.SelectTabs("SEARCHDATA");
       baseMethods.SerchQueryInNewSearch(config.searchItemFileName,config.searchCondition,config.searchTextForFIleName);
       var bResultStatus=browser.isExisting(parameter.searchResultPane);
       assert.equal(bResultStatus,true,bResultStatus+" actual status is not same as expected status"+true);
       browser.pause(1000);
       var sExpectedText=browser.element(parameter.resultFirstItem).getText();
       var sText=browser.element(parameter.filteredItem).getText();
       var sText1=sText.split(" ");
       console.log(sText1[0]);
       expect(sExpectedText.toLowerCase()).to.include(sText1[1].toLowerCase());
      // assert.equal(sText,config.searchSelectDataSet,sText+" actual status is not same as expected status"+config.searchSelectDataSet);
       // baseMethods.clickOnSearchData();
       baseMethods.SelectTabs("IMPORTDATA");
       var bImportdataexistance=browser.isExisting(parameter.addData);
       assert.equal(bImportdataexistance,true,bImportdataexistance+" actual status is not same as expected status"+true);
       baseMethods.SelectTabs("SEARCHDATA");
       var sText=browser.element(parameter.filteredItem).getText();
       console.log(sText);
       expect(sText.toLowerCase()).to.include(sText1[1].toLowerCase());
       //assert.equal(sText,config.searchSelectDataSet,sText+" actual status is not same as expected status"+config.searchSelectDataSet);
        console.log("Test completed - XYN-2789_To verify search data retainity");
    });  

    it('XYN-1477_Verify if user gets a message when invalid search criteria is provided ', function(){
        console.log("Test started - XYN-1477_Verify if user gets a message when invalid search criteria is provided");
        baseMethods.SelectTabs("SEARCHDATA");
        var newQueryTab = browser.element(parameter.searchTextArea);
        newQueryTab.click();
        newQueryTab.keys(config.searchTextForFIleNameForError);
        var searchButton = browser.element(parameter.searchButton);
        searchButton.click();
        browser.pause(500);
        var bStatus = browser.element(parameter.incorrectQuery).isExisting();
        console.log(bStatus);
        assert(bStatus, true);
        var errortitle=browser.element(parameter.errorMessageTitle).getText();
        assert(errortitle, config.searcherrorMessageTitle);
        var errorMessage=browser.element(parameter.incorrectQuery).getText();
        assert(errorMessage, config.searcherrorMessageText);
        browser.element(parameter.incorrectQueryClose).click();
        console.log("Test completed - XYN-1477_Verify if user gets a message when invalid search criteria is provided");
    });

    it('XYN-1478_Verify if user gets a message when there are no records matching the search criteria', function () {
        console.log("Test started- XYN-1478_Verify if user gets a message when there are no records matching the search criteria");
       //baseMethods.SerchQueryInNewSearch("Dataset","contains","voice");
       baseMethods.SelectTabs("SEARCHDATA");
       baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,config.searchTextForFIleNameForError);
       var sText=browser.element(parameter.errorMessage).getText();
       console.log(sText+" text is displayed");
       assert.equal(sText,config.searchErrorMsg,sText+" actual status is not same as expected status"+config.searchErrorMsg);
       browser.element(parameter.errorMessageOK).click();
        console.log("Test completed - XYN-1478_Verify if user gets a message when there are no records matching the search criteria");
    }); 
    
    it('Verify if user is able to search by device name - XYN-1475', function(){
        console.log("Test started - Verify if user is able to search by device name - XYN-1475");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemDeviceName,config.searchCondition,config.searchTextForDeviceName);
        //browser.pause(500);
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.treeNodeDropDown).click();
        var listItem = browser.elements(parameter.listItem).getText();
        console.log(listItem);
        var listItemlength = browser.elements(parameter.listItem).value.length;
        console.log("Devicename results count is " + listItemlength);
        var j=0;
        while (listItemlength > j){
            //var rowContent = browser.element(listItem[j]).getText();
            //console.log(rowContent);  
            expect(listItem[j]).to.include(config.searchTextForDeviceName);
            j++;
        }
        console.log("Verified each record from filter results if it matches with the search keyword");
        searchdataObject.selectdatasetresult();
        var selectdataset = browser.element(parameter.filteredItem);
        var visibility = selectdataset.isVisible();
        assert(visibility,true);
        console.log("Test completed - Verify if user is able to search by device name - XYN-1475");
    });

    it('Verify if user is able to search dataset which was saved moment ago using Save Search Results as - XYN-2784, XYN-1480,XYN-2929', function(){
        console.log("Test started - Verify if user is able to search dataset saved moment ago using Save Search Result as and in History and Browse Data - XYN-2784, XYN-1480,XYN-2929");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,config.searchTextForFIleName);
        searchdataObject.clickSaveasbutton();
        searchdataObject.selectsavesearchresultsas();
        //baseMethods.saveDatasetName("QATest");
        var dataset = baseMethods.saveFileName(config.datasetName);
        console.log(dataset);
        searchdataObject.enterSaveSearchResultNameValue(dataset);
        browser.element(parameter.saveSearchResultSaveBtn).click();
        browser.element(parameter.dataSetSaveCancelBtn).click();
        //baseMethods.SelectTabs("IMPORTDATA");
        baseMethods.selectdatasetinImportHistory(dataset);
        var open = browser.element(parameter.openBtn);
        var ienalbled = open.isEnabled();
        assert(ienalbled,true);
        browser.element(parameter.historyFilterClear).click();
        baseMethods.selectdatasetinBrowseDatasets(dataset);
        var open = browser.element(parameter.datasetsOpenBtn);
        var benalbled = open.isEnabled();
        assert(benalbled,true);
        browser.element(parameter.datasetsFilterClear).click();
        console.log("Test Completed - To verify if user is able to search dataset saved moment ago using Save Search Results as and in History and Browse data - XYN-2784, XYN-1480,XYN-2929");
    });

    it('Verify if user is able to search by channel name with <=,=,>=,< conditions - XYN-1476', function(){
        console.log("Test Started - Verify if user is able to search by channel name with <=,=,>=,< conditions - XYN-1476");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryChannel(config.searchItemChannel,config.searchConditionLessThanEqualTo,config.searchTextForChannel);
        baseMethods.verifySpinnerVisibility();
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.treeNodeDropDownCollapsed).click();
        var result = searchdataObject.lessthanequalto();       
        assert(result,true);
        searchdataObject.clearsearchkeyword();
        console.log("verified Less than equal to");

        baseMethods.searchNQLQueryChannel(config.searchItemChannel,config.searchConditionEqualTo,config.searchTextForChannel);
        baseMethods.verifySpinnerVisibility();
        var result = searchdataObject.equalto();       
        assert(result,true);
        searchdataObject.clearsearchkeyword();
        console.log("verified equal to");

        baseMethods.searchNQLQueryChannel(config.searchItemChannel,config.searchConditionGreaterThanEqualTo,config.searchTextForChannel);        
        var result = searchdataObject.greaterthanequalto();   
        assert(result,true);
        searchdataObject.clearsearchkeyword();
        console.log("verified Greater than equal to");

        baseMethods.searchNQLQueryChannel(config.searchItemChannel,config.searchConditionLessThan,config.searchTextForChannel);   
        var result = searchdataObject.lessthan();
        assert(result,true);
        console.log("verified Less than to");
        console.log("Test completed - Verify if user is able to search by channel name with <=,=,>=,< conditions - XYN-1476");

    });
    
    it('Verify if user is able to search by channel name with >,!=,in conditions - XYN-1476', function(){
        console.log("Test Started - Verify if user is able to search by channel name with >,!=,in conditions - XYN-1476");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryChannel(config.searchItemChannel,config.searchConditionGreaterThan,config.searchTextForChannel);  
        baseMethods.verifySpinnerVisibility();
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.treeNodeDropDownCollapsed).click();  
        var result = searchdataObject.greaterthan();       
        assert(result,true);
        searchdataObject.clearsearchkeyword();        
        console.log("Verified greater than case");

        baseMethods.searchNQLQueryChannel(config.searchItemChannel,config.searchConditionNotEqualTo,config.searchTextForChannel);
        baseMethods.verifySpinnerVisibility();
        var result = searchdataObject.notequalto();       
        assert(result,true);
        searchdataObject.clearsearchkeyword();
        console.log("Verified not equal to case");

        var newQueryTab = browser.element(parameter.searchTextArea);
        newQueryTab.click();
        newQueryTab.keys("channel in ");
        var braces = browser.element(parameter.braces);
        braces.click();
        braces.keys(config.searchTextForChannelIn);
        var searchButton = browser.element(parameter.searchButton);
        searchButton.click();
        baseMethods.verifySpinnerVisibility();
        var result = searchdataObject.inconditions();
        // browser.pause(2000);
        // searchdataObject.selectdatasetresult();
        // var selectdataset = browser.element(parameter.filteredItem);
        // var visibility = selectdataset.isVisible();
        assert(result,true);
        //searchdataObject.clearsearchkeyword();
        console.log("Verified in case");
        console.log("Test Completed - Verify if user is able to search by channel name with >,!=,in conditions - XYN-1476");
    });

    it('Verify if user is able to open the selected search result in canvas - XYN-1483', function(){
        console.log("Test Started - Verify if user is able to open the selected search result in canvas - XYN-1483");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,config.searchTextForFIleName);
        searchdataObject.selectdatasetresult();
        var selectdataset = browser.element(parameter.filteredItem);
        var visibility = selectdataset.isVisible();
        assert(visibility,true);
        var selecteddataset = selectdataset.getText();
        console.log(selecteddataset);
        var sText = selecteddataset.split(" ");
        searchdataObject.clickOpeninCanvasbutton();
        baseMethods.openInCanvasValidation();
        baseMethods.verifySpinnerVisibility();
        var scope = browser.element(parameter.scope);
        if(!(scope.getAttribute("class")).includes('enabled')){
            scope.click();
        }
        browser.pause(1000);
        var verifyDataset = browser.element("[data-test-id='"+sText[1]+"']");
        console.log(sText[1]);
        browser.pause(300);
        var visibility1 = verifyDataset.isVisible();
        //console.log(visibility1);
        assert(visibility1,true);
        //browser.pause(200);
        browser.element(parameter.DataMenu).click();
        console.log("Test completed - Verify if user is able to open the selected search result in canvas - XYN-1483");
    });

    it('Verify the functinality of Open on Search Data- XYN-1488', function(){
        console.log("Test Started - Verify the functinality of cancel button for Open Search Terms for Search Data XYN-1488");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.SelectSearchDataTabs("NEWSEARCH");
        baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,config.searchTextForFIleName);        
        searchdataObject.selectdatasetresult();
        var selectdataset = browser.element(parameter.filteredItem);
        var visibility = selectdataset.isVisible();
        assert(visibility, true);
        searchdataObject.clickSaveasbutton();
        searchdataObject.selectsavesearchteamsas();
        // var savesearchterm = browser.element("[data-test-id='modal-title-bar']");
        // var visibility = savesearchterm.isVisible();
        // assert(visibility, true);
        var datasetname = baseMethods.saveFileName(config.searchTermName);
        console.log(datasetname);
        searchdataObject.enterSaveSearchTermsResultNameValue(datasetname);
        browser.element(parameter.saveSearchResultSaveBtn).click();
        browser.element(parameter.dataSetSaveCancelBtn).click();
        searchdataObject.clearsearchkeyword();
        baseMethods.SelectTabs("SEARCHDATA");      
        baseMethods.SelectSearchDataTabs("OPENSEARCHTERMS");
        baseMethods.verifySpinnerVisibility();
        textbox = browser.element(parameter.openSearchFilter).click();     
        textbox.keys(datasetname);
        browser.element(parameter.openFilteredItem).click();
        searchdataObject.clickCancelinOpenSavedSearch();
        console.log("Test completed - Verify the functinality of cancel button for Open Search Terms for Search Data XYN-1488");
    });
    
    // it('SearchData - Verify ToolTip in search data Map filters, XYN-2317', function(){
    //     console.log("Test started - To Verify ToolTip in search data Map filters, XYN-2317");
    //     baseMethods.checkForWorkspaceWidget();
    //     baseMethods.searchEventNQLQuery(config.searchDatasetForRegularTest);
    //     browser.pause(500);
    //     var datasetItem = browser.element(parameter.listItem);
    //     datasetItem.click();
    //     baseMethods.verifySpinnerVisibility();
    //     browser.pause(500);
    //     searchdataObject.ClickPolygon();
    //     var PolygonTooltip = browser.element("[data-tooltip='Draw Polygon']");
    //     PolygonTooltip.isVisible();
    //     assert(PolygonTooltip,true);
    //     searchdataObject.ClickCircle();
    //     var CircleTooltip = browser.element("[data-tooltip='Draw Circle']");
    //     CircleTooltip.isVisible();
    //     assert(CircleTooltip,true);
    //     // searchdataObject.ClickEdit();
    //     // var EditTooltip = browser.element("[data-tooltip='Edit Area']");
    //     // EditTooltip.isVisible();
    //     // assert(EditTooltip,true);
    //     console.log("Test completed - To Verify ToolTip in search data Map filters, XYN-2317");
    //});

    it('SearchData - Verify whether user can open saved dataset, XYN-2928', function(){
        console.log("Test started - To Verify whether user can open saved dataset, XYN-2928");
        baseMethods.checkForWorkspaceWidget();
        baseMethods.searchEventNQLQuery(config.searchDatasetForRegularTest);
        browser.pause(500);
        var datasetItem = browser.element(parameter.listItem);
        datasetItem.click();
        baseMethods.verifySpinnerVisibility();
        var selectdataset = browser.element(parameter.filteredItem);
        var selecteddataset = selectdataset.getText();
        console.log(selecteddataset);
        var sText = selecteddataset.split(" ");
        browser.pause(500);
        searchdataObject.clickSaveasbutton();
        searchdataObject.selectsavesearchresultsas();
        var datasetname = baseMethods.saveFileName(config.datasetName);
        console.log(datasetname);
        searchdataObject.enterSaveSearchResultNameValue(datasetname);
        browser.pause(500);
        browser.element(parameter.saveSearchResultSaveBtn).click();
        browser.pause(500);
        browser.element(parameter.openSavedResult).click();
        browser.pause(200);
        baseMethods.openInCanvasValidation();
        baseMethods.verifySpinnerVisibility();
        var scope = browser.element(parameter.scope);
        if(!(scope.getAttribute("class")).includes('enabled')){
            scope.click();
        }
        browser.pause(1000);
        var verifyDataset = browser.element("[data-test-id='"+sText[1]+"']");
        browser.pause(300);
        var visibility = verifyDataset.isVisible();
        console.log(sText[1]);
        assert(visibility,true);
        //browser.pause(500);
        browser.element(parameter.DataMenu).click();
        console.log("Test Completed - To Verify whether user can open saved dataset, XYN-2928");

    });

    it('SearchData - Verify whether user can able to append files to the already existing dataset, XYN-2927', function(){
        console.log("Test started - To Verify whether user can able to append files to the already existing dataset, XYN-2927");
        baseMethods.checkForWorkspaceWidget();
        baseMethods.searchEventNQLQuery(config.searchDatasetForRegularTest);
        browser.pause(500);
        var datasetItem = browser.element(parameter.listItem);
        datasetItem.click();
        baseMethods.verifySpinnerVisibility();
        var selectdataset = browser.element(parameter.filteredItem);
        var selecteddataset = selectdataset.getText();
        console.log(selecteddataset);
        var sText = selecteddataset.split(" ");
        browser.pause(500);
        searchdataObject.clickSaveasbutton();
        searchdataObject.selectsavesearchresultsas();
        var datasetname = baseMethods.saveFileName(config.datasetName);
        console.log(datasetname);
        searchdataObject.enterSaveSearchResultNameValue(datasetname);
        browser.pause(500);
        browser.element(parameter.saveSearchResultSaveBtn).click();
        browser.pause(100);
        browser.element(parameter.dataSetSaveCancelBtn).click();
        searchdataObject.clickSaveasbutton();
        searchdataObject.selectsavesearchresultsas();
        searchdataObject.enterSaveSearchResultNameValue(datasetname);
        browser.pause(500);
        browser.element(parameter.saveasDropDown).click();
        var WarningText = browser.element(parameter.datasetDuplicateWarning);
        WarningText.isVisible();
        var SaveDatasetName = browser.element(parameter.saveSearchResultSaveBtn);
        SaveDatasetName.click();
        browser.pause(500);
        browser.element(parameter.openSavedResult).click();
        browser.pause(200);
        baseMethods.openInCanvasValidation();
        baseMethods.verifySpinnerVisibility();
        var scope = browser.element(parameter.scope);
        if(!(scope.getAttribute("class")).includes('enabled')){
            scope.click();
        }
        browser.pause(1000);
        var verifyDataset = browser.element("[data-test-id='"+sText[1]+"']");
        browser.pause(300);
        var visibility = verifyDataset.isVisible();
        console.log(sText[1]);
        assert(visibility,true);
        browser.element(parameter.DataMenu).click();
        console.log("Test completed - To Verify whether user can able to append files to the already existing dataset, XYN-2927");
    });

    it('Verify if the selected record gets filtered. - XYN-1484', function(){
        console.log("Test Started - Verify if the selected record gets filtered. - XYN-1484");   
        baseMethods.checkForWorkspaceWidget(); 
        baseMethods.searchEventNQLQuery(config.searchDatasetForRegularTest);
        browser.pause(500); var datasetItem = browser.element(parameter.listItem);
        datasetItem.click();
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        var FilteredDataset = browser.element(parameter.filteredItem);  
        var visibility = FilteredDataset.isVisible();
        assert(visibility,true);
        var Datasetname = FilteredDataset.getText();  
        var SelectedDataset = browser.element(parameter.listItem).getText();
        expect(Datasetname.toLowerCase()).to.include(config.searchDatasetForRegularTest.toLowerCase());
        expect(SelectedDataset.toLowerCase()).to.include(config.searchDatasetForRegularTest.toLowerCase());
        console.log("Test Completed - Verify if the selected record gets filtered. - XYN-1484");   


    });
    // it('Verify the functionality of Time filters - XYN-1487', function(){
    //     console.log("Test Started - Verify the functionality of Time filters - XYN-1487");
    //     baseMethods.SelectTabs("SEARCHDATA");
    //     baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,config.searchTextForFIleName);
    //     browser.element("[data-test-id='filterButton']").click(); 
    //     browser.pause(500);  
    //     browser.element("[data-test-id='last-checkmarkRadio']").click();  
    //     searchdataObject.EnterWeekCount("100"); 
    //     browser.element("[data-test-id='searchButton']").click();  
    //     browser.pause(1000);
    //     browser.element("[data-test-id='seach-default-text']").isVisible();  
    //     browser.element("[data-test-id='filterButton']").click();   
    //     browser.element("[data-test-id='last-checkmarkRadio']").click();  
    //     searchdataObject.EnterWeekCount("1"); 
    //     browser.element("[data-test-id='searchButton']").click();
    //     browser.pause(500);
    //     browser.element("//button[@class='action-button button-small-size']").isVisible();

    //});

    it('Verify the functinality of Open on Search Data- XYN-1488', function(){
        console.log("Test Started - Verify the functinality of open button for Open Search Terms for Search Data XYN-1488");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.SelectSearchDataTabs("NEWSEARCH");
        baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,config.searchTextForFIleName);        
        searchdataObject.selectdatasetresult();
        var selectdataset = browser.element(parameter.filteredItem);
        var visibility = selectdataset.isVisible();
        assert(visibility, true);
        searchdataObject.clickSaveasbutton();
        searchdataObject.selectsavesearchteamsas();
        // var savesearchterm = browser.element("[data-test-id='modal-title-bar']");
        // var visibility = savesearchterm.isVisible();
        // assert(visibility, true);
        var datasetname = baseMethods.saveFileName(config.searchTermName);
        console.log(datasetname);
        searchdataObject.enterSaveSearchTermsResultNameValue(datasetname);
        browser.element(parameter.saveSearchResultSaveBtn).click();
        browser.element(parameter.dataSetSaveCancelBtn).click();
        browser.pause(500);
        searchdataObject.clearsearchkeyword();
        baseMethods.SelectTabs("SEARCHDATA");      
        baseMethods.SelectSearchDataTabs("OPENSEARCHTERMS");
        baseMethods.verifySpinnerVisibility();
        textbox = browser.element(parameter.openSearchFilter).click();     
        textbox.keys(datasetname);
        browser.element(parameter.openFilteredItem).click();
        browser.element(parameter.openSavedSearchBtn).click();
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        var selectdataset = browser.element(parameter.filteredItem);
        selectdataset.isVisible();
        assert(selectdataset, true);
        baseMethods.verifySpinnerVisibility(); 
        browser.pause(500);
        var selectSearchHeader = browser.element(parameter.newSearchHeader).getText();
        expect(selectSearchHeader).to.include(config.searchTermName);
        var selectTextFileName = browser.element(parameter.savedSearchQuery).getText();
        expect(selectTextFileName).to.include(config.searchItemFileName);
        expect(selectTextFileName).to.include(config.searchCondition);
        expect(selectTextFileName).to.include(config.searchTextForFIleName);
        console.log("Test Completed - Verify the functinality of open button for Open Search Terms for Search Data XYN-1488")

    });

    it('Verify the fields on search data page - XYN-1472', function(){
        console.log("Test Started - Verify the fields on search data page XYN-1472");
        baseMethods.SelectTabs("SEARCHDATA");
        var NewSearch = browser.element(parameter.newSearch);
        var visibility = NewSearch.isVisible();         
        assert(visibility,true);
        var EnterQuery = browser.element(parameter.searchTextArea);
        var visibility = EnterQuery.isVisible();         
        assert(visibility,true);    
        var FilterButton = browser.element(parameter.filterButton);
        var visibility = FilterButton.isVisible();         
        assert(visibility,true);
        var SearchButton = browser.element(parameter.searchButton);
        var visibility = SearchButton.isVisible();         
        assert(visibility,true);
        console.log("Test Completed - Verify the fields on search data page XYN-1472");
    });

    it('Verify whether search results shows up when user types in already existing dataset name and ensure it save dataset name in CAPS - XYN-2926, XYN-2925', function(){
        console.log("Test started - Verify whether search results shows up when user types in existing dataset name and ensure it save dataset name in CAPS - XYN-2926, XYN-2925");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);
        searchdataObject.selectdatasetresult();
        searchdataObject.clickSaveasbutton();
        //var saveresultas = browser.element("//button[@class='action-button']").click();
        var saveresult = browser.element(parameter.saveSearchResult).click();
        var FileInCAPS = baseMethods.saveFileName(config.searchTermName);
        searchdataObject.enterSaveSearchResultNameValue(FileInCAPS.toUpperCase());
        //browser.element("[data-test-id='SavedNameinput']").click();
        var SaveDataset = browser.element(parameter.saveSearchResultSaveBtn).click();
        var closeBtn = browser.element(parameter.dataSetSaveCancelBtn).click();
        searchdataObject.clickSaveasbutton();
        //var saveresultas = browser.element("//button[@class='action-button']").click();
        var saveresult = browser.element(parameter.saveSearchResult).click();
        searchdataObject.enterSaveSearchResultNameValue(FileInCAPS.toUpperCase());
        browser.pause(5000);
        listValue = browser.element(parameter.saveasDropDown).getText();
        assert.equal(FileInCAPS.toUpperCase(), listValue);
        browser.element(parameter.saveSearchResultName).click();
        browser.element(parameter.dataSetSaveCancelBtn).click();
        console.log("Test completed - Verify whether search results shows up when user types in existing dataset name and ensure it save dataset name in CAPS - XYN-2926, XYN-2925");
    });

    it('Verify if the Time filters are displayed on  clicking on [Filters] button - XYN-1486', function(){
        console.log("Test started - Verify if the Time filters are displayed on  clicking on [Filters] button - XYN-1486");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);
        var selectFilterBtn = searchdataObject.searchDataFilterButton();        
        var span = browser.elements("//div[@class='time-filter-modes']//li").getText();
        var timefiltercount = browser.elements("//div[@class='time-filter-modes']//li").value.length;
        console.log(timefiltercount);
        var j=1;
        var iTrue=0;
        while ((timefiltercount >= j) && (iTrue == 0)){
            var rowContent = browser.elements("//div[@class='time-filter-modes']//li[" + j +"]").getText(); 
            console.log(rowContent);  
            if (rowContent.includes("Last")){
                iTrue=1;
            }
            j++;
        }
        expect(span).to.include("No Time Filter");
        expect(span).to.include("Start Time");
        expect(span).to.include("End Time");
        expect(browser.element(parameter.dropdownHours).getText()).to.include("Hours"); 
        expect(browser.element(parameter.dropdownDays).getText()).to.include("Days");   
        expect(browser.element(parameter.dropdownWeeks).getText()).to.include("Weeks");
        console.log("Test completed - Verify if the Time filters are displayed on  clicking on [Filters] button - XYN-1486");
    });

    it('Verify if user is able to save the search parameters - XYN-1479', function(){
        console.log("Test started - Verify if user is able to save the search parameters - XYN-1479");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);
        searchdataObject.selectdatasetresult();
        var saveresultas = searchdataObject.clickSaveasbutton();
        expect(browser.element(parameter.saveResultBtn).getText()).to.include(config.saveSearchTerm);
        expect(browser.element(parameter.saveSearchResult).getText()).to.include(config.saveSearchResultAs);
        searchdataObject.selectsavesearchteamsas();
        var datasetname = baseMethods.saveFileName(config.searchTermName);
        searchdataObject.enterSaveSearchTermsResultNameValue(datasetname);
        browser.element(parameter.saveSearchResultSaveBtn).click();
        browser.pause(500);
        expect(browser.element(parameter.mmc).getText()).to.include(config.searchTermSave);
        expect(browser.element(parameter.openSavedResult).getText()).to.include(config.open);
        expect(browser.element(parameter.dataSetSaveCancelBtn).getText()).to.include(config.close);
        searchdataObject.DataSetSaveCancelBtn();
        console.log("Test completed - Verify if user is able to save the search parameters - XYN-1479");
    });

    it('XYN-1473_Verify if user is able to search by dataset ', function(){
        console.log("Test started - XYN-1473_Verify if user is able to search by dataset");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);
        // var selectdataset = browser.element(parameter.SearchResultPane);
        // var visibility = selectdataset.isVisible();
        // console.log(visibility);
        // assert(visibility, true);
        browser.pause(1000);
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.treeNodeDropDownCollapsed).click();
        var listItem = browser.elements(parameter.listItem).getText();
        console.log(listItem);
        var listItemlength = browser.elements(parameter.listItem).value.length;
        console.log("Dataset results count is " + listItemlength);
        var j=0;
        while (listItemlength > j){
            //var rowContent = browser.element(listItem[j]).getText();
            //console.log(rowContent);  
            expect(listItem[j].toLowerCase()).to.include(config.searchDatasetForRegularTest.toLowerCase());
            j++;
        }
        console.log("Verified each record from filter results if it matches with the search keyword");
        // var bStatus=baseMethods.verifyDataSetNameInSearchResult(config.datasetName);
        // console.log(bStatus);
        // assert(bStatus, true);
        console.log("Test completed - XYN-1473_Verify if user is able to search by dataset");
    });

    it('Verify search filter with disabled files - XYN-2320', function(){
        console.log("Test started - Verify search filter with disabled files - XYN-2320");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);
        searchdataObject.selectdatasetresult();
        browser.element(parameter.treeNodeDropDown).click();
        browser.element(parameter.listItem1).click();
        browser.pause(200);
        var sText=browser.element(parameter.errorMessage).getText();
        console.log(sText+" text is displayed");
        assert.equal(sText,config.searchErrorMsg,sText+" actual status is not same as expected status"+config.searchErrorMsg);
        browser.element(parameter.errorMessageOK).click();
        console.log("Test completed - Verify search filter with disabled files - XYN-2320");
    });

    it('Verify that when user opens the same dataset again then it should not trigger add/replace message - XYN-3454', function(){
        console.log("Test started - Verify that when user opens the same dataset again then it should not trigger add/replace message - XYN-3454");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);        
        var searchButton = browser.element(parameter.searchButton);
        searchButton.click();
        var openButton = browser.element(parameter.searchDataOpenBtn);
        openButton.click();
        browser.pause(100);
        var dataTab = browser.element(parameter.DataMenu);
        dataTab.click();        
        openButton.click();
        browser.pause(100);
        var openDataTitleBar = browser.element(parameter.sameDatasetDialogWindow);
        var visibility = openDataTitleBar.isVisible();         
        assert(visibility,true);
        var openDataTitleBarClose = browser.element(parameter.sameDatasetDialogWindowClose);
        openDataTitleBarClose.click();        
        dataTab.click();             
        console.log("Test completed - Verify that when user opens the same dataset again then it should not trigger add/replace message - XYN-3454");
    });

    it('Verify Search Data validation prompts and Miscellaneous - XYN-3592', function(){
        console.log("Test started - Verify Search Data validation prompts and Miscellaneous - XYN-3592");
        baseMethods.SelectTabs("SEARCHDATA");        
        baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,'');        
        var errorTitle = browser.element(parameter.errorMessageTitle);
        var visibility = errorTitle.isVisible();         
        assert(visibility,true);
        var errorCloseBtn = browser.element(parameter.incorrectQueryClose);
        errorCloseBtn.click();
        searchdataObject.clearsearchkeyword();
        baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,config.searchTextForFIleNameForError);        
        var sText=browser.element(parameter.errorMessage).getText();        
        console.log(sText+" text is displayed");        
        assert.equal(sText,config.searchErrorMsg,sText+" actual status is not same as expected status"+config.searchErrorMsg);        
        browser.element(parameter.errorMessageOK).click();       
        var sText1=browser.element(parameter.searchTextArea).getText();
        console.log(sText1+" text is displayed");
        //assert.equal(sText1,config.searchTextForFIleNameForError,sText+" actual text is not same as expected"+config.searchTextForFIleNameForError);
        console.log("Test completed - Verify Search Data validation prompts and Miscellaneous - XYN-3592");
    });

/* Keep the below function as last function in this test file */

    it('Verify Save Workspace from Add/replace window - XYN-2155', function(){
        console.log("Test started - Verify save workspace from Add/Replace window - XYN-2155");
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchALFDataset);
        searchdataObject.selectdatasetresult();
        var selectdataset = browser.element(parameter.filteredItem);
        var visibility = selectdataset.isVisible();
        assert(visibility,true);
        baseMethods.verifySpinnerVisibility();
        console.log("1");
        browser.pause(500);
        searchdataObject.clickOpeninCanvasbutton();
        browser.pause(1000);
        baseMethods.openInCanvasValidation();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        baseMethods.dragAndDrop(parameter.MessagesDraggable,parameter.MessageDroppable);
        browser.element(parameter.DataMenu).click();
        browser.element(parameter.filteredItem).click();
        browser.pause(200);
        searchdataObject.clearsearchkeyword();
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);
        searchdataObject.selectdatasetresult();
        var selectdataset = browser.element(parameter.filteredItem);
        var visibility = selectdataset.isVisible();
        assert(visibility,true);
        searchdataObject.clickOpeninCanvasbutton();
        browser.pause(1000);
        //baseMethods.clickOnSearchViewIcon(config.searchALFDataset);
        browser.element(parameter.dataScopeReplaceBtn).click();
        browser.element(parameter.dataScopeReplaceSaveBtn).click();
        var workspacename = baseMethods.saveFileName("a");
        searchdataObject.enterSaveWorkspaceNameValue(workspacename);
        browser.element(parameter.workspaceSaveBtn).click();
        browser.pause(3000);
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        genericObject.clickOnDTTab(parameter.workspacesMenu);
        browser.element("[data-test-id='"+workspacename+"']").click();
        browser.element(parameter.workspaceOpenBtn).click();
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        var checktab = browser.elements("[title='Messages']").getText();
        console.log(checktab);
        //tabvisible = browser.elements(checktab[0]).isVisible();
        //var tabvisible = checktab.isVisible();
        //assert(tabvisible,true);
        expect(checktab[0]).to.include("Messages");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        baseMethods.clickOnViewsCloseAllBtn();
        browser.element(parameter.closeAllViewsbtn).click();
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.DataMenu).click();
        console.log("Test completed - Verify save workspace from Add/Replace window - XYN-2155");
    });

});