let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
var tableViewObject=require('../../../pageobjects/tableViewPage.js');
let timeseries=require("../../../pageobjects/timeseries.js");
let eventsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let xynergyconstants=require('../../test_xynergy.constants.js');
let genericObject=require('../../generic_objects.js');
let config=require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var conf = require('../../../config');
var expect = require('chai').expect;require('../../commons/global')();

describe('Table View',function(){
    var init=0;
    beforeEach(function () {
        if (!isElectron()) {
            console.log("Logging into the application");
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest)
            
            console.log("Logging successful");
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
        }

    });

    afterEach(function () {
        if(isElectron()){
        console.log("closing all open views");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        console.log("closing all open views");
            var closeAllViews=browser.element("[data-test-id='close-all-btn']");
            if(closeAllViews.isVisible()){
            closeAllViews.click();
            browser.pause(300);
            console.log("clicking on close btn");
            //var closeAllViewsTitleBar=browser.element("[data-test-id='Close All Views-Title-Bar']");
            var modalBox=browser.element("//modal-box");
            var mBoxCloseBtn=modalBox.element("[data-test-id='close-all-views']");
            if(mBoxCloseBtn.isVisible()){
            mBoxCloseBtn.click();
            console.log("clicked on close btn");
            }
            browser.pause(300);
            //close views menu
            var viewsHeader=browser.element("//views-menu");
            var closeViewsMenu=viewsHeader.element("[data-test-id='close-button']");
            if(closeViewsMenu.isVisible()){
                closeViewsMenu.click();
            }
            }
        }
        
    });

    it('To verify whether filter is working accordingly - XYN-1666', function () {
        console.log("Test started to verify filter functionality according to Device name");        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        var selectRowElement = browser.element(parameter.SelectColumn);
        selectRowElement.click();
        browser.pause(300);
        var selectTime = browser.element(parameter.DeviceName);
        selectTime.click();
        tableViewObject.enterTableFilterValue(conf.findTableViewFilterValue1);
        browser.pause(300);
        var j = 1;
        rowCount = browser.elements(parameter.gridRows).value.length;
        console.log(rowCount);
        while (rowCount > 2) {
            var row = browser.elements("//datatable-row-wrapper[" + j +"]");
            var rowContent=row.element("//datatable-body-cell[3]");
            console.log(rowContent.getText(),j);
            expect(rowContent.getText()).to.include(conf.findTableViewFilterValue1);
            j++;
            rowCount--;   
        }
    });

    it('To verify whether filter is working accordingly - XYN-1666', function () {
        console.log("Test started to verify filter functionality according to Plotted metric");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        var selectRowElement = browser.element(parameter.SelectColumn);
        selectRowElement.click();
        browser.pause(1000);
        var selectTime = browser.element(parameter.PlottedMetric);
        selectTime.click();
        tableViewObject.enterTableFilterValue(conf.findTableViewFilterValue2);
        browser.pause(300);
        var j = 1;
        rowCount = browser.elements(parameter.gridRows).value.length;
        console.log(rowCount);
        while (rowCount > 2) {
            var row = browser.elements("//datatable-row-wrapper[" + j +"]");
            var rowContent=row.element("//datatable-body-cell[4]");
            console.log(rowContent.getText(),j);
            expect(rowContent.getText()).to.include(conf.findTableViewFilterValue2);
            j++;
            rowCount--;   
        }
    });
    

    //passing on 8088
    it('Verification of null values rendering on UI - XYN-2815', function(){
        console.log("Test started - XYN-2815 - Verification of null values rendering on UI");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        tableViewObject.enterTableFilterValue(config.dummyValue);
        browser.pause(500);
        baseMethods.verifySpinnerVisibility();
        var errorMsg = browser.element(parameter.noData).isVisible();
        assert(errorMsg, true);
        console.log("Test completed - XYN-2815 -Verification of null values rendering on UI");
    });

    it('Verify Plotted Metric column on table view - XYN-1987', function(){
        console.log("Test started - XYN-1987 - verification of plotted metric RSRP column is added on table view");        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        browser.pause(1000);        
        var av=browser.elements(parameter.tableAppData);
        var headerRow = av.elements(parameter.tableHeaderRow);
        console.log(headerRow.value.length);     
        var headerColumn=browser.elements(parameter.headerColumn).value.length;
        assert.equal(headerColumn,1);
        //plot any event and verify got plotted properly or not
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        browser.pause(300); 
        EventViewTabCnt =  browser.elements(parameter.eventTabCount).value.length;
        console.log(EventViewTabCnt);
        assert.equal(EventViewTabCnt, 1);
        var fileElement = browser.element(parameter.fileElement).getText();       
        assert.equal(fileElement,config.eventName);
        genericObject.clickOnDTTab(parameter.EventsMenu);
        eventsDataObject.clearEventName();
        console.log("Test completed - XYN-1987 - verification of plotted metric RSRP column is added on table view");
    });


    it('Verify Filtering option on table view - XYN-1667', function(){
        console.log("Test started - To Verify Filtering option on table view - XYN-1667");
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            browser.pause(300);
            genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
            browser.pause(300);
            baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
            var selectRowElement = browser.element(parameter.SelectColumn);
            selectRowElement.click();
            browser.pause(1000);
            var selectTime = browser.element(parameter.Time);
            selectTime.click();
            tableViewObject.enterTableFilterValue(conf.findTableViewFilterValue);
            browser.pause(300);
            var j = 1;
            rowCount = browser.elements(parameter.gridRows).value.length;
            console.log(rowCount);
            while (rowCount > 2) {
                var row = browser.elements("//datatable-row-wrapper[" + j +"]");
                var rowContent=row.element("//datatable-body-cell[2]");
                console.log(rowContent.getText(),j);
                expect(rowContent.getText()).to.include(conf.findTableViewFilterValue);
                j++;
                rowCount--;   
            }
            console.log("Test completed - To Verify Filtering option on table view - XYN-1667");
    });

//it('Verify export to excel feature for Table view - XYN-1671', function(){
  //  console.log("Test started - To Verify export to excel feature for Table view - XYN-1671");
  //  baseMethods.clickOnAdvancedSearchViewIcon()
  //  genericObject.clickOnDTTab(parameter.ViewsMenu);
  //  browser.pause(300);
  //  genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
  //  browser.pause(300);
  //  baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
  //  var Export = browser.element(parameter.ExportMenu);
  //  Export.click();
  //  var FileDownload = browser.element(parameter.ExportFile);
  //  FileDownload.click();
//console.log("Test completed - XYN-1987 - verification of plotted metric RSRP column is added on table view");
//});

/*
it('Verify filtering for Views->Table View Lat & Long columns - XYN-1370', function(){
    console.log("Test started - To Verify filtering for Views->Table View Lat & Long columns - XYN-1370");
    baseMethods.clickOnAdvancedSearchViewIcon()
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    browser.pause(300);
    genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
    browser.pause(300);
    baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
    browser.pause(300);
    console.log("element");
    // var tableCellSelector = browser.$(".datatable-header-cell.col-selector");
    // console.log(tableCellSelector);
    // tableCellSelector.click();
    var tableCellSelector=browser.element("//app-data-grid[@name='table-view']//i[@class='icon-16-menu icon-small-size pointer-cursor']");
    tableCellSelector.click();
    browser.pause(200);
    var lattitude = browser.element(parameter.Lattitudecolumn);
    lattitude.click();
    tableViewObject.enterTableFilterValue(config.findTableViewFilterValue);
    var j = 1;
        rowCount = browser.elements("//datatable-row-wrapper").value.length;
        console.log(rowCount);
        while (rowCount > 2) {
            var row = browser.elements("//datatable-row-wrapper[" + j +"]");
            var rowContent=row.element("//datatable-body-cell[2]");
            console.log(rowContent.getText(),j);
            expect(rowContent.getText()).to.include(conf.findTableViewFilterValue);
            j++;
            rowCount--;            
        }
    console.log("Test completed - To Verify filtering for Views->Table View Lat & Long columns - XYN-1370");

});*/

//it('Verify filtering for Views->Table View File name columns - XYN-3113', function(){
    //console.log("Test started - To Verify filtering for Views->Table View File name columns - XYN-3113");
    //baseMethods.clickOnAdvancedSearchViewIcon()
    //genericObject.clickOnDTTab(parameter.ViewsMenu);
    //browser.pause(300);
    //genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
    //browser.pause(300);
    //baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
    //browser.pause(300);
    //var tableCellSelector = browser.$(".datatable-header-cell.col-selector");
    //tableCellSelector.click();
    //browser.pause(200);
    //var lattitude = browser.element(parameter.Lattitudecolumn);
    //lattitude.click();
    //tableViewObject.enterTableFilterValue(config.findTableViewFilterValue);
    //var j = 1;
        //rowCount = browser.elements("//datatable-row-wrapper").value.length;
        //console.log(rowCount);
        //while (rowCount > 2) {
            //var row = browser.elements("//datatable-row-wrapper[" + j +"]");
            //var rowContent=row.element("//datatable-body-cell[2]");
            //console.log(rowContent.getText(),j);
            //expect(rowContent.getText()).to.include(conf.findTableViewFilterValue);
            //j++;
            //rowCount--;            
        //}
    //console.log("Test completed - To Verify filtering for Views->Table View File name columns - XYN-3113");

//});

    it('Verify Full screen feature for Table view - XYN-1670', function(){
        console.log("Verify Full screen feature for Table view - XYN-1670");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        var divElements = browser.elements(parameter.widgetContainer_1);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MapDraggable, parameter.WidgetRightBox);
        var divElements1 = browser.elements(parameter.widgetContainer_2);
        baseMethods.plotMetric(config.metricNameRSRQ, parameter.RSRQMetric, parameter.PlotAcrossWidgets);        
        baseMethods.verifySpinnerVisibility();
        browser.pause(300);
        var styleType = browser.elements(parameter.analyticsview_2).getAttribute(config.style);
        expect(styleType).to.include(config.block);        
        tableViewObject.legendOpenButton();
        var MapLayers = browser.elements(parameter.dataLayers).getText();
        expect(MapLayers[2]).to.include(config.metricNameRSRQ);
        tableViewObject.closeButton();
        tableViewObject.clickFullScreenIcon();
        var newStyleType = browser.elements(parameter.analyticsview_2).getAttribute(config.style);
        expect(newStyleType).to.include(config.none);
        console.log("Verify Full screen feature for Table view - XYN-1670");
    }); 
    it('XYN-1664_To check table views widget', function () {
        console.log("Test started- XYN-1664_To check table views widget");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        var sActaulText=browser.element(parameter.TableHeadText).getText();
        assert.equal(sActaulText,config.tableViewHeadData,sActaulText+" actual text is not same as expected text"+config.tableViewHeadData);
        browser.pause(300);
        var sActaulText1=browser.element(parameter.TableSmallText).getText();
        assert.equal(sActaulText1,config.tableViewData,sActaulText1+" actual text is not same as expected text"+config.tableViewData);
        
        console.log("Test completed - XYN-1664_To check table views widget");
    });   
    it('Verify single event tab created for multiple events on table views - XYN-1672', function(){
        console.log("Started - Verify single event tab created for multiple events on table views - XYN-1672");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        browser.pause(300)
        EventViewTabCnt =  browser.elements(parameter.eventViewTab).value.length;
        assert.equal(EventViewTabCnt, 1);
        var headerRow = browser.elements(parameter.eventViewHeader);
        var TimeColumn = headerRow.elements(parameter.eventHeader).getText();         
        expect(TimeColumn).to.include(parameter.eventHeaderTime);        
        expect(TimeColumn).to.include(parameter.eventHeaderEventName);
        expect(TimeColumn).to.include(parameter.eventHeaderTechnology);
        expect(TimeColumn).to.include(parameter.eventHeaderDeviceName);        
        baseMethods.plotEvent(config.eventName2, parameter.VoiceSetupEvent, parameter.PlotAcrossWidgets);
        browser.pause(300);
        var fileElement = tableViewObject.getfileElement(config.eventName);    
        assert.equal(fileElement,config.eventName);
        fileElement = tableViewObject.getfileElement(config.eventName2);
        assert.equal(fileElement,config.eventName2);
        console.log("Completed - Verify single event tab created for multiple events on table views - XYN-1672");
    });
    it('Verify time column on table view - XYN-1669', function(){
        console.log("Test started - XYN-1669 - verification of time column on table view");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        var headerRow = browser.elements(parameter.tableHeaderRow);
        var headerColumn=headerRow.elements(parameter.timeColumn);
        console.log(headerColumn);
        expect(headerColumn.getText()).to.be.includes(config.tableViewHeaderColumnFilterSearchBy);
        console.log("Test completed - XYN-1669 - verification of time column on table view");
    });

    it.skip('Verify dropdown in table view - XYN-1667', function(){
        console.log("Test started - XYN-1667 - verification of dropdown in table view");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        browser.pause(3000);
        var selectSearchColumn = browser.element("//select[@data-test-id='grid-filter-select']");
        selectSearchColumn.click();
        console.log("click on select column");
        console.log(selectSearchColumn.value.length);
        var selectAColumn=selectSearchColumn.element("//option[@data-test-id='Time']");
        selectAColumn.click();
        var i=1,contentRows=[];
        console.log("no issue");
        var colSelector=browser.elements("//div[@class='datatable-header-cell col-selector']");
        var checkBoxCount=colSelector.elements("//i[@class='icon-menu-1 icon-small-size pointer-cursor']").value.length;
        var j=1,checkBoxSelect,notChecked=0,n=checkBoxCount;
        while(checkBoxCount>1){
            checkBoxSelect=colSelector.element("//i[@class='icon-menu-1 icon-small-size pointer-cursor']["+j+"]");
            console.log("no issue");
            while(n>0){
                headerColumnName[i]= headerRow.element("//datatable-header-cell["+i+"]");
                console.log(headerColumnName[i].getText());
                var checkboxText=checkBoxSelect.getText();
                if(checkboxText==headerColumnName[i].getText()){
                    notChecked=1;
                    itemToCheck=headerColumnName[i];
                    break;
                }
                n--;
                i++;
            }
            n=checkBoxCount;
            j++;
            if(notChecked==0){
                var checkItem=checkBoxSelect.element("//input[@type='checkbox']");
                checkItem.click();
            }
            checkBoxCount--;
        }
        console.log("Test completed - XYN-1667 - verification of dropdown in table view");
    });

    it('Verify that multiple tabs are not getting created when events are plotted - XYN-3506', function(){
        console.log("Test started - Verify that multiple tabs are not getting created when events are plotted - XYN-3506");
        //Table-1 Metric
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        var headerRow = browser.elements(parameter.tableHeaderRow);
        var headerCol=headerRow.elements(parameter.headerColumn);
        console.log(headerCol);
        expect(headerCol.getText()).to.be.includes(config.metricName);
        //Table-2 Event
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.WidgetLeftBox);
        browser.pause(300);        
        baseMethods.plotEvent(config.eventName2, parameter.VoiceSetupEvent, parameter.TableDroppable);
        browser.pause(300);
        //click on any row in metric table and verify
        browser.element(parameter.metricRow).click();
        browser.pause(1000); //wait time must needed.
        var getCneterTexts = browser.element(parameter.unAvailableMsg).getText();
        console.log(getCneterTexts);
        expect(getCneterTexts).to.include(config.unAvailable2SecMsg);                          
        console.log("Test completed - Verify that multiple tabs are not getting created when events are plotted - XYN-3506");
    });
    it('Verify that plotted event name should be displayed under event name column - XYN-3563', function(){
        console.log("Test started - Verify that plotted event name should be displayed under event name column - XYN-3563");        
        //Table Event
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);        
        baseMethods.plotEvent(config.eventName, parameter.VoiceStartEvent, parameter.PlotAcrossWidgets);
        browser.pause(300);
        //verify Event name column
        var headerRow = browser.elements(parameter.eventViewHeader);
        var TimeColumn = headerRow.elements(parameter.eventHeader).getText();         
        expect(TimeColumn).to.include(parameter.eventHeaderTime);
        var fileElement = tableViewObject.getfileElement(config.eventName);    
        assert.equal(fileElement,config.eventName);
        var totRows=browser.elements(parameter.totRows).value.length;
        console.log(totRows);
        var i=1;
        while (i<=totRows){
            var getContent = tableViewObject.getEventElement(i);
            assert.equal(getContent.toUpperCase(), config.eventName.toUpperCase())
            i++;
        }
        console.log("Test completed - Verify that plotted event name should be displayed under event name column - XYN-3563");
    });
    it('Show Metric Units in brackets - XYN-3434', function(){
        console.log("Test started - Show Metric Units in brackets - XYN-3434");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        var divElements = browser.elements(parameter.widgetContainer_1);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TimeseriesDraggable, parameter.WidgetRightBox);
        var divElements1 = browser.elements(parameter.widgetContainer_2);
        baseMethods.plotMetric(config.metricNameRSRQ, parameter.RSRQMetric, parameter.PlotAcrossWidgets);        
        baseMethods.verifySpinnerVisibility();
        browser.pause(300);
        //verify for Table view
        var headerRow = browser.element(parameter.MetricNameInHeader).getText();        
        expect(headerRow).to.include(config.BestCellRSRQUnits);
        //Verify for TimeSeries
        timeseries.clickOnTimeSeriesLayerBtn();
        var plottedLayers = browser.element(parameter.TimeseriesLayerData).isVisible();
        if (plottedLayers){
            var plotMetric = browser.elements(parameter.TimeseriesLayerData).getText();
            expect(plotMetric[1]).to.include(config.BestCellRSRQUnits);
        }
        baseMethods.closeTimeSeriesDataLayer();               
        console.log("Test completed - Show Metric Units in brackets - XYN-3434");
    });
});
