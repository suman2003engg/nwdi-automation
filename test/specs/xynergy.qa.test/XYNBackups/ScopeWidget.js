let baseMethods = require('../../testbase');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
let browseDataObject= require('../../../pageobjects/browseDataPage.js');
let scopeDataObject= require("../../../pageobjects/scopeWidgetPage.js");
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Browse data page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            //browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    
    afterEach(function () {
        if(isElectron()){
            console.log("Test Completed");
        }
    });

    
    it('Verify whether typed in filter is remembered once it is closed/collapsed under data panel - XYN-2085', function(){
        console.log("Test started - Verify whether typed in filter is remembered once it is closed/collapsed under data panel - XYN-2085");        
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);       
        searchdataObject.selectdatasetresult();
        var selectdataset = browser.element(parameter.filteredItem);
        var visibility = selectdataset.isVisible();
        assert(visibility, true);
        searchdataObject.clickOpeninCanvasbutton();
        browser.pause(1000);
        baseMethods.openInCanvasValidation();
        var scopeTab=browser.element(parameter.scopeWidget);
        if(!(scopeTab.getAttribute("class")).includes('selected')){
            scopeTab.click();
        }
        scopeDataObject.scopeFilter(config.searchItemdataset);
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.scopeWidget).click();
        var scopefilter = browser.element(parameter.scopeFilterClear);
        var visibility1 = scopefilter.isVisible();
        assert(visibility1, true); 
        browser.element(parameter.scopeFilterClear).click();
        browser.element(parameter.incorrectQueryClose).click();
        console.log("Test Completed - Verify whether typed in filter is remembered once it is closed/collapsed under data panel - XYN-2085");
    });

    it('Verify if an error message is displayed if none of mandatory fields are checked and not closed until mandatory fields are checked in Data panel - XYN-2844', function(){
        console.log("Test started - Verify if an error message is displayed if none of mandatory fields are checked and not closed until mandatory fields are checked in Data panel - XYN-2844");
        browser.element(parameter.DataMenu).click();
        baseMethods.SelectTabs("SEARCHDATA");
        searchdataObject.newSearchPage();        
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);       
        searchdataObject.selectdatasetresult();
        searchdataObject.clickOpeninCanvasbutton();
        browser.pause(1000);
        baseMethods.openInCanvasValidation();
        var scopeTab = browser.element(parameter.scopeWidget);
        if(!(scopeTab.getAttribute("class")).includes('selected')){
            scopeTab.click();
        }
        browser.pause(200);
        var checkBox = browser.element(parameter.scopeCheckBox);
        if(!(checkBox.getAttribute("class")).includes('check-img checkmarkred')){
            checkBox.click();
        }
        var validationTitle = browser.element(parameter.validationErrorTitle);
        browser.pause(300);
        var visibility1 = validationTitle.isVisible();
        assert(visibility1, true);
        if(!(checkBox.getAttribute("class")).includes('check-img checkmarkred')){
            checkBox.click();
        }
        //browser.element(parameter.incorrectQueryClose).click();
        console.log("Test Completed - Verify if an error message is displayed if none of mandatory fields are checked and not closed until mandatory fields are checked in Data panel - XYN-2844");

    });

    it('To verify whether only datasets and files is opened and rest is collapsed in search data/import data - XYN-2041', function(){
        console.log("Test started - To verify whether only datasets and files is opened and rest is collapsed in search data/import data - XYN-2041");
        browser.element(parameter.DataMenu).click();
        baseMethods.SelectTabs("SEARCHDATA");
        searchdataObject.newSearchPage();        
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);       
        searchdataObject.selectdatasetresult();
        searchdataObject.clickOpeninCanvasbutton();
        browser.pause(500);
        baseMethods.openInCanvasValidation();
        var scopeTab = browser.element(parameter.scopeWidget);
        if(!(scopeTab.getAttribute("class")).includes('selected')){
            scopeTab.click();
        }
        var scopeDataFilter = browser.element(parameter.treeNodeDropDown);        
        var visibility2 = scopeDataFilter.isEnabled();
        assert(visibility2, true);       
        var scopeDataSet = browser.element(parameter.scopeDatasetDropDown);        
        var visibility3 = scopeDataSet.isEnabled();
        assert(visibility3, true);        
        var scopeFile = browser.element(parameter.scopeFileDropDown);        
        var visibility4 = scopeFile.isEnabled();
        assert(visibility4, true);        
        var scopeDevice = browser.element(parameter.scopeDeviceDropDownCollapsed);        
        var visibility5 = scopeDevice.isEnabled();
        assert(visibility5, true);        
        var scopeTechnology = browser.element(parameter.scopeTechnologyDropDownCollapsed);        
        var visibility6 = scopeTechnology.isEnabled();
        assert(visibility6, true);       
        var scopePlotSetting = browser.element(parameter.treeNodeDropDown);        
        var visibility7 = scopePlotSetting.isEnabled();
        assert(visibility7, true);        
        var scopeDevice1 = browser.element(parameter.scopeDeviceDropDownCollapsed);
        if(!(scopeDevice1.getAttribute("class")).includes('expanded')){
             scopeDevice1.click();
        }        
        browser.element(parameter.DataMenu).click();
        browser.element(parameter.AVMenu).click();        
        var scopeDeviceExpanded = browser.element(parameter.scopeDeviceDropDownExpanded);        
        var visibility8 = scopeDeviceExpanded.isEnabled();
        assert(visibility8, true);        
        browser.element(parameter.incorrectQueryClose).click();
        console.log("Test Completed - To verify whether only datasets and files is opened and rest is collapsed in search data/import data - XYN-2041");
    
    });

    it('Data Scope - Verify formatting issue on information for all sections - XYN-2068', function(){
        console.log("Test started - Data Scope - Verify formatting issue on information for all sections - XYN-2068");
        browser.element(parameter.DataMenu).click();
        baseMethods.SelectTabs("SEARCHDATA");
        searchdataObject.newSearchPage();        
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);        
        searchdataObject.selectdatasetresult();
        searchdataObject.clickOpeninCanvasbutton();
        browser.pause(500);
        baseMethods.openInCanvasValidation();
        var scopeTab = browser.element(parameter.scopeWidget);
        if(!(scopeTab.getAttribute("class")).includes('selected')){
            scopeTab.click();
        }
        var scopeDataFilter = browser.element(parameter.treeNodeDropDown);
        browser.pause(1000);
        var checkBox = browser.element(parameter.scopeCheckBox);
        var scopeFilter = browser.element(parameter.scopeFilter);
        if(!(checkBox.getAttribute("class")).includes('check-img checkmarkred')){
            scopeFilter.click();
        }
        var value = 'datasets';
        scopeFilter.keys(value);
        console.log("Datasets");
        var scopeDataSet = browser.element(parameter.scopeDatasetDropDown);        
        var visibility = scopeDataSet.isEnabled();
        assert(visibility, true);
        var scopeFilterClear = browser.element(parameter.scopeFilterClear);
        if(!(checkBox.getAttribute("class")).includes('check-img checkmarkred')){
            scopeFilterClear.click();
        }
        browser.element(parameter.incorrectQueryClose).click();
        console.log("Test Completed - Data Scope - Verify formatting issue on information for all sections - XYN-2068");
    
    });

    it('Verify Data panel styling - XYN-2312', function(){
        console.log("Test Started - Verify Data panel styling - XYN-2312");
        browser.element(parameter.DataMenu).click();
        baseMethods.SelectTabs("SEARCHDATA");
        searchdataObject.newSearchPage();        
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);        
        searchdataObject.selectdatasetresult();
        searchdataObject.clickOpeninCanvasbutton();
        browser.pause(500);
        baseMethods.openInCanvasValidation();
        var scopeTab = browser.element(parameter.scopeWidget);
        if(!(scopeTab.getAttribute("class")).includes('selected')){
            scopeTab.click();
        }        
        browser.pause(500);
        browser.element("[data-test-id='Datasets']").click();
        var validationToolTip = browser.element(parameter.validationErrorTitle);
        browser.pause(300);
        var visibility = validationToolTip.isVisible();
        assert(visibility, true);
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.scopeWidget).click();        
        var validationToolTip1 = browser.element(parameter.validationErrorTitle);
        browser.pause(300);
        var visibility1 = validationToolTip1.isVisible();
        assert(visibility1, true);
        browser.element("[data-test-id='Files']").click();
        browser.element("[data-test-id='Files']").click();    
        var validationToolTip2 = browser.element(parameter.validationErrorTitle);
        browser.pause(300);        
        var checkToolTip = true;
        if(validationToolTip2.isVisible()){
            checkToolTip = false;
        }
        assert(checkToolTip, true);
        browser.element(parameter.incorrectQueryClose).click();       
        console.log("Test Completed - Verify Data panel styling - XYN-2312");        
    
    });

    it('Verify Landing page & Analytics View redesign - XYN-2046', function(){
        console.log("Test Started - Verify Landing page & Analytics View redesign - XYN-2046");
        browser.element(parameter.DataMenu).click();
        var importData = browser.element("[data-test-id='Import Data']");        
        var checkImportData = true;
        if(importData.isVisible()){
            checkImportData = true;
        }
        assert(checkImportData, false);
        var searchData = browser.element("[data-test-id='Search Data']");        
        var checkSearchData = true;
        if(searchData.isVisible()){
            checkSearchData = true;
        }
        assert(checkSearchData, false);
        var browseData = browser.element("[data-test-id='Browse Data']");        
        var checkBrowseData = true;
        if(browseData.isVisible()){
            checkBrowseData = true;
        }
        assert(checkBrowseData, false);
        browser.element(parameter.AVMenu).click();
        browser.pause(5000);
        var scope = browser.element(parameter.scope);        
        var checkScope = true;
        if(scope.isVisible()){
            checkScope = true;
        }
        assert(checkScope, false);
        var view = browser.element(parameter.ViewsMenu);       
        var checkView = true;
        if(view.isVisible()){
            checkView = true;
        }
        assert(checkView, false);
        var metric = browser.element(parameter.MetricsMenu);        
        var checkMetric = true;
        if(metric.isVisible()){
            checkMetric = true;
        }
        assert(checkMetric, false);
        var event = browser.element(parameter.EventsMenu);        
        var checkEvent = true;
        if(event.isVisible()){
            checkEvent = true;
        }
        assert(checkEvent, false);
        var workspace = browser.element(parameter.workspacesMenu);        
        var checkWorkspace = true;
        if(workspace.isVisible()){
            checkWorkspace = true;
        }
        assert(checkWorkspace, false);
        //browser.element(parameter.incorrectQueryClose).click();    
        browser.pause(200);   
        var menuBarExpander = browser.element("//analytics-view//div[@data-test-id='vertical-menu-item']//div[@class='bottom-expander']");
        menuBarExpander.click();
        var menuBarExpander = browser.element("//analytics-view//div[@data-test-id='vertical-menu-item']//div[@class='bottom-expander']");
        menuBarExpander.click();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.MapDraggable, parameter.FreshViewsDroppable);
        var scopeTab = browser.element(parameter.scopeWidget);
        if((scopeTab.getAttribute("class")).includes('selected')){
        scopeTab.click();
    } 
        browser.element("[data-test-id='Mapclose-btn']").click();                      
        console.log("Test Completed - Verify Landing page & Analytics View redesign - XYN-2046");

    });

});