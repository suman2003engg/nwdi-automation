let baseMethods = require('../../../testbase.js');
let genericObject = require('../../../generic_objects.js');
let parameter=require('../../../parameters.js');
var assert = require('assert');

describe('views', function () {
    beforeEach(function () {
        browser.url('/account/login');
        browser.setValue('input[name="username"]', 'demotest', 100);
        browser.setValue('input[name="password"]', 'Xceed123', 100);
        var submitButton = browser.element("//button[@type='submit']");
        submitButton.click();
        //baseMethods.verifySpinnerVisibility();
        browser.pause(2000);
        browser.windowHandleMaximize();
        browser.waitForExist("[data-test-id='Analytics View']");
    });

    describe('messges as selected widget', function () {
        
        it('should verify user is able to click on views tab', function () {
            console.log("should verify user is able to click on views tab-started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            var value= browser.element("[data-test-id='WidgetUniqueIdXynergyMapComponent']").isExisting();
            assert.equal(value,true);
            console.log("should verify user is able to click on views tab-done");
        });

        it('should verify layer3 message data table', function () {
            console.log("should verify layer3 message data table-started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.MapDraggable,parameter.WidgetRoot);
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.MessagesDraggable,parameter.WidgetRightBox);
            var value=baseMethods.verifyData();
            assert.equal(value, true);
            console.log("should verify layer3 message data table-done");
        });
        
        it('should verify find option of messages view', function () {
            console.log("should verify find option of messages view-started");
            baseMethods.clickOnAdvancedSearchViewIcon();
            genericObject.clickOnDTTab(parameter.ViewsMenu);
            baseMethods.dragAndDrop(parameter.MessagesDraggable,parameter.WidgetRightBox);
            var value=baseMethods.verifyData();
            assert.equal(value, true);
            console.log("should verify find option of messages view-done");
        });
    });
});