let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Parameter Comparison', function () {
    var init=0;
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        }
    });
    it('Verify that Settings is renamed as Viewing Options on Parameter Comparison - XYN-2896', function () {
        console.log("Test started - Verify that Settings is renamed as Viewing Options on Parameter Comparison - XYN-2896");
        //First test <6 dataset which will not pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForPComparison5GTest);
        baseMethods.openInCanvasValidation();      
        baseMethods.verifySpinnerVisibility();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.verifySpinnerVisibility();
        var Option=browser.elements("//span[contains(text(),'"+config.viewOption+"')]").value.length;
        assert.equal(Option,config.compareValueWithOne);
        console.log("Test completed - Verify that Settings is renamed as Viewing Options on Parameter Comparison - XYN-2896");
    });
});