let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Parameter Comparison', function () {
    var init=0;
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        }
    });
    it('Comparison View - Technology Selection - XYN-2788 and Header Enhancement - XYN-2786', function () {
        console.log("Test started - Comparison View - Technology Selection - XYN-2788 and Header Enhancement - XYN-2786");
        //First test >6 and LTE&5G technolgies which will pop-up model box for PComparison
        baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForPComparison5GLTETest);
        baseMethods.openInCanvasValidation();
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);      
        baseMethods.verifySpinnerVisibility();        
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened
        if (mboxOpened){
            //click 3 parameters to compare
            parameterComparisonObject.selectAllCheckBox();
            parameterComparisonObject.clickOKBtn();
            baseMethods.verifySpinnerVisibility();
        }
        parameterComparisonObject.clickSelectCellsTab();
        var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();
        //if modelbox opened,
        var i=1;
        var ftechLTE=0;
        var ftech5G=0;
        var result = config.selectCells1;
        if (mboxOpened){
            //1. verify all columns headers are as expected.
            var j=2;
            while(j<=6){
                var hCell = parameterComparisonObject.headerItems(parameter.headerItem + "[" + j + "]");
                console.log(config.selectCells[j-2])
                assert.equal(hCell, config.selectCells[j-2]);
                j++;
            }
            //2. verify technologies under system column
            do{ //loop should be less than 6 cells dataset only
                var techVal = parameterComparisonObject.technologyType(i);
                console.log(techVal);
                if (ftechLTE == 0){
                    var newtechType1 = config.findMessageFilterValue3.toUpperCase();
                    if(techVal == newtechType1){
                        ftechLTE = 1; //found LTE tech from list
                    }
                }else
                if (ftech5G == 0){
                    var newtechType2 = config.techType2.toUpperCase();
                    if(techVal == newtechType2){
                        ftech5G = 1; //found LTE tech from list
                    }
                }
                if ((ftechLTE == 1) && (ftech5G == 1)){
                    i=0;
                }
                else{
                    i++;
                }         
            }
            while(i!=0);
            assert.equal(ftechLTE, 1);
            assert.equal(ftech5G, 1);
            parameterComparisonObject.clickOKBtn();                                                        
        }                
        console.log("Test completed - Comparison View - Technology Selection - XYN-2788 and Header Enhancement - XYN-2786");
    });
});