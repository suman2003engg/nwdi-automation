let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var parameterComparisonObject=require('../../../pageobjects/parameterComparisonPage.js');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Parameter Comparison', function () {
    var init=0;
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
        }
    });
it('Verify if user changes the cell selection then user should be prompted to change baseline cell accordingly - XYN-2817', function () {
    console.log("Test started - Verify if user changes the cell selection then user should be prompted to change baseline cell accordingly - XYN-2817");
    //First test <6 dataset which will not pop-up model box for PComparison
    baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
    baseMethods.openInCanvasValidation();      
    baseMethods.verifySpinnerVisibility();
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    browser.pause(300);
    genericObject.dragAndDropViews(parameter.parameterComparisonDraggable, parameter.FreshViewsDroppable);
    baseMethods.verifySpinnerVisibility();        
    //1. By default, when you select a cell then it will be the only baseline so  dialog will not pop-up for baseline select
    var mboxOpened = browser.element(parameter.mBoxTitleId).isVisible();  
    if (mboxOpened){
        for (var i=1; i<4; i++){
            parameterComparisonObject.clickCheckBox(i);
        }            
        parameterComparisonObject.clickOKBtn();
        baseMethods.verifySpinnerVisibility();
    } 
    //verify default 'No baseline' has been selected
    parameterComparisonObject.clickSelectBaselineTab();
    var getTotCells = browser.elements(parameter.totRowsCount).value.length;
    console.log(getTotCells);
    assert(getTotCells > 1);
    var getCellName = parameterComparisonObject.getBaselineCellName(1);
    console.log(getCellName);
    assert.equal(config.noBaseline, getCellName);
    //2. select a different baseline, by default 1st radio button has been checked(No baseline), so select next one
    parameterComparisonObject.selectRdBtnBaseline(2);        
    //get the above baseline cell name to uncheck it from the select cells
    getCellName = parameterComparisonObject.getBaselineCellName(2);
    console.log(getCellName);
    parameterComparisonObject.clickOKBtnBaseline();        
    //3. now unselect cell from select cells tab and expect the baseline tab to get open
    parameterComparisonObject.clickSelectCellsTab();
    getTotCells = browser.elements(parameter.totRowsCount).value.length;
    console.log(getTotCells);
    if(getTotCells > 1){
        var j = 1;
        while(j <= getTotCells){
            var rowVal = parameterComparisonObject.getRowVal(j);
            if (expect(rowVal).to.include(getCellName)){                    
                //also unselect the cell and click ok
                parameterComparisonObject.unSelectCheckbox(j);
                parameterComparisonObject.clickOKBtn();
                baseMethods.verifySpinnerVisibility();
                j = getTotCells+1;
            }
        }
    }        
    browser.pause(500);
    assert.equal(browser.element(parameter.mBoxTitleId).isVisible(), true);
    parameterComparisonObject.clickOKBtnBaseline();
    //4. Now open select cell and check one more check box, no Baseline dialog should pop-up
    parameterComparisonObject.clickSelectCellsTab();
    var newI = i+1;
    parameterComparisonObject.clickCheckBox(newI);                    
    parameterComparisonObject.clickOKBtn();
    baseMethods.verifySpinnerVisibility();
    browser.pause(500);
    assert.equal(browser.element(parameter.mBoxTitleId).isVisible(), false);
    console.log("Test completed - Verify if user changes the cell selection then user should be prompted to change baseline cell accordingly - XYN-2817");
});
});