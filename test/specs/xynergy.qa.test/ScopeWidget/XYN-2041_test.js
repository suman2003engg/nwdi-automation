let baseMethods = require('../../testbase');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
let browseDataObject= require('../../../pageobjects/browseDataPage.js');
let scopeDataObject= require("../../../pageobjects/scopeWidgetPage.js");
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Browse data page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            //browser.waitForExist("[data-test-id='Analytics View']");
        }
    });

    it('To verify whether only datasets and files is opened and rest is collapsed in search data/import data - XYN-2041', function(){
        console.log("Test started - To verify whether only datasets and files is opened and rest is collapsed in search data/import data - XYN-2041");
        browser.element(parameter.DataMenu).click();
        baseMethods.SelectTabs("SEARCHDATA");
        searchdataObject.newSearchPage();        
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);       
        searchdataObject.selectdatasetresult();
        searchdataObject.clickOpeninCanvasbutton();
        browser.pause(500);
        baseMethods.openInCanvasValidation();
        var scopeTab = browser.element(parameter.scopeWidget);
        if(!(scopeTab.getAttribute("class")).includes('selected')){
            scopeTab.click();
        }
        var scopeDataFilter = browser.element(parameter.treeNodeDropDown);        
        var visibility2 = scopeDataFilter.isEnabled();
        assert(visibility2, true);       
        var scopeDataSet = browser.element(parameter.scopeDatasetDropDown);        
        var visibility3 = scopeDataSet.isEnabled();
        assert(visibility3, true);        
        var scopeFile = browser.element(parameter.scopeFileDropDown);        
        var visibility4 = scopeFile.isEnabled();
        assert(visibility4, true);        
        var scopeDevice = browser.element(parameter.scopeDeviceDropDownCollapsed);        
        var visibility5 = scopeDevice.isEnabled();
        assert(visibility5, true);        
        var scopeTechnology = browser.element(parameter.scopeTechnologyDropDownCollapsed);        
        var visibility6 = scopeTechnology.isEnabled();
        assert(visibility6, true);       
        var scopePlotSetting = browser.element(parameter.treeNodeDropDown);        
        var visibility7 = scopePlotSetting.isEnabled();
        assert(visibility7, true);        
        var scopeDevice1 = browser.element(parameter.scopeDeviceDropDownCollapsed);
        if(!(scopeDevice1.getAttribute("class")).includes('expanded')){
             scopeDevice1.click();
        }        
        browser.element(parameter.DataMenu).click();
        browser.element(parameter.AVMenu).click();        
        var scopeDeviceExpanded = browser.element(parameter.scopeDeviceDropDownExpanded);        
        var visibility8 = scopeDeviceExpanded.isEnabled();
        assert(visibility8, true);        
        browser.element(parameter.incorrectQueryClose).click();
        console.log("Test Completed - To verify whether only datasets and files is opened and rest is collapsed in search data/import data - XYN-2041");
    
    });

});