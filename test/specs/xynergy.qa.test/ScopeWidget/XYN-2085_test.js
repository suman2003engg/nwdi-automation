let baseMethods = require('../../testbase');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
let browseDataObject= require('../../../pageobjects/browseDataPage.js');
let scopeDataObject= require("../../../pageobjects/scopeWidgetPage.js");
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Browse data page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            //browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    
    afterEach(function () {
        if(isElectron()){
            console.log("Test Completed");
        }
    });

    
    it('Verify whether typed in filter is remembered once it is closed/collapsed under data panel - XYN-2085', function(){
        console.log("Test started - Verify whether typed in filter is remembered once it is closed/collapsed under data panel - XYN-2085");        
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);       
        searchdataObject.selectdatasetresult();
        var selectdataset = browser.element(parameter.filteredItem);
        var visibility = selectdataset.isVisible();
        assert(visibility, true);
        searchdataObject.clickOpeninCanvasbutton();
        browser.pause(1000);
        baseMethods.openInCanvasValidation();
        var scopeTab=browser.element(parameter.scopeWidget);
        if(!(scopeTab.getAttribute("class")).includes('selected')){
            scopeTab.click();
        }
        scopeDataObject.scopeFilter(config.searchItemdataset);
        browser.element(parameter.incorrectQueryClose).click();
        browser.element(parameter.scopeWidget).click();
        var scopefilter = browser.element(parameter.scopeFilterClear);
        var visibility1 = scopefilter.isVisible();
        assert(visibility1, true); 
        browser.element(parameter.scopeFilterClear).click();
        browser.element(parameter.incorrectQueryClose).click();
        console.log("Test Completed - Verify whether typed in filter is remembered once it is closed/collapsed under data panel - XYN-2085");
    });

});