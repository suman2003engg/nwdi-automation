let baseMethods = require('../../testbase');
let genericObject = require('../../generic_objects.js');
let parameter = require('../../parameters.js');
let browseDataObject= require('../../../pageobjects/browseDataPage.js');
let scopeDataObject= require("../../../pageobjects/scopeWidgetPage.js");
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
let config = require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('Browse data page', function () {
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            //browser.waitForExist("[data-test-id='Analytics View']");
        }
    });
    
    afterEach(function () {
        if(isElectron()){
            console.log("Test Completed");
        }
    });

    it('Data Scope - Verify formatting issue on information for all sections - XYN-2068', function(){
        console.log("Test started - Data Scope - Verify formatting issue on information for all sections - XYN-2068");
        browser.element(parameter.DataMenu).click();
        baseMethods.SelectTabs("SEARCHDATA");
        searchdataObject.newSearchPage();        
        baseMethods.searchNQLQueryCustom(config.searchItemdataset,config.searchCondition,config.searchDatasetForRegularTest);        
        searchdataObject.selectdatasetresult();
        searchdataObject.clickOpeninCanvasbutton();
        browser.pause(500);
        baseMethods.openInCanvasValidation();
        var scopeTab = browser.element(parameter.scopeWidget);
        if(!(scopeTab.getAttribute("class")).includes('selected')){
            scopeTab.click();
        }
        var scopeDataFilter = browser.element(parameter.treeNodeDropDown);
        browser.pause(1000);
        var checkBox = browser.element(parameter.scopeCheckBox);
        var scopeFilter = browser.element(parameter.scopeFilter);
        if(!(checkBox.getAttribute("class")).includes('check-img checkmarkred')){
            scopeFilter.click();
        }
        var value = 'datasets';
        scopeFilter.keys(value);
        console.log("Datasets");
        var scopeDataSet = browser.element(parameter.scopeDatasetDropDown);        
        var visibility = scopeDataSet.isEnabled();
        assert(visibility, true);
        var scopeFilterClear = browser.element(parameter.scopeFilterClear);
        if(!(checkBox.getAttribute("class")).includes('check-img checkmarkred')){
            scopeFilterClear.click();
        }
        browser.element(parameter.incorrectQueryClose).click();
        console.log("Test Completed - Data Scope - Verify formatting issue on information for all sections - XYN-2068");
    
    });

});