var dragAndDrop = require('html-dnd').codeForSelectors;
let baseMethods = require('./testbase.js');
let xynergyconstants = require('./test_xynergy.constants.js');
let parameter = require('./parameters.js');
let genericObject = function(){

    this.dragAndDropViews=function(draggable,droppable)
    {
        browser.execute(dragAndDrop, draggable, droppable);
    }

    this.clickOnDTTab=function(DTTab){
        browser.element(DTTab).click();
        browser.pause(1000);
    }
    
    this.setEventName=function(event_name){
        var clearEventBtn=browser.element("[data-test-id='clear-event-name']");
        if(clearEventBtn.isVisible()){
            clearEventBtn.click();
        }
        var eventTab = browser.element("[data-test-id='Events-search-box']");
        eventTab.click();
        eventTab.setValue(event_name);
        browser.pause(400);
    }

    this.setMetricName=function(metric_name){
        var clearMetricBtn=browser.element("[data-test-id='clear-metric-name']");
        if(clearMetricBtn.isVisible()){
            clearMetricBtn.click();
        }
        var metricsTab = browser.element("[data-test-id='Metrics-search-box']");
        metricsTab.setValue(metric_name);
        browser.pause(400);
    }
};
module.exports=new genericObject();