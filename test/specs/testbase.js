var dragAndDrop = require('html-dnd').codeForSelectors;
var genericObject = require("./../pageobjects/genericobjects");
var xynergyConstants = require("./test_xynergy.constants.js");
let parameter = require("./parameters.js");
var config = require('../config.js');
var metricAndEventObject = require("./../pageobjects/metricsAndEventsDataPage");
var browseDataObject = require("./../pageobjects/browseDataPage");
let baseMethods = {};

baseMethods.plotMetric = function (metricName, dragabbleElement, droppableArea) {
    genericObject.clickOnDTTab(parameter.MetricsMenu);
    baseMethods.verifySpinnerVisibility();
    genericObject.setMetricName(metricName);
    baseMethods.dragAndDrop(dragabbleElement, droppableArea);
}

baseMethods.plotEvent = function (eventName, dragabbleElement, droppableArea) {
    genericObject.clickOnDTTab(parameter.EventsMenu);
    baseMethods.verifySpinnerVisibility();
    genericObject.setEventName(eventName);
    browser.waitForVisible(dragabbleElement);
    baseMethods.dragAndDrop(dragabbleElement, droppableArea);
}

baseMethods.plotUnAvailableEvent = function (eventName, dragabbleElement, droppableArea) {
    genericObject.clickOnDTTab(parameter.EventsMenu);
    baseMethods.verifySpinnerVisibility();
    genericObject.setEventName(eventName);
    browser.waitForVisible(dragabbleElement);
    if (browser.element(parameter.disabledEventOrMetric).isVisible()){
        baseMethods.dragAndDrop(dragabbleElement, droppableArea);
    }    
}

baseMethods.dragAndDrop = function (dragabbleElement, droppableElement) {
    genericObject.dragAndDropViews(dragabbleElement, droppableElement);
    browser.pause(2500);
};

baseMethods.verifyTextInDecodedMessagesGrid = function (messagename) {
    var flag = true;
    var decodedMessage = browser.elements("//*[@name='messages-search-grid']//datatable-scroller[@class='datatable-scroll']/datatable-row-wrapper");
    var decodedMessagelength = decodedMessage.value.length;
    //console.log(decodedMessagelength)
    for (var i = 1; i < decodedMessagelength; i++) {
        //var t="//div[@class='findall-result-container']//datatable-scroller[@class='datatable-scroll']/datatable-row-wrapper["+i+"]//datatable-body-cell[5]//span"
        var decodedMessageGrigRow = "//*[@name='messages-search-grid']//datatable-scroller[@class='datatable-scroll']/datatable-row-wrapper[" + i + "]"
        var textValue = browser.element(decodedMessageGrigRow);
        var position = textValue.getText().toUpperCase().search(messagename.toUpperCase());
        if (position > 0) {
            flag = true;
        } else {
            flag = false;
            break;
        }
    }
    return flag;
};

baseMethods.selectTextInDecodedMessagesGrid = function (messagename) {
    var sMessage = "";
    var decodedMessage = browser.elements("//div[@class='findall-result-container']//datatable-scroller[@class='datatable-scroll']/datatable-row-wrapper");
    var decodedMessagelength = decodedMessage.value.length;
    //console.log(decodedMessagelength)
    for (var i = 1; i < decodedMessagelength; i++) {
        var decodedMessageGrigRow = "//div[@class='findall-result-container']//datatable-scroller[@class='datatable-scroll']/datatable-row-wrapper[" + i + "]"
        var textValue = browser.element(decodedMessageGrigRow);
        var position = textValue.getText().toUpperCase().search(messagename.toUpperCase());
        //var position = textValue.getText().search(messagename.trim());
        if (position > 0) {
            textValue.click();
            sMessage = textValue.getText();
            flag = true;
            break;
        } else {
            flag = false;

        }
    }
    if (flag = true) {
        return sMessage;
    } else {
        return false;
    }


};

baseMethods.VerifyActiveRowCellValue = function (messageName) {
    var ExpectedMeaage = baseMethods.selectTextInDecodedMessagesGrid(messageName)
    browser.pause(5000);
    //var message = browser.element("//*[@name='messages-search-grid']//datatable-body-row[contains(@class,'active')]//datatable-body-cell[5]");
    // var actualMessage = message.getText();
    var message = "//div[@class='top-section reduced-height']//datatable-scroller[@class='datatable-scroll']/datatable-row-wrapper/datatable-body-row[contains(@class,'active')]//datatable-body-cell[5]"
    var actualMessage = browser.element(message).getText();
    var position = ExpectedMeaage.toUpperCase().search(actualMessage.toUpperCase());
    if (position > 0) {
        return true;
    } else {
        return false;
    }
};
baseMethods.verifyFindNextPopupMessage = function (messageName) {
    baseMethods.verifySpinnerVisibility();
    browser.pause(1000);
    var message = "//div[@class='modal-message-content']/span"
    var actualMessage = browser.element(message).getText();
    //var position = messageName.toUpperCase().e .search(actualMessage.toUpperCase());
    if (messageName.toUpperCase() == actualMessage.toUpperCase()) {
        browser.element("//button[@class='action-button button-small-size']").click();
        return true;
    } else {
        return false;
    }

};
//................NOT TO BE TOUCHED........................//

baseMethods.loginToApplication = function () {
    //browser.url('/account/login');
    browser.url('/');
    browser.setValue('input[name="username"]', 'demotest', 100);
    browser.setValue('input[name="password"]', 'Xceed123', 100);
    var submitButton = browser.element("//button[@type='submit']");
    submitButton.click();
    //baseMethods.verifySpinnerVisibility();
    browser.windowHandleMaximize();
    browser.waitForExist("[data-test-id='Analytics View']");
    //browser.url('/app/grid-prototype');

}

baseMethods.clickOnUserInfo = function () {
    var userinfo = browser.element("//div[@class='user-info']");
    userinfo.element("//div[@class='profile-header pointer-cursor']").click();
    browser.pause(2000);
}

baseMethods.tableViewColumnHamBurger = function () {
    var tableView = browser.element("[class='table-view relative-component']");
    var xynTableContainer = tableView.element("[class='xyn-table-container']");
    var tableContainer = xynTableContainer.element("[class='table-container']");
    var tablePane = tableContainer.element("[class='table-pane']");
    var dataTableHeader = tablePane.element("[class='datatable-header-inner']");
    var dataTableRow = dataTableHeader.element("[class='datatable-row-center']");
    var dataTableHeaderCell = dataTableRow.element("[class='datatable-header-cell']");
    var dataTableHeaderCellTemplate = dataTableHeaderCell.element("[class='datatable-header-cell-template-wrap']");
    //var hamBurger = dataTableHeaderCellTemplate.element("[data-test-id='export-to-excel-btn']").click();
    dataTableHeaderCellTemplate.element("[data-test-id='1SelectRow-Btn']").click();
}

baseMethods.searchNQLQueryForSingleDevice = function (device, deviceValue) {
    var newQueryTab = browser.element("[data-test-id='NQLquery-text-area']");
    newQueryTab.click();
    newQueryTab.keys("FileName contains ");
    var braces = browser.element("//span[@class='cm-string']");
    braces.click();
    braces.keys("822");
    var searchButton = browser.element("[data-test-id='searchButton']");
    searchButton.click();
    //baseMethods.verifySpinnerVisibility();
    browser.pause(300);
}

baseMethods.advancedSearchViewForSingleDeviceData = function () {
    baseMethods.searchNQLQuery();
    var datasetItem = browser.element("[data-test-id='listItem']");
    datasetItem.click();
    //baseMethods.verifySpinnerVisibility();
    browser.pause(100);
    var viewIcon = browser.element("[data-test-id='viewsicon']");
    viewIcon.click();
    browser.pause(2000);
}
baseMethods.advancedSearchViewForMultiDeviceData = function () {
    baseMethods.searchNQLQueryForSingleDevice("FileName contains", "822");
    var datasetItem = browser.element("[data-test-id='listItem']");
    datasetItem.click();
    //baseMethods.verifySpinnerVisibility();
    browser.pause(100);
    var viewIcon = browser.element("[data-test-id='viewsicon']");
    viewIcon.click();
    browser.pause(2000);
}
baseMethods.verifyTimeseriesGraphData = function (layerName) {
    var maplayerTabs = browser.elements("[data-highcharts-chart='0']");
    var drivetestTab = maplayerTabs.value;
    var value = false;
    drivetestTab.forEach(element => {
        var textValue = element.getText();
        const text = new RegExp(layerName + '*');
        if (text.test(textValue)) {
            value = true;
        }
    });
    return value;
}

baseMethods.verifyDTMenuHeader = function (headerName) {
    browser.waitForVisible("[data-test-id='left-tab-menu-header']", 10000);
    var tabHeader = browser.elements("[data-test-id='left-tab-menu-header']");
    var menuTab = tabHeader.value;
    var assertionvalue = false;
    menuTab.forEach(element => {
        var text = element.getText();
        if (text == headerName) {
            assertionvalue = true;
        }
    });
    return assertionvalue;
}

baseMethods.verifyRightMenuHeader = function (headerName) {
    var tabHeader = browser.elements("[data-test-id='right-tab-menu-header']");
    var menuTab = tabHeader.value;
    var assertionvalue = false;
    menuTab.forEach(element => {
        var text = element.getText();
        if (text == headerName) {
            assertionvalue = true;
        }
    });
    return assertionvalue;
}

baseMethods.clickOnLayerMenuBtn = function () {
    browser.waitForVisible("[data-test-id='IconUniqueDataIdLegend']", 100);
    var mapMenu = browser.element("[data-test-id='IconUniqueDataIdLegend']");
    mapMenu.click();
    browser.waitForVisible("[data-test-id='legend-data']", 100);
}

baseMethods.verifyLayerData = function (layerName) {
    var maplayerTabs = browser.elements("[data-test-id='legend-data']");
    var drivetestTab = maplayerTabs.value;
    var value = false; //0
    drivetestTab.forEach(element => {
        var textValue = element.getText();
        const text = new RegExp(layerName + '*');
        if (text.test(textValue)) {
            value = true; //value+1
        }
    });
    return value;
}

baseMethods.verifySpinnerVisibility = function () {
    var spinner = true;
    spinnerExists = browser.isExisting("[data-test-id='spinner']");
    spinnerVisible = browser.isVisible("[data-test-id='spinner']");
    if (spinnerExists == true && spinnerVisible == true) {
        while (spinner == true) {
            spinner = browser.isVisible("[data-test-id='spinner']");
        }
    }
    else
        if (spinnerExists == false || spinnerVisible == false) {
            spinner = false;
        }
}

baseMethods.verifyTimeSeriesEventLayerData = function (layerName) {
    var maplayerTabs = browser.elements("[data-test-id='TimeSeriesPlottedEventData']");
    var drivetestTab = maplayerTabs.value;
    var value = false;
    drivetestTab.forEach(element => {
        var textValue = element.getText();
        const text = new RegExp(layerName + '*');
        if (text.test(textValue)) {
            value = true;
        }
    });
    return value;
}

baseMethods.clickOnTimeSeriesCloseLayerBtn = function () {
    var layerElement = browser.element("[data-test-id='close-button']");
    layerElement.click();
    browser.pause(100);
}

baseMethods.clickOnTimeSeriesRemoveAllBtn = function () {
    var removeButton = browser.element("[data-test-id='RemoveAllLayers']");
    removeButton.click();
    browser.pause(100);
}

baseMethods.verifyEventsLayerData = function (eventName) {
    var maplayerTabs = browser.elements("[data-test-id='listcontent']");
    var drivetestTab = maplayerTabs.value;
    var value = false;
    drivetestTab.forEach(element => {
        var textValue = element.getText();
        const text = new RegExp(eventName + '*');
        if (text.test(textValue)) {
            value = true;
        }
    });
    return value;
}

baseMethods.clickOnInbuildingTestMenu = function () {
    genericObject.clickOnDTTab(parameter.DataMenu);
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    baseMethods.dragAndDrop(parameter.MapDraggable, parameter.WidgetRoot);
    var inbuildingElement = browser.element("[data-test-id='inbuilding']");
    inbuildingElement.click();
}

baseMethods.clickOnGeoMenu = function () {
    genericObject.clickOnDTTab(parameter.DataMenu);
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    baseMethods.dragAndDrop(parameter.MapDraggable, parameter.WidgetRoot);
    var geoElement = browser.element("[data-test-id='geo']");
    geoElement.click();
    browser.waitForVisible("[data-test-id='right-tab-menu-header']", 20000);
    browser.waitForVisible("//a[@id='ZoomInButton']", 20000);
}

baseMethods.verifyData = function () {
    browser.pause(1000);
    var eventsdataTab = browser.elements("//div[@data-test-id='data-grid-container']/ngx-datatable[1]/div[1]/datatable-body[1]/datatable-selection[1]/datatable-scroller[1]/datatable-row-wrapper");
    var objectCount = eventsdataTab.value;
    var elementCount = objectCount.length;
    var count = false;
    if (elementCount > 2) {
        count = true;
    }
    return count;
}

baseMethods.waitTillDataLoads = function () {
    clickOnDataMenu(parameter.DataMenu);
    //baseMethods.verifySpinnerVisibility();
}

baseMethods.clickOnInfoTab = function () {
    browser.waitForVisible("[data-test-id='IconUniqueDataIdInfo']", 20000);
    var mapMenu = browser.element("[data-test-id='IconUniqueDataIdInfo']");
    mapMenu.click();
    browser.waitForVisible("[data-test-id='left-tab-menu-header']", 20000);
}

baseMethods.verifyMapSearch = function () {
    var searchLabel = browser.elements("[data-test-id='map-search']");
    var searchLabelElmnt = searchLabel.value;
    var value = false;
    searchLabelElmnt.forEach(element => {
        var textValue = element.isVisible();
        if (textValue == true) {
            value = true
        }
    });
    return value;
}

baseMethods.verifyAdvancedSearchVerticalBtns = function (datatestid) {
    var searchLabel = browser.elements(datatestid);
    var searchLabelElmnt = searchLabel.value;
    var value = false;
    searchLabelElmnt.forEach(element => {
        var textValue = element.isVisible();
        if (textValue == true) {
            value = true
        }
    });
    return value;
}

baseMethods.verifyAddNetworkDataSideBarItems = function (tabName) {
    var assertionvalue = false;
    browser.pause(100);
    var header = browser.elements("//span[@class='new-query']");
    var headerValue = header.value;
    headerValue.forEach(element => {
        var text = element.getText();
        if (text == tabName) {
            assertionvalue = true;
        }
    });
    return assertionvalue;
}

baseMethods.clickOnAddNetworkDataSideBarItems = function (tabName) {
    var assertionvalue = false;
    var header = browser.elements("//span[@class='new-query']");
    var headerValue = header.value;
    headerValue.forEach(element => {
        var text = element.getText();
        if (text == tabName) {
            element.click();
        }
    });
}

baseMethods.clickOnSequenceData = function (tabName) {
    var assertionvalue = false;
    var header = browser.elements("[data-test-id='left-tab-menu-header']");
    var headerValue = header.value;
    headerValue.forEach(element => {
        var text = element.getText();
        if (text == tabName) {
            element.click();
        }
    });
}

baseMethods.addNetworkDataHeader = function (headerName) {
    var assertionvalue = false;
    var header = browser.elements("//div[@class='import-data-screen-header']");
    var headerValue = header.value;
    headerValue.forEach(element => {
        var text = element.getText();
        if (text == headerName) {
            assertionvalue = true;
        }
    });
    return assertionvalue;
}

baseMethods.verifyImportDataWizardHeader = function (headerName) {
    var assertionvalue = false;
    var header = browser.elements("//div[@class='import-data-wizard-item']");
    var headerValue = header.value;
    headerValue.forEach(element => {
        var text = element.getText();
        if (text == headerName) {
            assertionvalue = true;
        }
    });
    return assertionvalue;
}

baseMethods.searchNQLQuery = function () {
    var searchData = browser.element("[data-test-id='Search Data']");
    searchData.click();
    var newQueryText = browser.element("[data-test-id='NQLquery-text-area']");
    newQueryText.click();
    newQueryText.keys("Dataset contains ");
    var braces = browser.element("//span[@class='cm-string']");
    braces.click();
    braces.keys("GoodData");
    var searchButton = browser.element("[data-test-id='searchButton']");
    searchButton.click();
    //baseMethods.verifySpinnerVisibility();
    browser.pause(300);
}
// XYN_1923 - customized search

baseMethods.clickOnAdvancedSearchViewIcon_ALF = function () {
    baseMethods.checkForWorkspaceWidget();
    // baseMethods.navigateToAdvancedSearchMenu();
    // baseMethods.navigateToAdvancedImportMenu();
    baseMethods.searchEventNQLQuery_ALF();
    browser.pause(500);
    var datasetItem = browser.element("[data-test-id='listItem']");
    datasetItem.click();
    baseMethods.verifySpinnerVisibility();
    browser.pause(500);
    var viewIcon = browser.element("[data-test-id='openInCanvasbtn']");
    viewIcon.click();
    //baseMethods.navigateToAdvancedImportMenu();
    browser.pause(1500);
    //baseMethods.clickOnDataCloseBtn();
}

baseMethods.searchEventNQLQuery_ALF = function () {
    baseMethods.navigateToAdvancedSearchMenu();
    var newQueryTab = browser.element("[data-test-id='NQLquery-text-area']");
    newQueryTab.click();
    newQueryTab.keys("Dataset contains ");
    var braces = browser.element("//span[@class='cm-string']");
    braces.click();
    braces.keys("Run38");
    var searchButton = browser.element("[data-test-id='searchButton']");
    searchButton.click();
    baseMethods.verifySpinnerVisibility();
    browser.pause(5000);
}
// XYN_1923 - customized search
baseMethods.navigateToAdvancedSearchMenu = function () {
    var expander = browser.element("//div[@class='bottom-expander']");
    expander.click();
    var searchButton = browser.element("[data-test-id='Search Data']");
    searchButton.click();
    genericObject.clearsearchkeyword();
}

baseMethods.navigateToAdvancedImportMenu = function () {
    var expander = browser.element("[data-test-id='menubarExpander']");
    expander.click();
    var searchButton = browser.element("[data-test-id='Import Data']");
    searchButton.click();
    var historyButton = browser.element("[data-test-id='History']");
    historyButton.click();
    //baseMethods.verifySpinnerVisibility();
    var filterButton = browser.element("[data-test-id='history-filter-box']");
    filterButton.click();
    browser.pause(1500);
    filterButton.keys("voice_sprint-mt", 1000);
    browser.pause(1500);
    var elem = browser.element("//div[@data-test-id='data-grid-container']/ngx-datatable/div/datatable-body/datatable-selection/datatable-scroller/datatable-row-wrapper/datatable-body-row/div[2]/datatable-body-cell[2]/div/label");
    elem.click();
    browser.pause(200);
    var open = browser.element("[data-test-id='open-selected-file']");
    open.click();
    browser.pause(100);
}

baseMethods.clickOnAdvancedSearchViewIcon = function (datasetname) {
    baseMethods.checkForWorkspaceWidget();
    genericObject.clickOnDataMenu();
    // baseMethods.navigateToAdvancedSearchMenu();
    // baseMethods.navigateToAdvancedImportMenu();
    baseMethods.searchEventNQLQuery(datasetname);
    browser.pause(500);
    //var datasetItem = browser.element("[data-test-id='listItem']");
    var datasetItem = browser.element("//span[@class='listItem-text']");
    datasetItem.click();
    baseMethods.verifySpinnerVisibility();
    browser.pause(500);
    var viewIcon = browser.element("[data-test-id='openInCanvasbtn']");
    viewIcon.click();
    //baseMethods.navigateToAdvancedImportMenu();
    browser.pause(1500);
    //baseMethods.clickOnDataCloseBtn();
}

baseMethods.searchEventNQLQuery = function (value) {
    baseMethods.navigateToAdvancedSearchMenu();
    var newQueryTab = browser.element("[data-test-id='NQLquery-text-area']");
    newQueryTab.click();
    newQueryTab.keys("Dataset contains ");
    var braces = browser.element("//span[@class='cm-string']");
    braces.click();
    braces.keys(value);
    var searchButton = browser.element("[data-test-id='searchButton']");
    searchButton.click();
    baseMethods.verifySpinnerVisibility();
    browser.pause(5000);
}

baseMethods.searchEventDataSet = function () {
    baseMethods.navigateToAdvancedSearchMenu();
    baseMethods.searchEventNQLQuery();
    var datasetItem = browser.element("[data-test-id='listItem']");
    datasetItem.click();
    baseMethods.verifySpinnerVisibility();
    browser.pause(100);
    var viewIcon = browser.element("[data-test-id='viewsicon']");
    viewIcon.click();
    browser.pause(3000);
}

baseMethods.clickOnImportData = function () {
    var leftMenuElements = browser.elements("[data-test-id='leftMenuTab']");
    var leftMenuTab = leftMenuElements.value;
    leftMenuTab.forEach(element => {
        var text = element.getText();
        if (text == "Import Data") {
            element.click();
            browser.pause(1000);
        }
    });
}

baseMethods.clickOnAnalyticsView = function () {
    var analyticsMenu = browser.element("[data-test-id='Analytics View']");
    analyticsMenu.click();
    browser.pause(100);
}

baseMethods.clickOnMenuExpander = function () {
    browser.pause(100);
    var analyticsMenu = browser.element("[data-test-id='menubarExpander']");
    analyticsMenu.click();
    browser.pause(100);
}

baseMethods.filterMetric = function (metricName) {
    var metricsTab = browser.element("[data-test-id='Metrics-search-box']");
    metricsTab.setValue(metricName);
    //baseMethods.verifySpinnerVisibility();
    browser.pause(500);
    var treeElement = browser.element("[data-test-id='filteredMetric']");
    browser.pause(100);
    var text = treeElement.getText();
    return text;
}

baseMethods.verifyFilteredMetricTechnology = function (metricName) {
    var metricsTab = browser.element("[data-test-id='Metrics-search-box']");
    metricsTab.setValue(metricName);
    //baseMethods.verifySpinnerVisibility();
    browser.pause(500);
    var treeElement = browser.element("[data-test-id='filteredMetric']");
    var treeElement = browser.element("[data-test-id='filteredMetricTechnology']");
    browser.pause(100);
    var text = treeElement.getText();
    return text;
}

baseMethods.clickOnOpenInCanvasBtn = function () {
    var canvasBtn = browser.element("[data-test-id='openInCanvasbtn']");
    canvasBtn.click();
    browser.pause(600);
    //baseMethods.clickOnDataCloseBtn();
}

baseMethods.drawPolygon = function () {
    var polygon = browser.element("[data-test-id='IconUniqueDataIdpolygon']");
    polygon.click();
    browser.moveToObject("//canvas[@id='tileCanvasId']", 400, 150);
    browser.buttonDown();
    browser.buttonUp();
    browser.moveToObject("//canvas[@id='tileCanvasId']", 460, 210);
    browser.buttonDown();
    browser.buttonUp();
    browser.moveToObject("//canvas[@id='tileCanvasId']", 200, 250);
    browser.buttonDown();
    browser.buttonUp();
    browser.moveToObject("//canvas[@id='tileCanvasId']", 300, 180);
    browser.buttonDown();
    browser.buttonUp();
    browser.buttonDown();
    browser.buttonUp();
    var polygon = browser.element("[data-test-id='IconUniqueDataIdpolygon']");
    polygon.click();
}

baseMethods.clickOnSavePolygonBtn = function () {
    var saveButton = browser.element("[data-test-id='IconUniqueDataIdsavePolygon']");
    saveButton.click();
    //baseMethods.verifySpinnerVisibility();
}

baseMethods.searchPolygonArea = function (polygonName) {
    var popupElment = browser.element("[data-test-id='SavedNameinput']");
    popupElment.setValue(polygonName);
}

baseMethods.clcikOnSavePolygonArea = function () {
    var saveAreaButton = browser.element("[data-test-id='SaveBtn']");
    saveAreaButton.click();
}

baseMethods.clcikOnCancelPolygonBtn = function () {
    var cancelSaveAreaButton = browser.element("[data-test-id='DataSetSaveCancelBtn']");
    cancelSaveAreaButton.click();
}

baseMethods.clickOnOpenSavedPolygonBtn = function () {
    var openSavedElemnet = browser.element("[data-test-id='IconUniqueDataIdopenPolygon']");
    openSavedElemnet.click();
    //baseMethods.verifySpinnerVisibility();
}

baseMethods.openPolygonArea = function (polygonName) {
    var openSearchArea = browser.element("[data-test-id='SavedSearchBtn']");
    openSearchArea.setValue("Auto_SavePolygonTest");
    //baseMethods.verifySpinnerVisibility();
}

baseMethods.clickOnMapBin = function () {
    var mapComponent = browser.executeAsync(function (done) {
        var elem;
        elem = document.querySelector("[data-automation-id='mapTestHook']");
        elem.click();
        done(true);
    });
    var x = browser.getAttribute("[data-automation-id='mapTestHook']", "data-automation-x");
    var y = browser.getAttribute("[data-automation-id='mapTestHook']", "data-automation-y");
    browser.moveToObject("//canvas[@id='tileCanvasId']", parseFloat(x), parseFloat(y));
    browser.buttonDown();
    browser.buttonUp();
    browser.pause(3000);
}

baseMethods.clickOnBinViewmoreBtn = function () {
    browser.element("[data-test-id='viewmorebtn']").click();
    browser.pause(100);
}

baseMethods.verifyBinTooltipData = function () {
    baseMethods.clickOnMapBin();
    var toolTipRow = browser.elements("[data-test-id='tooltip-row']").getText();
    baseMethods.clickOnBinViewmoreBtn();
    var binInfo = browser.elements("//div[@data-test-id='infoBinContent']/ul/li").getText();
    var count = 0;
    toolTipRow.forEach(tooltipElement => {
        binInfo.forEach(bininfoElement => {
            var position = tooltipElement.indexOf(":");
            var tooltipValue = tooltipElement.slice(position + 1);
            var tooltipData = tooltipValue.replace(/(\n)/, "");
            var bininfoPosition = bininfoElement.indexOf(":");
            var bininfoValue = bininfoElement.slice(bininfoPosition + 1);
            var bininfoData = bininfoValue.trim(" ");
            if (tooltipData == bininfoData) {
                count++;
            }
        });
    });
    return count;
}

baseMethods.clickOnCalculateLatency = function () {
    var latencyBtn = browser.element("[data-test-id='Calculate-latency-btn']");
    latencyBtn.click();
};

baseMethods.clickOnColumnsbutton = function () {
    var columnsBtn = browser.element("[data-test-id='columns-btn']");
    columnsBtn.click();
};

baseMethods.clickOnDatalayerstab = function () {
    var datalayersTab = browser.element("[data-test-id='left-tab-menu-header']");
    datalayersTab.click();
};

baseMethods.clickOnSequenceInfo = function () {
    var SequenceInfoTab = browser.getElementsByClassName("[class='tab-menu selected]");
    SequenceInfoTab.click();
};

baseMethods.clickOnSequenceDatalayers = function myfunction() {
    var SequenceDataTab = browser.element("//span[@data-test-id='left-tab-menu-header']");
    SequenceDataTab.click();
};

baseMethods.clickOnCloseBtn = function () {
    var CloseBtn = browser.element("[data-test-id='close-btn']");
    CloseBtn.click();
};

baseMethods.clickOnHideBtn = function () {
    var HideBtn = browser.element("[data-test-id='hide-btn']");
    HideBtn.click();
};


baseMethods.clickOnSaveWorkspace = function () {
    var SaveWorkBtn = browser.element("//div[@class='app-save-workspace-menu']");
    SaveWorkBtn.click();
};

baseMethods.calculateLatency = function () {
    var dataTab = browser.elements("[data-test-id='timestamp-text']");
    var objectCount = dataTab.value;
    var value = 0;
    var element1, element2;
    objectCount.forEach(element => {
        value++;
        if (value == 1) {
            element1 = element.getText();
        }
        else if (value == 2) {
            element2 = element.getText();
        }
    });
    var integerValue1 = new Date(element1);
    var integerValue2 = new Date(element2);
    var latency = (Math.abs(integerValue1 - integerValue2)) / 1000;
    return latency;
};

baseMethods.clickOnWorkspaceSaveAsBtn = function () {
    var saveasbtn = browser.element("[data-test-id='ws-saveas-btn']");
    saveasbtn.click();
}

baseMethods.clickOnWorkspaceSaveOKBtn = function () {
    var saveOKbtn = browser.element("[data-test-id='ws-savebtn']");
    saveOKbtn.click();
}

baseMethods.saveWorkspace = function (workspaceName) {
    genericObject.clickOnDTTab(parameter.WorkspacesMenu);
    // var verifyWorkspace=browser.element("[data-test-id='Auto-Test']").isExisting();
    // if(verifyWorkspace == true){
    //     workspaceName=workspaceName + '1';
    // }
    baseMethods.clickOnWorkspaceSaveAsBtn();
    var inputTab = browser.element("[data-test-id='ws-name-input']");
    inputTab.click();
    inputTab.keys(workspaceName);
    baseMethods.clickOnWorkspaceSaveOKBtn();
    //baseMethods.verifySpinnerVisibility();
    browser.pause(1000);
}

baseMethods.renameWorkspace = function (workspaceName) {
    var renameTab = browser.element("[data-test-id='Rename-Input']");
    renameTab.click();
    renameTab.keys(workspaceName);
    var renameSaveBtn = browser.element("[data-test-id='rename-save-btn']");
    renameSaveBtn.click();
    //baseMethods.verifySpinnerVisibility();
    browser.pause(5000);
}

baseMethods.clickOnWSOpenBtn = function () {
    var openbtn = browser.element("[data-test-id='open-ws-btn']");
    openbtn.click();
    // baseMethods.verifySpinnerVisibility();
    browser.pause(8000);
}

baseMethods.clickOnAdvancedMenu = function () {
    var advanced = browser.element("[data-test-id='Advanced']");
    advanced.click();
    //baseMethods.verifySpinnerVisibility();
}

baseMethods.clickOnDriveTestMenu = function () {
    var drivetest = browser.element("[data-test-id='Drive Test']");
    drivetest.click();
    //baseMethods.verifySpinnerVisibility();
}

baseMethods.clickOnWorkspaceMenu = function () {
    var workspace = browser.element("[data-test-id='Workspaces']");
    workspace.click();
    baseMethods.verifySpinnerVisibility();
}

baseMethods.clickOnDataCloseBtn = function () {
    var closeBtn = browser.element("[data-test-id='close-button']");
    closeBtn.click();
}

baseMethods.clickOnViewsCloseAllBtn = function () {
    var closeAllBtn = browser.element("[data-test-id='close-all-btn']");
    closeAllBtn.click();
}

baseMethods.clickOnViewsSaveAsBtn = function () {
    var saveAsBtn = browser.element("[data-test-id='save-as-btn']");
    saveAsBtn.click();
}

baseMethods.uniqueWorkspaceName = function () {
    var input = "Test";
    var count = 0;
    var uiqueInput = false;
    var inputTab = browser.element("[data-test-id='ws-name-input']");
    inputTab.click();
    while (uiqueInput == false) {
        inputTab.keys(input);
        var dropdownElements = browser.elements("[data-test-id='workspace-dropdown']");
        var dropdownValues = dropdownElements.value;
        dropdownValues.forEach(element => {
            var textValue = element.getText();
            if (textValue == input) {
                uniqueInput = false;
                input = input + count;
                count++;
            }
        });
    }
}

baseMethods.checkForWorkspaceWidget = function () {
    browser.pause(3000);
    var workspaceWidget = browser.element("[data-test-id='open-ws-cancel']").isExisting();
    if (workspaceWidget == true) {
        browser.element("[data-test-id='open-ws-cancel']").click();
    }
    else {
        return;
    }
}
baseMethods.clickOnTimeseriesEventElement = function (timestamp) {
    //var elem = browser.elements("//*[@class='highcharts-root']//*[@class='highcharts-series-group']//*[@data-z-index='0.1']//*[@class='highcharts-point highcharts-color-0']");
    /* The above element identifier is valid x-path but doesn't return any value(s) so used below element identifier*/
    var elem = browser.elements("//*[@class='highcharts-root']//*[@class='highcharts-series-group']//*[@preserveAspectRatio='none']");
    var elementValue = elem.value;
    var i = 0;
    elementValue.forEach(element => {
        if (i == 0) {
            browser.pause(200);
            element.click();
            browser.pause(100);
            var timeseriesEventElement = browser.element("//div[@class='height-width-full']/div[1]/*[@class='highcharts-root']/*[@data-z-index='8']/*[@data-z-index='1']");
            var tooltipContent = timeseriesEventElement.element('<tspan />').getText();
            if (tooltipContent == timestamp) {
                console.log(tooltipContent + ' is timestamp for Time Series');
                i = 1;
            }
        }

    })
}

baseMethods.setStartTime = function (startTime, endTime) {
    var startTimeBtn = browser.element("[data-test-id='starttime-radio-btn']");
    startTimeBtn.click();
    var startdateclearBtn = browser.element("//div[@class='control-group'][2]/div[1]/my-date-picker/div/div/div[1]/button[@class='btnclear btnclearenabled']");
    startdateclearBtn.click();
    startdateclearBtn.click();
    browser.pause(200);
    var dateInput = browser.element("//div[@class='control-group'][2]/div[1]/my-date-picker/div/div/input");
    dateInput.click();
    dateInput.setValue(startTime);
    var enddateclearBtn = browser.element("//div[@class='control-group'][2]/div[3]/my-date-picker/div/div/div[1]/button[@class='btnclear btnclearenabled']");
    enddateclearBtn.click();
    enddateclearBtn.click();
    browser.pause(200);
    var dateInput = browser.element("//div[@class='control-group'][2]/div[3]/my-date-picker/div/div/input");
    dateInput.click();
    dateInput.setValue(endTime);
    var applyBtn = browser.element("[data-test-id='apply-btn']");
    applyBtn.click();
    browser.pause(300);
}

baseMethods.filterBrowseData = function (datasetName) {
    var filterButton = browser.element("[data-test-id='browse-data-filter']");
    //baseMethods.clickOnSequenceData("Files");
    //var filterButton = browser.element("[data-test-id='browse-data-files-filter']");
    filterButton.click();
    browser.pause(500);
    filterButton.keys(datasetName, 1000);
    browser.pause(1000);
    var elem = browser.element("//div[@data-test-id='data-grid-container']/ngx-datatable/div/datatable-body/datatable-selection/datatable-scroller/datatable-row-wrapper/datatable-body-row/div[2]/datatable-body-cell[2]/div/label");
    //var elem = browser.element("//div[@data-test-id='data-grid-container']/ngx-datatable/div/datatable-header/div/div[2]/datatable-header-cell[2]/div/label/input");
    elem.click();
    browser.pause(200);
    var open = browser.element("[data-test-id='open-flex']");
    open.click();
    browser.pause(500);
}

baseMethods.verifyTimeSeriesMetricLayerData = function (layerName) {
    var maplayerTabs = browser.elements(parameter.TimeseriesLayerData);
    var drivetestTab = maplayerTabs.value;
    var value = false;
    drivetestTab.forEach(element => {
        var textValue = element.getText();
        const text = new RegExp(layerName + '*');
        if (text.test(textValue)) {
            value = true;
        }
    });
    return value;
}
//Verify plotted events from TimeSeries
baseMethods.verifyTimeSeriesEventLayerData = function (layerName) {
    var tslayerTabs = browser.elements(parameter.timeSeriesEventData);
    var drivetestTab = tslayerTabs.value;
    var value = false;
    drivetestTab.forEach(element => {
        var textValue = element.getText();
        const text = new RegExp(layerName + '*');
        if (text.test(textValue)) {
            value = true;
        }
    });
    return value;
}
//Verify plotted events from Map Layers
baseMethods.verifyMapEventLayerData = function (layerName) {
    var maplayerTabs = browser.elements(parameter.mapLayersPlottedEvents);
    var drivetestTab = maplayerTabs.value;
    var value = false;
    drivetestTab.forEach(element => {
        var textValue = element.getText();
        const text = new RegExp(layerName + '*');
        if (text.test(textValue)) {
            value = true;
        }
    });
    return value;
}
baseMethods.clickOnMapLayerBtn = function () {
    browser.element(parameter.mapDataLayerBtn).click();
    browser.pause(200);
}

baseMethods.skipSomeDataFiles = function () {
    var fileElement = browser.element("//div[@data-test-id='20180330_1300_0110_18678.drm']//span[@class='tree-nodechk']//span[@data-test-id='check-box']");
    //20180330_1300_0110_18678.drm
    fileElement.waitForVisible(200);
    fileElement.click();
    var fileElnt = browser.element("//div[@data-test-id='20180330_1411_0110_18678.drm']//span[@class='tree-nodechk']//span[@data-test-id='check-box']");
    fileElnt.waitForVisible(200);
    fileElnt.click();
    var refreshBtn = browser.element("[data-test-id='refresh-btn']");
    refreshBtn.click();
    var refreshViewsBtn = browser.element("[data-test-id='Refresh']");
    refreshViewsBtn.click();
}

baseMethods.SelectedDataToCalculateLatency = function () {
    var result = [];
    var dataTab = browser.elements("[data-test-id='list-line-center']");
    var objectCount = dataTab.value;
    var value = 1;
    objectCount.forEach(element => {
        if (value == 1 || value == 2) {
            element.click();
            result = browser.elements("//div[@class='list-line-center selected']").getText();
        }

        value++;
    });
    return result;
};


baseMethods.clickOnSearchViewIcon = function (value) {
    //baseMethods.searchTwoDeviceDataSet();
    // baseMethods.navigateToAdvancedSearchMenu();
    // baseMethods.navigateToAdvancedImportMenu();
    baseMethods.searchTwoDeviceDataSet(value);
    browser.pause(500);
    var dataTab = browser.elements("[data-test-id='listItem']");
    var objectCount = dataTab.value;
    var value = 1;
    objectCount.forEach(element => {
        if (value == 1 || value == 2) {
            element.click();
        }
    })
    baseMethods.verifySpinnerVisibility();
    browser.pause(500);
    var viewIcon = browser.element("[data-test-id='openInCanvasbtn']");
    viewIcon.click();
    //baseMethods.navigateToAdvancedImportMenu();
    browser.pause(1500);
    //baseMethods.clickOnDataCloseBtn();
}

baseMethods.searchTwoDeviceDataSet = function (value) {
    baseMethods.navigateToAdvancedSearchMenu();
    var newQueryTab = browser.element("[data-test-id='NQLquery-text-area']");
    newQueryTab.click();
    newQueryTab.keys("Dataset contains ");
    var braces = browser.element("//span[@class='cm-string']");
    braces.click();
    braces.keys(value);
    var searchButton = browser.element("[data-test-id='searchButton']");
    searchButton.click();
    baseMethods.verifySpinnerVisibility();
    browser.pause(1500);
}
baseMethods.SerchQueryInNewSearch = function (item, ConditionforSearch, SearchText) {
    baseMethods.checkForWorkspaceWidget();
    // baseMethods.navigateToAdvancedSearchMenu();
    // baseMethods.navigateToAdvancedImportMenu();
    baseMethods.searchNQLQueryCustom(item, ConditionforSearch, SearchText);
    baseMethods.verifySpinnerVisibility();
    browser.pause(1000);
    var datasetItem = browser.element("//span[@class='listItem-text']");
    datasetItem.click();
    baseMethods.verifySpinnerVisibility();
    browser.pause(500);
    //var viewIcon = browser.element("[data-test-id='openInCanvasbtn']");
    // viewIcon.click();
    //baseMethods.navigateToAdvancedImportMenu();
    browser.pause(1500);
    //baseMethods.clickOnDataCloseBtn();
}
baseMethods.searchNQLQueryCustom = function (item, ConditionforSearch, SeachText) {
    //baseMethods.navigateToAdvancedSearchMenu();
    var newQueryTab = browser.element("[data-test-id='NQLquery-text-area']");
    newQueryTab.click();
    var sSearchText = item + " " + ConditionforSearch + " ";
    newQueryTab.keys(sSearchText);
    var braces = browser.element("//span[@class='cm-string']");
    braces.click();
    braces.keys(SeachText);
    var searchButton = browser.element("[data-test-id='searchButton']");
    searchButton.click();
    baseMethods.verifySpinnerVisibility();
    browser.pause(1500);
}

baseMethods.searchNQLQueryChannel = function (item, ConditionforSearch, SearchText) {
    //baseMethods.navigateToAdvancedSearchMenu();
    var newQueryTab = browser.element("[data-test-id='NQLquery-text-area']");
    newQueryTab.click();
    var sSearchText = item + " " + ConditionforSearch + " " + SearchText;
    newQueryTab.keys(sSearchText);
    //var braces = browser.element("//span[@class='cm-string']");
    //braces.click();
    //braces.keys(SearchText);
    var searchButton = browser.element("[data-test-id='searchButton']");
    searchButton.click();
    baseMethods.verifySpinnerVisibility();
    browser.pause(1500);
}

baseMethods.SelectTabs = function (tabName) {
    switch (tabName) {
        case "IMPORTDATA":
            baseMethods.checkForWorkspaceWidget();
            var expander = browser.element("//div[@class='bottom-expander']");
            expander.click();
            var ImportData = browser.element("[data-test-id='Import Data']");
            ImportData.click();
            break;
        case "SEARCHDATA":
            baseMethods.checkForWorkspaceWidget();
            var expander = browser.element("//div[@class='bottom-expander']");
            expander.click();
            var SearchData = browser.element("[data-test-id='Search Data']");
            SearchData.click();
            break;
        case "BROWSEDATA":
            baseMethods.checkForWorkspaceWidget();
            var expander = browser.element("//div[@class='bottom-expander']");
            expander.click();
            var browseData = browser.element("[data-test-id='Browse Data']");
            browseData.click();
            break;
    }
};

baseMethods.selectdatasetinImportHistory = function (dataset) {
    var datatab = browser.element("[data-test-id='Data']");
    datatab.click();
    baseMethods.SelectTabs("IMPORTDATA");
    var historyButton = browser.element("[data-test-id='History']");
    historyButton.click();
    //baseMethods.verifySpinnerVisibility();
    var filterButton = browser.element("[data-test-id='history-filter-box']");
    filterButton.click();
    browser.pause(1000);
    filterButton.keys(dataset, 1000);
    browser.pause(1500);
    var elem = browser.element("//div[@data-test-id='data-grid-container']/ngx-datatable/div/datatable-body/datatable-selection/datatable-scroller/datatable-row-wrapper/datatable-body-row/div[2]/datatable-body-cell[2]/div/label");
    elem.click();
    //browser.pause(200);
    //open.click();
    browser.pause(100);
}

baseMethods.selectdatasetinBrowseDatasets = function (dataset) {
    var datatab = browser.element("[data-test-id='Data']");
    datatab.click();
    baseMethods.SelectTabs("BROWSEDATA");
    //baseMethods.verifySpinnerVisibility();
    var filterButton = browser.element("[data-test-id='browse-data-filter']");
    filterButton.click();
    browser.pause(1000);
    filterButton.keys(dataset, 1000);
    browser.pause(1500);
    var elem = browser.element("//div[@data-test-id='data-grid-container']/ngx-datatable/div/datatable-body/datatable-selection/datatable-scroller/datatable-row-wrapper/datatable-body-row/div[2]/datatable-body-cell[2]/div/label");
    elem.click();
    browser.pause(100);
}

baseMethods.saveFileName = function (value) {
    var d = new Date();
    //var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth() + 1;      // "+ 1" becouse the 1st month is 0
    var day = d.getDate();
    var hour = d.getHours();
    var minutes = d.getMinutes();
    var secconds = d.getSeconds()
    var nametext = value + year + month + day + hour + minutes + secconds;
    //textbox.keys(nametext);
    return nametext;
    //browser.pause(100);
}

baseMethods.closeMapDataLayer = function () {
    var mapLayer = browser.element("//map-layer");
    var mapLayerCloseBtn = mapLayer.element("[data-test-id='close-button']");
    if (mapLayerCloseBtn.isVisible()) {
        mapLayerCloseBtn.click();
    }
}
baseMethods.closeTimeSeriesDataLayer = function () {
    var tSDataLayer = browser.elements("//div[@class='right-menu-pane']");
    var closeTSDataLayer = tSDataLayer.element("[data-test-id='close-button']");
    if (closeTSDataLayer.isVisible()) {
        closeTSDataLayer.click();
    }
}

baseMethods.SelectSearchDataTabs = function (SearchDataTabName) {
    switch (SearchDataTabName) {
        case "NEWSEARCH":
            baseMethods.checkForWorkspaceWidget();
            //var expander = browser.element("//div[@class='bottom-expander']");
            //expander.click();
            var NewSearch = browser.element("[data-test-id='New Search']");
            NewSearch.click();
            browser.pause(1500);
            break;

        case "OPENSEARCHTERMS":
            baseMethods.checkForWorkspaceWidget();
            // var expander = browser.element("//div[@class='bottom-expander']");
            // expander.click();
            var OpenSearchTerms = browser.element("[data-test-id='Open Search Terms']");
            OpenSearchTerms.click();
            browser.pause(1500);
            break;
    }
}
baseMethods.getAppendMessage = function () {
    var importScreen = browser.elements("//import-data-files");
    var appendMessage = importScreen.elements("//div[@class='ds-info-message flex-centered']");
    console.log(appendMessage.getText());//button-item margin-offset
    return appendMessage.getText();
}

baseMethods.ClearSearchedEvent = function () {
    genericObject.clickOnDTTab(parameter.EventsMenu);
    baseMethods.verifySpinnerVisibility();
    var eventElement = browser.element("[data-test-id='clear-event-name']");
    eventElement.click();
}
baseMethods.closeAllViews = function () {
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    browser.pause(500);
    var closeAllViews = browser.element("[data-test-id='close-all-btn']");
    if (closeAllViews.isVisible()) {
        closeAllViews.click();
        browser.pause(300);
        //var closeAllViewsTitleBar=browser.element("[data-test-id='Close All Views-Title-Bar']");
        var modalBox = browser.element("//modal-box");
        var mBoxCloseBtn = modalBox.element("[data-test-id='close-all-views']");
        if (mBoxCloseBtn.isVisible()) {
            mBoxCloseBtn.click();
        }
        browser.pause(300);
        //close views menu
        var viewsHeader = browser.element("//views-menu");
        var closeViewsMenu = viewsHeader.element("[data-test-id='close-button']");
        if (closeViewsMenu.isVisible()) {
            closeViewsMenu.click();
        }
    }
}

baseMethods.openInCanvasValidation = function() {
    var sameDataset = browser.element(parameter.sameDatasetDialogWindow);
    var replaceDataset = browser.element(parameter.replaceDatasetDialogWindow);
    if(sameDataset.isVisible()){
        browser.element(parameter.sameDatasetDialogWindowClose).click();
        browser.element("[data-test-id='Analytics View']").click();
        browser.pause(400);
    }
    else if(replaceDataset.isVisible()){
        browser.element(parameter.dataScopeReplaceBtn).click();
        browser.element(parameter.dataScopeReplaceOKBtn).click();
        browser.pause(3000);
        baseMethods.verifySpinnerVisibility();
    }
}
baseMethods.cLickFilesTab = function (headerName) {
    browser.waitForVisible("[data-test-id='left-tab-menu-header']", 10000);
    var tabHeader = browser.elements("[data-test-id='left-tab-menu-header']");
    var menuTab = tabHeader.value;
    var assertionvalue = false;
    menuTab.forEach(element => {
        var text = element.getText();
        if (text == headerName) {
            assertionvalue = true;
            element.click();
        }
    });
    return assertionvalue;
}
baseMethods.clearAllFavElements=function(value){
    browser.pause(400);
    console.log(value);
    if(value=="metric"){
        var elem=browser.elements(parameter.filteredMetric);
        //console.log("inside loop",elem.value.length);
        
        var j=1;
        while(j<=elem.value.length) {
            console.log(elem.value.length);
            console.log("removing fav elem",j);
            metricAndEventObject.clickFilteredMetric();
            metricAndEventObject.clickOnRemoveFavElem(); 
            console.log("removed fav elem");   
            browser.pause(500);
            j++;       
        }      
    }
    if(value=="event"){
        var elem=browser.elements(parameter.filteredEvent);
        var j=1;
        while(j<=elem.value.length) {
            console.log(elem.value.length);
            console.log("removing fav elem",j);
            metricAndEventObject.clickFilteredMetric();
            metricAndEventObject.clickOnRemoveFavElem(); 
            console.log("removed fav elem");   
            browser.pause(500);
            j++;       
        }    
    }
}
    
baseMethods.messagesRowContent = function (rowNo) {
        var msgRowContent = browser.elements(parameter.MessagesRowContent+"[" + rowNo + "]").getText();    
        return msgRowContent;
    }
    baseMethods.messagesgetAttribute = function (msgColumnFilter, style) {    
        var msgAttribute = browser.element(msgColumnFilter).getAttribute(style);
        return msgAttribute;
}

baseMethods.checkDatasetFilter=function(value){
    var errorMsg = browser.element(parameter.noData).isVisible();
    if (errorMsg == true) {
        var length = 1;
        while(length > 0){
            browser.element(parameter.datasetsFilterClear).click();
            browser.pause(500);
            browser.element(parameter.AVMenu).click();
            browser.element(parameter.DataMenu).click();
            baseMethods.cLickFilesTab(config.datasetsTab);
            baseMethods.cLickFilesTab(config.filesTab);
            browseDataObject.enter_Filename('"' +value+ '"');
            browser.pause(2000);
            var errorMsg1 = browser.element(parameter.noData).isVisible();
            if (errorMsg1 == true) {
                length = 1;
            }
            else{
                length = 0;
            }
        }
    }   
    return length;
}
baseMethods.selectDataSetFromBrowseData=function(value){
    baseMethods.SelectTabs(config.browseDataTab);
    browseDataObject.LastFilters(config.LastFilterDaysValue);
    browseDataObject.LastdropdownDays();
    browseDataObject.clickApplyBtn();
    browser.pause(200);
    browseDataObject.enter_datsetname('"' + value + '"');
    browser.pause(2000);
    browser.element(parameter.checkMarkLeft).click();
    browser.pause(200);
}

baseMethods.selectTemplate=function(val){
    var textis = "//div[@class='template-list']//div[@class='item-row']//*[contains(text(),'" + val + "')]";
    var selectTemp = browser.element(textis);    
    if(selectTemp.isVisible()){
        browser.element(textis).click();
    }                        
}

module.exports = baseMethods;