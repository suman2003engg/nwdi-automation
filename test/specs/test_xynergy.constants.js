var xynergy_Constants = {
    Drive_Test: 'DriveTest',
    Geo: 'GEO',
    Site: 'Site',
    EventAnalysis: 'Event Analysis',
    Inbuilding: 'Inbuilding',
    QueryTerms: "FileName contains ",
    QueryCondition: "18678",
    RSRPMetric:"Best Cell RSRP",
    RSRQMetric:"Best Cell RSRQ",
    VoiceStartEvent:"Accuver Voice Call Start",
    VoiceSetupEvent:"Accuver Voice Call Setup",
    VoiceCoversationStart: "Accuver Voice Call Conversation Start"
};
module.exports = xynergy_Constants;