let baseMethods = require('../../testbase');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let metricsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let timeseries=require("../../../pageobjects/timeseries.js");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();
describe('Metrics test', function () {
    var init=0;
    var type="metric";
    beforeEach(function () {
        if(!isElectron()){
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
               
        }
        
    });
    afterEach(function () {
        if(isElectron()){
            metricsDataObject.closeMetricWidget();
        }       
    });

    it('Verify whether Unavailable metrics[grayed out] should not plot graph on applicable views and should display error message - XYN-2113', function(){
        console.log("Verify whether Unavailable metrics[grayed out] should not plot graph on applicable views and should display error message - XYN-2113");
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        baseMethods.verifySpinnerVisibility();
        baseMethods.clearAllFavElements(type);
        browser.pause(200);
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TimeseriesDraggable, parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.unavailableMetric, parameter.unavailableMetric, parameter.TimeseriesDroppable);
        metricsDataObject.clickOkOnErrorMsg();
        metricsDataObject.clickOnTimeseriesCloseBtn();

        // genericObject.clickOnDTTab(parameter.ViewsMenu);
        // genericObject.dragAndDropViews(parameter.MapDraggable, parameter.FreshViewsDroppable);
        // browser.pause(300);
        // baseMethods.plotMetric(config.unavailableMetric, parameter.unavailableMetric, parameter.MapDroppable);
        // browser.pause(300);
        // metricsDataObject.clickOnMapErrorMsg();

        console.log("Verify whether Unavailable metrics[grayed out] should not plot graph on applicable views and should display error message - XYN-2113");
    });
    it('To verify whether toggle button under metrics remembers the selection - XYN-2364', function(){
        console.log("Test started - To verify whether toggle button under metrics remembers the selection - XYN-2364");
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        browser.pause(300);                     
        //When 'Show only available metrics' button is on
        metricsDataObject.clickOnToggle();
        var grayMetrics = browser.elements(parameter.greyMetricsList).isVisible();
        assert.equal(grayMetrics, false);
        //close and re-open metrics tab
        metricsDataObject.closeMetricWidget();
        browser.pause(300);
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        browser.pause(300);
        var grayMetrics = browser.elements(parameter.greyMetricsList).isVisible();
        assert.equal(grayMetrics, false);
        //Untoggle at end of script
        //metricsDataObject.clickOnToggle();
        metricsDataObject.closeMetricWidget();
        console.log("Test completed - To verify whether toggle button under metrics remembers the selection - XYN-2364");
    });
    it('Verify whether Metric widget options working accordingly - XYN-1516', function(){
        console.log("Test started - XYN-1516 - Verify Metric widget options");
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        browser.pause(300);       
        var dockingpinUnpinned = browser.element(parameter.dockpinUnPin);        
        if (dockingpinUnpinned.isVisible()){             
            metricsDataObject.eventWidgetDock();
        }
        browser.pause(300);
        var dockingpinPinned = browser.element(parameter.dockpinStatus);
        assert.equal(dockingpinPinned.isVisible(), true);
        metricsDataObject.closeMetricWidget();
        //Search for metric
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        var dockingpinPinned = browser.element(parameter.dockpinStatus);
        metricsDataObject.enterMetricFilterValue(config.metricName);
        //verify pinned window not closed
        assert.equal(dockingpinPinned.isVisible(), true);
        //close metric widget and verify
        metricsDataObject.closeMetricWidget();
        browser.pause(300);
        var MetricsTabClosed = browser.element(parameter.metricTabStatus);
        assert.equal(MetricsTabClosed.isVisible(), false);
        //clear metric filter and undock and close metric view
        genericObject.clickOnDTTab(parameter.MetricsMenu);
        metricsDataObject.clearMetricName();
        browser.pause(300);
        metricsDataObject.eventWidgetUnDock();
        browser.pause(300);
        metricsDataObject.closeMetricWidget();       
        console.log("Test completed - XYN-1516 - Verify Metric widget options");
    });
    it('To Verify Per Cell KPIs plotting selection option  - XYN-2757', function () {
        console.log("Test started To Verify Per Cell KPIs plotting selection option   - XYN-2757'");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        baseMethods.plotMetric(config.ltePciMetric, parameter.ltePciMetric, parameter.TableDroppable);
        browser.pause(500);
        baseMethods.verifySpinnerVisibility();
        var DialogBoxMsg = browser.elements(parameter.dialogBoxTitle).getText();
        assert.equal(DialogBoxMsg,config.dialogBoxTitle);
        var DialogBoxHeader = browser.elements(parameter.dialogBoxHead).getText();
        assert.equal(DialogBoxHeader,config.dialogBoxHeader);
        metricsDataObject.clickSelectionMetricsOKBtn();
        baseMethods.verifySpinnerVisibility();
        browser.pause(500);
        var av=browser.elements(parameter.tableAppData);
        var headerRow = av.elements(parameter.tableHeaderRow);
        console.log(headerRow.value.length);     
        var headerColumn=browser.elements("//span[contains(text(),'"+config.ltePciMetric+"')]").value.length;
        assert.equal(headerColumn,1);
        baseMethods.closeAllViews();
        console.log("Test completed To Verify Per Cell KPIs plotting selection option   - XYN-2757'");

    });
});