let baseMethods = require('../../testbase');
//require('../../configurations');
let genericObject = require("../../../pageobjects/genericobjects");
let parameter = require("../../parameters.js");
let importDataObject= require("../../../pageobjects/importDataPage");
let searchdataObject= require("../../../pageobjects/searchDataPage");
let config = require("../../../config.js");
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var path = require('path');
var expect = require('chai').expect;
require('../../commons/global.js')();

describe('drive-test page', function () {
    var importScreen=browser.elements('import-data-files');
    var dataGrid=importScreen.elements('[data-test-id="data-grid-container"]');
    var uploadDataSetName=config.addDatasetName;
    before(function(){
        uploadDataSetName=baseMethods.saveFileName(config.addDatasetName);
    });
    beforeEach(function () {
        if (!isElectron()) {
            baseMethods.loginToApplication();
            //baseMethods.verifySpinnerVisibility(); 
            browser.waitForExist("[data-test-id='Analytics View']");
        }
        importDataObject.clickImportDataMenu();
        importDataObject.clickImportAddDataMenu();
    });
    
    afterEach(function () {
        if(isElectron()){
            console.log("Test Completed");
        }
    });
    it('Verification of import data in history tab, XYN-2902, ', function () {
        console.log("Test started- Verification of import data in history tab, XYN-2902");
        //genericObject.clearPreviousValueFromField(importAddDataSetInputField);
        //browser.pause(300); 
        var dataSetName=uploadDataSetName;
        console.log(dataSetName);
        importDataObject.enterDataSetName(dataSetName);
        browser.pause(600);   
        var absolutePath = path.resolve(__dirname, config.fileNameWithPath1);      
        importDataObject.browseFile(absolutePath);
        browser.pause(400);
        importDataObject.clickImportBtn();
        browser.pause(400);
        importDataObject.waitForProgressPage();
        importDataObject.getProgressDataSet(dataSetName);
        browser.pause(9000);
            //browser.element("//span[@data-test-id='History']").click();
        browser.waitForExist(parameter.historyOpenBtn);
        browser.pause(400);
        importDataObject.enterValueInHistoryFilter(dataSetName);
        browser.pause(400);
        var getUploadedDataset=importDataObject.getDataSetCount(parameter.historyDataGrid,parameter.gridRows,dataSetName);
        console.log(getUploadedDataset);
        browser.pause(300);           
        expect(getUploadedDataset).to.be.equal(parseInt(config.compareValueWithOne));
    });
});