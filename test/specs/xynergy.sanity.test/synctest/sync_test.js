let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../../pageobjects/genericobjects.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
require('../../commons/global.js')();

describe('sync-test page', function () {

    var init=0;
    beforeEach(function () {
        if (!isElectron()) {
            console.log("Logging into the application");
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest)
            
            console.log("Logging successful");
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
        }

    });

    afterEach(function () {
        if(isElectron()){
        console.log("closing all open views");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        console.log("closing all open views");
            var closeAllViews=browser.element("[data-test-id='close-all-btn']");
            if(closeAllViews.isVisible()){
            closeAllViews.click();
            browser.pause(300);
            console.log("clicking on close btn");
            //var closeAllViewsTitleBar=browser.element("[data-test-id='Close All Views-Title-Bar']");
            var modalBox=browser.element("//modal-box");
            var mBoxCloseBtn=modalBox.element("[data-test-id='close-all-views']");
            if(mBoxCloseBtn.isVisible()){
            mBoxCloseBtn.click();
            console.log("clicked on close btn");
            }
            browser.pause(300);
            //close views menu
            var viewsHeader=browser.element("//views-menu");
            var closeViewsMenu=viewsHeader.element("[data-test-id='close-button']");
            if(closeViewsMenu.isVisible()){
                closeViewsMenu.click();
            }
            }
        }        
    });
    it('verify synchronization of data on timeseries,table view and messages widget', function () {
    console.log("verify synchronization of data on timeseries,table view and messages widget");
    //browser.windowHandleFullscreen();
    var assertValue = false;
    var timestamp = "03/30/2018 17:59:47";       
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    baseMethods.dragAndDrop(parameter.TimeseriesDraggable, parameter.FreshViewsDroppable);
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    baseMethods.dragAndDrop(parameter.TableDraggable, parameter.WidgetBottomBox);
    browser.execute(() => {
        let obj = document.querySelector("[data-test-id='v-box-right']");
        obj.outerHTML = ''
    });
    genericObject.clickOnDTTab(parameter.ViewsMenu);
    baseMethods.dragAndDrop(parameter.MessagesDraggable, parameter.WidgetRightBox);
    baseMethods.plotEvent(config.eventName, "[data-test-id='eventName"+config.eventName+"']", parameter.PlotAcrossWidgets);
    //var windowSize=browser.windowHandleSize();
    //console.log(windowSize);
    var elem = browser.elements("//*[@class='highcharts-root']//*[@class='highcharts-series-group']//*[@data-z-index='0.1']//*[@class='highcharts-point highcharts-color-0']");
    var elementValue = elem.value;
    var i = 2;
        while(i<10){
            browser.pause(200);
            browser.element("//*[@class='highcharts-root']//*[@class='highcharts-series-group']//*[@data-z-index='0.1']["+i+"]//*[@class='highcharts-point highcharts-color-0']").click();
            i--;
    var tablecellcontent = browser.element("//datatable-row-wrapper/datatable-body-row[contains(@class,'datatable-body-row active')]/div[2]/datatable-body-cell[2]").getText();
    browser.pause(500);
    var msgcellcontent = browser.element("//datatable-row-wrapper/datatable-body-row[contains(@class,'datatable-body-row active')]/div[2]/datatable-body-cell[2]/div[1]").getText();
    var timeseriesElement = browser.element("//div[@class='height-width-full']/div[1]/*[@class='highcharts-root']/*[@data-z-index='8']/*[@data-z-index='1']");
    var timeseriestooltipContent = timeseriesElement.element('<tspan />').getText();
    console.log(tablecellcontent);
    console.log(msgcellcontent);
    console.log(timeseriestooltipContent);
    if (tablecellcontent.includes(timestamp) && msgcellcontent.includes(timestamp)) {
        if(timeseriestooltipContent.includes(timestamp)){
        assertValue = true;
        }
    }
if(tablecellcontent==timeseriestooltipContent){
    if(tablecellcontent==timeseriestooltipContent){
        assertValue = true;
        break;
    }
}}
    assert.equal(assertValue, true);
    console.log("verify synchronization of data on timeseries,table view and messages widget");
});
});