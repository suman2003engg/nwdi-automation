let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
let genericObject = require('../../generic_objects.js');
//let sequenceDiagram = require('../../../pageobjects/sequence-diagram.js');
var config = require('../../../config.js')
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var searchdataObject=require('../../../pageobjects/searchdataPage.js');
var expect = require('chai').expect;require('../../commons/global')();

describe('Search Data', function () {
    beforeEach(function () { 
        if (!isElectron()) {
        console.log("Logging into the application");
        baseMethods.loginToApplication();
        console.log("Logging successful");
        }
    });

    afterEach(function () {
        if(isElectron()){
        console.log("Clearing the Search keyword and results");
        baseMethods.SelectTabs("SEARCHDATA");
        searchdataObject.newSearchPage();
        console.log("Cleared the Search keyword and results");
        }
        
    });

    it('Verify if user is able to search by FileName XYN-1474', function(){
        console.log("Test started - to verify if user is able to search by Filename XYN-1474");
        //baseMethods.navigateToAdvancedSearchMenu(); 
        browser.element(parameter.DataMenu).click();
        baseMethods.SelectTabs("SEARCHDATA");
        baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,config.searchTextForFIleName);
        //baseMethods.searchNQLQueryForSingleDevice();
        browser.element(parameter.treeNodeDropDown).click();
        var listItem = browser.elements(parameter.listItem).getText();
        console.log(listItem);
        var listItemlength = browser.elements(parameter.listItem).value.length;
        console.log("Filename results count is " + listItemlength);
        var j=0;
        while (listItemlength > j){
            //var rowContent = browser.element(listItem[j]).getText();
            //console.log(rowContent);  
            expect(listItem[j]).to.include(config.searchTextForFIleName);
            j++;
        }
        console.log("Verified each record from filter results if it matches with the search keyword");
        searchdataObject.selectdatasetresult();
        var selectdataset = browser.element(parameter.filteredItem);
        var visibility = selectdataset.isVisible();
        assert(visibility, true);
        console.log("Test completed - to verify if user is able to search by Filename");
    });

    it('XYN-2789_To verify search data retainity', function () {
        console.log("Test started- XYN-2789_To verify search data retainity");
        baseMethods.SelectTabs("SEARCHDATA");
       baseMethods.SerchQueryInNewSearch(config.searchItemFileName,config.searchCondition,config.searchTextForFIleName);
       var bResultStatus=browser.isExisting(parameter.searchResultPane);
       assert.equal(bResultStatus,true,bResultStatus+" actual status is not same as expected status"+true);
       browser.pause(1000);
       var sExpectedText=browser.element(parameter.resultFirstItem).getText();
       var sText=browser.element(parameter.filteredItem).getText();
       var sText1=sText.split(" ");
       console.log(sText1[0]);
       expect(sExpectedText.toLowerCase()).to.include(sText1[1].toLowerCase());
      // assert.equal(sText,config.searchSelectDataSet,sText+" actual status is not same as expected status"+config.searchSelectDataSet);
       // baseMethods.clickOnSearchData();
       baseMethods.SelectTabs("IMPORTDATA");
       var bImportdataexistance=browser.isExisting(parameter.addData);
       assert.equal(bImportdataexistance,true,bImportdataexistance+" actual status is not same as expected status"+true);
       baseMethods.SelectTabs("SEARCHDATA");
       var sText=browser.element(parameter.filteredItem).getText();
       console.log(sText);
       expect(sText.toLowerCase()).to.include(sText1[1].toLowerCase());
       //assert.equal(sText,config.searchSelectDataSet,sText+" actual status is not same as expected status"+config.searchSelectDataSet);
        console.log("Test completed - XYN-2789_To verify search data retainity");
    });  

    it('XYN-1477_Verify if user gets a message when invalid search criteria is provided ', function(){
        console.log("Test started - XYN-1477_Verify if user gets a message when invalid search criteria is provided");
        baseMethods.SelectTabs("SEARCHDATA");
        var newQueryTab = browser.element(parameter.searchTextArea);
        newQueryTab.click();
        newQueryTab.keys(config.searchTextForFIleNameForError);
        var searchButton = browser.element(parameter.searchButton);
        searchButton.click();
        browser.pause(500);
        var bStatus = browser.element(parameter.incorrectQuery).isExisting();
        console.log(bStatus);
        assert(bStatus, true);
        var errortitle=browser.element(parameter.errorMessageTitle).getText();
        assert(errortitle, config.searcherrorMessageTitle);
        var errorMessage=browser.element(parameter.incorrectQuery).getText();
        assert(errorMessage, config.searcherrorMessageText);
        browser.element(parameter.incorrectQueryClose).click();
        console.log("Test completed - XYN-1477_Verify if user gets a message when invalid search criteria is provided");
    });

    it('XYN-1478_Verify if user gets a message when there are no records matching the search criteria', function () {
        console.log("Test started- XYN-1478_Verify if user gets a message when there are no records matching the search criteria");
       //baseMethods.SerchQueryInNewSearch("Dataset","contains","voice");
       baseMethods.SelectTabs("SEARCHDATA");
       baseMethods.searchNQLQueryCustom(config.searchItemFileName,config.searchCondition,config.searchTextForFIleNameForError);
       var sText=browser.element(parameter.errorMessage).getText();
       console.log(sText+" text is displayed");
       assert.equal(sText,config.searchErrorMsg,sText+" actual status is not same as expected status"+config.searchErrorMsg);
       browser.element(parameter.errorMessageOK).click();
        console.log("Test completed - XYN-1478_Verify if user gets a message when there are no records matching the search criteria");
    }); 

});