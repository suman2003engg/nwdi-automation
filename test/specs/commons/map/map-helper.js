var MapHelper = (function() {
    function MapHelper() {}
    MapHelper.prototype.someFunction = function(someArg) {
        // Implementation goes here
        return "map";
    };
    return MapHelper;
}());
module.exports = function() {
    this.mapHelper = new MapHelper();
}