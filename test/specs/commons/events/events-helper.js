var EventsHelper = (function() {
    function EventsHelper() {}
    EventsHelper.someFunction = function(someArg) {
        // Implementation goes here
    };
    return EventsHelper;
}());
module.exports = function() {
    this.eventsHelper = new EventsHelper();
}