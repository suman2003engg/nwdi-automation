var MetricsHelper = (function() {
    function MetricsHelper() {}

    MetricsHelper.prototype.plotMetric = function(metricName) {
        /** plotMetric implementation goes here  **/
        return "metric";
    };

    return MetricsHelper;
}());
module.exports = function() {
    this.metricsHelper = new MetricsHelper();
}