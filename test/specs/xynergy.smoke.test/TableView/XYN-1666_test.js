let baseMethods = require('../../testbase.js');
let parameter = require('../../parameters.js');
var tableViewObject=require('../../../pageobjects/tableViewPage.js');
let eventsDataObject= require("../../../pageobjects/metricsAndEventsDataPage.js");
let genericObject=require('../../generic_objects.js');
let config=require('../../../config.js');
var dragAndDrop = require('html-dnd').codeForSelectors;
var assert = require('assert');
var conf = require('../../../config');
var expect = require('chai').expect;require('../../commons/global')();

describe('Table View',function(){
    var init=0;
    beforeEach(function () {
        if (!isElectron()) {
            console.log("Logging into the application");
            baseMethods.loginToApplication();
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest)
            
            console.log("Logging successful");
        }
        if(isElectron()){
            if(init==0){
            baseMethods.clickOnAdvancedSearchViewIcon(config.searchDatasetForRegularTest);
            init=1;
            }
        }

    });

    afterEach(function () {
        if(isElectron()){
        console.log("closing all open views");
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(500);
        console.log("closing all open views");
            var closeAllViews=browser.element("[data-test-id='close-all-btn']");
            if(closeAllViews.isVisible()){
            closeAllViews.click();
            browser.pause(300);
            console.log("clicking on close btn");
            //var closeAllViewsTitleBar=browser.element("[data-test-id='Close All Views-Title-Bar']");
            var modalBox=browser.element("//modal-box");
            var mBoxCloseBtn=modalBox.element("[data-test-id='close-all-views']");
            if(mBoxCloseBtn.isVisible()){
            mBoxCloseBtn.click();
            console.log("clicked on close btn");
            }
            browser.pause(300);
            //close views menu
            var viewsHeader=browser.element("//views-menu");
            var closeViewsMenu=viewsHeader.element("[data-test-id='close-button']");
            if(closeViewsMenu.isVisible()){
                closeViewsMenu.click();
            }
            }
        }
        
    });

    it('To verify whether filter is working accordingly - XYN-1666', function () {
        console.log("Test started to verify filter functionality according to Device name");        
        genericObject.clickOnDTTab(parameter.ViewsMenu);
        browser.pause(300);
        genericObject.dragAndDropViews(parameter.TableDraggable, parameter.FreshViewsDroppable);
        browser.pause(300);
        baseMethods.plotMetric(config.metricName, parameter.RSRPMetric, parameter.PlotAcrossWidgets);
        var selectRowElement = browser.element(parameter.SelectColumn);
        selectRowElement.click();
        browser.pause(300);
        var selectTime = browser.element(parameter.DeviceName);
        selectTime.click();
        tableViewObject.enterTableFilterValue(conf.findTableViewFilterValue1);
        browser.pause(300);
        var j = 1;
        rowCount = browser.elements(parameter.gridRows).value.length;
        console.log(rowCount);
        while (rowCount > 2) {
            var row = browser.elements("//datatable-row-wrapper[" + j +"]");
            var rowContent=row.element("//datatable-body-cell[3]");
            console.log(rowContent.getText(),j);
            expect(rowContent.getText()).to.include(conf.findTableViewFilterValue1);
            j++;
            rowCount--;   
        }
    });
});