var path = require('path');
const config = require('./wdio.shared.conf').config;
// const appPath = path.join(process.env.LOCALAPPDATA, "5G Lab Intelligence/UI/5G Lab Intelligence.exe");
const appPath = path.join(process.env.ProgramFiles, "Keysight/Nemo 5G Device Analytics/UI/Nemo 5G Device Analytics.exe");
//console.log("Application to launch", appPath);
config.debug = true;
config.maxInstances = 1;
config.capabilities = [{
    maxInstances: 1,
    browserName: 'chrome',
    chromeOptions: {
        binary: appPath,
        //args: ['--no-sandbox', '--disable-gpu', '--disable-dev-shm-usage', '--disable-setuid-sandbox']
    },
}];

config.before = function () {
    return new Promise((resolved, rejected) => {
        let counter = 0;
        let intervalId = setInterval(async () => {
            if (counter++ > 10) {
                clearInterval(intervalId);
                rejected();
            }
            let windowHandles = await browser.windowHandles();
            if (windowHandles && windowHandles.value && windowHandles.value.length > 0) {
                windowHandles.value.forEach(async (handle) => {
                    let url = await browser.window(handle).getUrl();
                    if (url && url.indexOf('/app/main') > -1) {
                        clearInterval(intervalId);
                        await browser.switchTab([handle]);
                        resolved();
                    }
                });
            }
        }, 40000);
    });
}
config.host = 'localhost';
config.port = 9515;
config.baseUrl = 'NA';
exports.config = config;